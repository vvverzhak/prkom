
from PyQt5 import Qt, QtSql
import sys, copy
import config, lib
from lib import protocol

def init():
	
    config.param = copy.deepcopy(protocol.send_recv(config.info_port, { }, True))

def connect():

    cn = QtSql.QSqlDatabase.addDatabase("QPSQL")
    cn.setHostName(config.server_ip)
    cn.setPort(config.param["db_port"])
    cn.setDatabaseName(config.param["db_name"])
    cn.setUserName(config.param["db_login"])
    cn.open()

def run(c_dialog):

    init()

    app = Qt.QApplication(sys.argv)

    connect()

    dia = c_dialog()
    dia.show()

    exit(app.exec())

