#!/usr/bin/python

import config

config.import_init()

import client
import ui.view

client.run(ui.view.c_dialog)

