#!/usr/bin/python

import config

config.import_init()

import client
import ui.main_secretary.view

client.run(ui.main_secretary.view.c_dialog)

