
from PyQt5 import Qt, QtCore, QtSql
import ui
from build.ui_enrolment import Ui_enrolment_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__is_init = False

        self.ui.contest_field.currentIndexChanged.connect(self.refresh)
        self.ui.is_original_field.toggled.connect(self.refresh)
        self.ui.decree_field.currentIndexChanged.connect(self.refresh)
        self.ui.left2right_button.clicked.connect(self.left2right)
        self.ui.right2left_button.clicked.connect(self.right2left)

        self.__decree_model = QtSql.QSqlQueryModel()
        self.__decree_model.setQuery("select value, id from decree where id > 1")
        self.ui.decree_field.setModel(self.__decree_model)

        self.__contest_model = QtSql.QSqlQueryModel()
        self.__contest_model.setQuery("select value, id from contestXfull")
        self.ui.contest_field.setModel(self.__contest_model)
        
        self.__is_init = True

        self.refresh()

    def refresh(self):

        if self.__is_init:

            contest = self.__contest_model.record(self.ui.contest_field.currentIndex()).value("id")
            decree = self.__decree_model.record(self.ui.decree_field.currentIndex()).value("id")
            is_original = self.ui.is_original_field.isChecked()
            
            self.__left_model = c_model(self.ui.left_view, contest, is_original, True)
            self.__right_model = c_model(self.ui.right_view, contest, is_original, False, decree)

    def move(self, view, model, decree):

        for ind in view.selectionModel().selectedRows():

            __id = model.get_st_id(ind.row())

            query = QtSql.QSqlQuery()
            self.throw_if_db(not query.exec("update statement set decree = %d where id = %d" % (decree, __id)), "Ошибка при обновлении приказа", msg = "Ошибка при обновлении приказа")

        self.refresh()

    def left2right(self):

        decree = self.__decree_model.record(self.ui.decree_field.currentIndex()).value("id")
        self.move(self.ui.left_view, self.__left_model, decree)

    def right2left(self):

        self.move(self.ui.right_view, self.__right_model, 1)

############################################################################ 

class c_model(QtSql.QSqlQueryModel):

    def __init__(self, ui, contest, is_original, is_only_first_decree, decree = None):

        super().__init__()

        copy_where = "id not in (select aid from education_document_full where is_main and is_copy)" if is_original else "True"

        if is_only_first_decree: decree_where = "id not in (select aid from statement where decree > 1)"
        else: decree_where = "decree = %d" % decree

        self.setQuery("""

        select
            
            id, __id,
            concat('(', reg_code, ') ', fio) as value,
            get_rating_by_group(id, spec_group) as rating
                
        from abiturient_conf_statement
        where
            
            (%s) and (%s)
            and
            contest = %d

        order by rating desc, fio

        """ % (decree_where, copy_where, contest))

        ui.setModel(self)

        ############################################################################ 

        for en_name, rus_name, size in \
        [
            ( "id", None, None ), ( "__id", None, None ), ( "value", "Абитуриент", 200 ), ( "fio", None, None ), ( "rating", "Оценка", 100 )
        ]:

            ind = self.record().indexOf(en_name)

            if rus_name:

                self.setHeaderData(ind, QtCore.Qt.Horizontal, rus_name)
                ui.setColumnWidth(ind, size)
            
            else:
                
                ui.setColumnHidden(ind, True)

    get_st_id = lambda self, row: self.record(row).value("__id")

