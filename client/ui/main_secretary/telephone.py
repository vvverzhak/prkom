
import ui, ui.edit, model
from model import c_field
from build.ui_telephone import Ui_telephone_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__model = c_model(self.ui.telephone_view)

        self.ui.find_button.clicked.connect(self.find)
        self.ui.telephone_view.doubleClicked.connect(self.edit)

    def find(self):

        raw_number = self.ui.telephone_field.text()
        self.__model.my_select(raw_number)

    def edit(self, index):

        aid = self.__model.record(index.row()).value("aid")
        dia = ui.edit.c_dialog(self, aid)
        dia.exec()

        self.__model.my_select()

class c_model(model.c_model):

    def __init__(self, ui):

        super().__init__(ui, "telephone",
        [
            c_field("id", "id", False, False, False),
            c_field("fio", "ФИО", True, False, False),
            c_field("relation_type_value", "Приходится", True, False, False),
            c_field("who", "Кому", True, False, False),
            c_field("type", "Телефон", True, False, False),
            c_field("code", "Код", True, False, False),
            c_field("number", "Номер", True, False, False)
        ])

        self.__raw_number = None

        self.setFilter("False")

    def my_select(self, raw_number = None):

        if raw_number != None:
            
            self.__raw_number = raw_number

        self.setFilter("""
        
            number = '%s'
            or
            position(concat(code, number) in '%s') > 0
            or
            position('%s' in concat(code, number)) > 0

        """ %
        (self.__raw_number, self.__raw_number, self.__raw_number))

        self.select()
        self.ui.resizeColumnsToContents()

