
from PyQt5 import QtGui, QtWidgets, QtSql
import ui, model.checkbox
from model import c_field
from build.ui_rating import Ui_rating_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.ui.refresh_button.clicked.connect(self.refresh)
        self.ui.rating_view.customContextMenuRequested.connect(self.rating_menu)
        
        self.refresh()

    def refresh(self):

        examine_name = self.ui.examine_combo.currentIndex() + 1
        examine_type = self.ui.examine_type_combo.currentIndex() + 1

        self.__model = c_model(self, self.ui.rating_view, examine_name, examine_type)
        self.ui.rating_view.setModel(self.__model)

    def rating_menu(self):

        menu = QtWidgets.QMenu()

        menu.addAction("Ввести оценку", self.set_rating)

        menu.exec(QtGui.QCursor.pos())

    def set_rating(self):

        rating, ok = QtWidgets.QInputDialog.getInt(self, "Оценка", "Введите оценку", 50, 0, 100, 1)

        if ok:

            for ind in self.ui.rating_view.selectionModel().selectedRows():

                self.__model.set_rating(ind, rating)

############################################################################ 

class c_model(model.checkbox.c_model):

    def __init__(self, parent_ui, ui, examine_name, examine_type):

        self.__parent_ui = parent_ui
        self.__examine_name = examine_name
        self.__examine_type = examine_type

        super().__init__(ui, "abiturient_full",
        [
            c_field("id", "", False, False, False),
            c_field("reg_code", "Рег. код", True, False, False),
            c_field("fio", "ФИО", True, False, False),
            c_field("passport", "Паспорт", True, False, False)
        ])

        self.id_column = self["id"].id

        self.select()
        self.setFilter("id in (select aid from examine_full where examineXname = %d and examineXtype = %d and yes)" % (self.__examine_name, self.__examine_type))

    def set_rating(self, ind, rating):

        aid = self.data(self.index(ind.row(), self.id_column))

        query = QtSql.QSqlQuery()
        self.__parent_ui.throw_if_db(not query.exec("update examine set rating = %d, yes = False where aid = %d and examineXname = %d and examineXtype = %d" % (rating, aid, self.__examine_name, self.__examine_type)), "Ошибка при изменении элемента", msg = "Ошибка при изменении элемента")

        self.__parent_ui.refresh()

