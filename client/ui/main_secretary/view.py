
from PyQt5 import QtWidgets
import codecs
import model, ui.view, ui.hate, ui.wait, lib.protocol, config
import ui.main_secretary.history, ui.main_secretary.telephone, ui.main_secretary.ivc, ui.main_secretary.rating, ui.main_secretary.enrolment
from ui.main_secretary.list import c_category_dialog, c_spec_dialog, c_contest_dialog, c_decree_dialog
from ui.view import c_menu_element

class c_dialog(ui.view.c_dialog):

    def __init__(self):

        super().__init__(title_postfix = " - консоль славного секретаря")

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Оценки",
                sub_elements =
                [
                    c_menu_element(name = "Оценки", icon = ":icons/pencil", fun = self.rating)
                ]
            ))

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Зачисление",
                sub_elements =
                [
                    c_menu_element(name = "Приказы о зачислении", icon = ":icons/books", fun = self.decree),
                    c_menu_element(name = "Зачисление", icon = ":icons/refresh", fun = self.enrolment)
                ]
            ))

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Резервное копирование",
                sub_elements =
                [
                    c_menu_element(name = "Резервное копирование", icon = ":icons/save", fun = self.backup)
                ]
            ))

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Служебные списки",
                sub_elements = 
                [
                    c_menu_element(name = "Категории поступающих", icon = ":icons/category", fun = self.category),
                    c_menu_element(name = "Специальности", icon = ":icons/spec", fun = self.spec),
                    c_menu_element(name = "Цели поступления", icon = ":icons/contest", fun = self.contest),
                ]
            ))

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Разное",
                sub_elements =
                [
                    c_menu_element(name = "История печати заявлений", icon = ":icons/history", fun = self.history),
                    c_menu_element(name = "Поиск по телефонам", icon = ":icons/telephone", fun = self.telephone),
                    c_menu_element(name = "Замечания и предложения", icon = ":icons/hate", fun = self.hate),
                    c_menu_element(name = "Сверка", icon = ":icons/check", fun = self.check),
                    c_menu_element(name = "Бухгалтерия", icon = ":icons/finance", fun = self.finance),
                    c_menu_element(name = "ИВЦ", icon = ":icons/stupid", fun = self.ivc),
                    c_menu_element
                    (
                        name = "ФИС ЕГЭ",
                        sub_elements =
                        [
                            c_menu_element(name = "Формирование запроса", icon = ":icons/open", fun = self.fis_ege),
                            c_menu_element(name = "Проверка результатов", icon = ":icons/check", fun = self.check_fis_ege)
                        ]
                    ),
                    c_menu_element
                    (
                        name = "Комитет глубокого бурения (центр развития Узбекистана)",
                        sub_elements =
                        [
                            c_menu_element(name = "Анкеты и заявления лиц, не являющихся гражданами России", icon = ":icons/not_russian", fun = self.not_russian)
                        ]
                    )
                ]
            ))

    def category(self): c_category_dialog(self).exec()
    def spec(self): c_spec_dialog(self).exec()
    def contest(self): c_contest_dialog(self).exec()
    def decree(self): c_decree_dialog(self).exec()
    def history(self): ui.main_secretary.history.c_dialog(self).exec()
    def telephone(self): ui.main_secretary.telephone.c_dialog(self).exec()
    def rating(self): ui.main_secretary.rating.c_dialog(self).exec()
    def enrolment(self): ui.main_secretary.enrolment.c_dialog(self).exec()
    def fis_ege(self): self.load_file("CSV - файл для ФИС ЕГЭ", "Сгенерировать CSV - файл для ФИС ЕГЭ?", "CSV - файл", "fis_ege")
    def backup(self): self.load_file("Резервное копирование", "Сделать backup?", "Файл с backup'ом", "backup")
    def check(self): self.load_file("CSV - файл для сверки", "Сгенерировать CSV - файл для сверки?", "CSV - файл", "check")
    def finance(self): self.load_file("CSV - файл для бухгалтерии", "Сгенерировать CSV - файл для бухгалтерии?", "CSV - файл", "finance")
    def not_russian(self): self.load_file("Zip - архив с анкетами и заявлениями лиц, не являющихся гражданами России", "Сгенерировать zip - архив с анкетами и заявлениями лиц, не являющихся гражданами России?", "Zip - архив", "not_russian")

    def ivc(self):
        
        dia = ui.main_secretary.ivc.c_dialog(self)

        if dia.exec() == QtWidgets.QDialog.Accepted:

            self.load_file("", "", "Файл", "ivc", dia.param, without_question = True)

    def load_file(self, title, question, suffix_desc, content_type, param = {}, without_question = False):

        if without_question or self.question(title, question):

            param["type"] = content_type

            func = lambda: lib.protocol.send_recv(config.param["remote_control_port"], param, True)
            wait_dia = ui.wait.c_dialog(self, func)
            wait_dia.run()

            content = wait_dia.result

            if content["status"]:

                suffix = content["file"]["suffix"]

                fname = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить файл", filter = "%s (*%s)" % (suffix_desc, suffix))[0]

                if fname != "":

                    if fname[- len(suffix) : ] != suffix : fname += suffix
                    lib.protocol.unpack_file(content["file"], fname)

                    self.info(title, "Файл сохранен")

    def hate(self):
        
        dia = ui.hate.c_dialog(self,
        [
            model.c_field("when_date", "Дата", True, False, False),
            model.c_field("when_time", "Время", True, False, False),
            model.c_field("ip", "Автор", True, False, False)
        ])
        dia.init_for_main_secretary()
                
        dia.exec()

    def check_fis_ege(self):

        title = "Проверка результатов ФИС ЕГЭ"

        if self.question(title, "Проверить результаты ФИС ЕГЭ?"):

            fname = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл с результатами", filter = "CSV-файл (*.csv)")[0]

            if fname != "":

                param = { "type" : "check_fis_ege" }

                with codecs.open(fname, "r", encoding = "CP1251") as fl:

                    param["data"] = fl.readlines()

                func = lambda: lib.protocol.send_recv(config.param["remote_control_port"], param, True)
                wait_dia = ui.wait.c_dialog(self, func)
                wait_dia.run()

                content = wait_dia.result
                suffix = content["file"]["suffix"]

                fname = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить файл", filter = "CSV-файл (*%s)" % suffix)[0]

                if fname != "":

                    if fname[- len(suffix) : ] != suffix : fname += suffix
                    lib.protocol.unpack_file(content["file"], fname)

                    self.info(title, "Файл сохранен")

