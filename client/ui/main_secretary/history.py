
import ui
from model import c_model, c_field
from build.ui_history import Ui_history_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__model = c_model(self.ui.history_view, "history_full",
        [
            c_field("name", "Имя", True, False, False),
            c_field("surname", "Фамилия", True, False, False),
            c_field("patrname", "Отчество", True, False, False),
            c_field("passport_series", "Серия паспорта", True, False, False),
            c_field("passport_number", "Номер паспорта", True, False, False),
            c_field("user_name", "Пользователь", True, False, False),
            c_field("ip", "IP", True, False, False),
            c_field("description", "Описание", True, False, False),
            c_field("when_date", "Дата печати", True, False, False),
            c_field("when_time", "Время печати", True, False, False)
        ])

        self.ui.refresh_button.clicked.connect(self.__model.select)
        self.__model.select()
        self.ui.history_view.resizeColumnsToContents()

