
from PyQt5 import QtSql
import ui
from build.ui_ivc import Ui_ivc_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)
        
        self.__model = QtSql.QSqlQueryModel()
        self.__model.setQuery("select value, id from decree")
        self.ui.decree_field.setModel(self.__model) 

        self.ui.generate_button.clicked.connect(self.generate)

    def generate(self):

        self.param = \
        {
            "decree" : self.__model.record(self.ui.decree_field.currentIndex()).value("id")
        }

        self.accept()

