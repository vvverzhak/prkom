
from PyQt5 import QtGui, QtWidgets
import ui, model, model.checkbox
from build.ui_list_edit import Ui_list_edit_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, wname, parent, table_name, fields):

        super().__init__(ui_class, parent)

        self.setWindowTitle(wname);

        self.model = model.checkbox.c_model(self.ui.view, table_name, fields)

        self.ui.view.customContextMenuRequested.connect(self.menu)

        self.column_resize()
        self.reset()

    def add(self):

        ind = self.model.rowCount()

        self.model.insertRow(ind)
        self.ui.view.selectRow(ind)

    def delete(self):

        if self.question("Удаление", "Удалить выбранные элементы? Связанные с ними абитуриенты ТАКЖЕ БУДУТ УДАЛЕНЫ"):
        
            if self.question("Удаление", "Вы ДЕЙСТВИТЕЛЬНО ОСОЗНАЕТЕ, что те абитуриенты, для которых справедливы выбранные элементы, ТАКЖЕ БУДУТ УДАЛЕНЫ?"):

                for ind in self.ui.view.selectionModel().selectedRows():

                    self.model.removeRow(ind.row())

    def menu(self, pnt):

        menu = QtWidgets.QMenu()

        menu.addAction("Добавить", self.add)
        if self.model.rowCount() > 0: menu.addAction("Удалить", self.delete)
        menu.addSeparator()
        menu.addAction("Сохранить", self.save)
        menu.addAction("Отменить", self.reset)

        menu.exec(QtGui.QCursor.pos())

    def save(self):
        
        try: self.throw_if_db(not self.model.submitAll(), "Ошибка при сохранении изменений", model = self.model)
        except: None

        self.reset()

    def reset(self):

        self.model.select()

############################################################################ 

class c_category_dialog(c_dialog):

    def __init__(self, parent):

        super().__init__("Категории поступающих", parent,
            "category",
            [
                model.c_field(en_name = "value", rus_name = "Категория", is_disp = True, is_edit = True, is_relation = False),
                model.c_field(en_name = "purpose", rus_name = "Целевое поступление", is_disp = True, is_edit = True, is_relation = True),
            ]
        )

    def column_resize(self):

        self.resize(800, self.size().height())

        self.ui.view.setColumnWidth(self.model["value"].id, 500)
        self.ui.view.setColumnWidth(self.model["purpose"].id, 200)

class c_spec_dialog(c_dialog):

    def __init__(self, parent):

        super().__init__("Специальности", parent,
            "spec",
            [
		model.c_field(en_name = "group_specXgroup", rus_name = "Группа специальностей", is_disp = True, is_edit = True, is_relation = True),
		model.c_field(en_name = "code", rus_name = "Код", is_disp = True, is_edit = True, is_relation = False),
		model.c_field(en_name = "name", rus_name = "Название", is_disp = True, is_edit = True, is_relation = False),
		model.c_field(en_name = "is_spec", rus_name = "Специальность?", is_disp = True, is_edit = True, is_relation = False),
		model.c_field(en_name = "is_fast", rus_name = "Ускоренники?", is_disp = True, is_edit = True, is_relation = False)
            ]
        )

    def column_resize(self):

        self.resize(1000, self.size().height())
        
        self.ui.view.setColumnWidth(self.model["group_specXgroup"].id, 200)
        self.ui.view.setColumnWidth(self.model["code"].id, 100)
        self.ui.view.setColumnWidth(self.model["name"].id, 280)
        self.ui.view.setColumnWidth(self.model["is_spec"].id, 150)
        self.ui.view.setColumnWidth(self.model["is_fast"].id, 150)

class c_contest_dialog(c_dialog):

    def __init__(self, parent):

        super().__init__("Специальности", parent,
            "contest",
            [
		model.c_field(en_name = "target", rus_name = "Цель", is_disp = True, is_edit = True, is_relation = True),
		model.c_field(en_name = "form", rus_name = "Форма", is_disp = True, is_edit = True, is_relation = True),
		model.c_field(en_name = "pay", rus_name = "Оплата", is_disp = True, is_edit = True, is_relation = True),
		model.c_field(en_name = "faculty", rus_name = "Факультет", is_disp = True, is_edit = True, is_relation = True),
		model.c_field(en_name = "spec", rus_name = "Специальность", is_disp = True, is_edit = True, is_relation = True, relation_field_name = "code")
            ]
        )

    def column_resize(self):

        self.resize(1100, self.size().height())
        
        self.ui.view.setColumnWidth(self.model["target"].id, 300)
        self.ui.view.setColumnWidth(self.model["form"].id, 210)
        self.ui.view.setColumnWidth(self.model["pay"].id, 210)
        self.ui.view.setColumnWidth(self.model["faculty"].id, 120)
        self.ui.view.setColumnWidth(self.model["spec"].id, 150)

class c_decree_dialog(c_dialog):

    def __init__(self, parent):

        super().__init__("Приказы о зачислении", parent,
            "decree",
            [
		model.c_field(en_name = "value", rus_name = "Приказ", is_disp = True, is_edit = True, is_relation = False),
		model.c_field(en_name = "date", rus_name = "Дата", is_disp = True, is_edit = True, is_relation = False), # TODO Отладить
            ]
        )

    def column_resize(self):

        self.resize(600, self.size().height())
        
        self.ui.view.setColumnWidth(self.model["value"].id, 400)
        self.ui.view.setColumnWidth(self.model["date"].id, 150)

