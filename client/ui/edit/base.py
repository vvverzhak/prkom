
from PyQt5 import Qt, QtSql, QtWidgets
import config, ui
from lib.exception import e_base

class c_dialog(ui.c_dialog):

    def __init__(self, table_name, parent, ui_class, aid = None):

        super().__init__(ui_class, parent)

        self.table_name = table_name

        self.model = QtSql.QSqlRelationalTableModel()
        self.model.setTable(self.table_name)
        self.model.setEditStrategy(QtSql.QSqlTableModel.OnManualSubmit)

        self.__event_filter = ui.widgets.c_event_filter()
        self.widgets = list(map(lambda w: ui.widgets.c_widget(self, w, self.model, aid), self.findChildren(QtWidgets.QWidget, Qt.QRegExp("_field"))))

        for tab_widget in self.findChildren(QtWidgets.QTabWidget):

            tab_widget.setCurrentIndex(0)

        self.ui.add_button.clicked.connect(self.add)
        self.ui.save_button.clicked.connect(self.save)

    def refresh(self, tid):

        self.id = tid
        is_edit = self.is_edit()

        ############################################################################ 
        # Инициализация модели

        if is_edit:
                        
            self.model.setFilter("%s.id = %d" % (self.table_name, self.get_id()))
            self.model.select()

        else:

            self.model.insertRow(self.model.rowCount())

        ############################################################################ 
        # Инициализация виджетов

        for w in self.widgets: w.real_init()

        ############################################################################ 
        # Инициализация маппера

        self.mapper = QtWidgets.QDataWidgetMapper()

        self.mapper.setModel(self.model)
        self.mapper.setItemDelegate(QtSql.QSqlRelationalDelegate(self))
        self.mapper.setSubmitPolicy(QtWidgets.QDataWidgetMapper.AutoSubmit)

        for w in self.widgets: w.mapper_init(self.mapper)

        if is_edit:	self.mapper.toFirst()
        else:		self.mapper.toLast()

        ############################################################################ 
        # Доинициализация виджетов

        for w in self.widgets: w.install_event_filter(self.__event_filter)

        self.ui.add_button.setEnabled(not is_edit)
        self.ui.save_button.setEnabled(is_edit)

        for w in self.widgets:
                
            if not is_edit: w.autoset()

            w.set_validator()

        if config.debug:

            for w in self.widgets: w.debug()

    ############################################################################
    # Подтверждение закрытия

    def closeEvent(self, event):
        
        if self.question("Закрытие формы ввода", "Закрыть форму ввода? Все несохраненные данные будут потеряны"): event.accept()
        else: event.ignore()

    ############################################################################ 

    def set_data(self, field_name, value):
        
        self.model.setData(self.model.index(0, self.model.fieldIndex(field_name)), value)

    def base_save(self):

        self.mapper.submit()
        self.throw_if_db(not self.model.submitAll(), "Ошибка при сохранении изменений", model = self.model)
        self.mapper.toFirst()

    def save(self):
            
        try:

            if self.question("Сохранение изменений", "Сохранить изменения?"):

                self.base_save()

        except e_base: pass

