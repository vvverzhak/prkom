
from PyQt5 import Qt, QtCore, QtGui, QtWidgets, QtSql
from lib.exception import e_base
import config, model, model.list_edit, lib
from ui.edit.base import c_dialog as c_base_dialog
from build.ui_education_document import Ui_education_document_dialog as education_document_ui_class
from build.ui_relation import Ui_relation_dialog as relation_ui_class
from build.ui_examine import Ui_examine_dialog as examine_ui_class
from build.ui_statement import Ui_statement_dialog as statement_ui_class

class c_dialog(c_base_dialog):

    def __init__(self, table_name, parent, ui_class, aid, eid = None):

        super().__init__(table_name, parent, ui_class, aid)
        self.refresh((aid, eid))

    get_id = lambda self: self.id[1]
    is_edit = lambda self: self.id[1] != None

    def add(self):

        try:

            self.mapper.submit()
            self.set_data("aid", self.id[0])
            self.throw_if_db(not self.model.submitAll(), "Ошибка при добавлении элемента", model = self.model)

            self.close()
        
        except e_base: pass

    def closeEvent(self, event):

        event.accept()

class c_education_document_dialog(c_dialog):

    def __init__(self, parent, aid, eid = None):

        super().__init__("education_document", parent, education_document_ui_class, aid, eid)

        self.ui.educationXdocumentXtype_field.currentIndexChanged.connect(self.type_changed)

    def type_changed(self, type_name):

        if \
            (type_name.lower().find("диплом") >= 0) \
            and \
            len(self.ui.who_field.text()) == 0:

                self.ui.who_field.setText("ФГБОУ ВПО Рязанский государственный радиотехнический университет")

class c_relation_dialog(c_dialog):

    def __init__(self, parent, aid, eid = None):

        self.__aid = aid
        super().__init__("relation", parent, relation_ui_class, aid, eid)

    def contextMenuEvent(self, ev):

        menu = QtWidgets.QMenu()

        menu.addAction("Заполнить домашний адрес", self.fill_address)
        menu.exec(QtGui.QCursor.pos())

    def fill_address(self):

        for fname in \
        [
            "address_region_additional", "address_city", "address_district", "address_street", "address_home",
            "address_building", "address_flat", "address_index", "telephone_code", "telephone_number" 
        ]:

            ui_fname = "%s_field" % fname
            self.ui.__dict__[ui_fname].setText(self.parent.ui.__dict__[ui_fname].text())

        for fname in \
        [
            "address_country", "address_base_region", "address_cityXtype"
        ]:
            
            ui_fname = "%s_field" % fname
            self.ui.__dict__[ui_fname].setCurrentIndex(self.parent.ui.__dict__[ui_fname].currentIndex())

class c_examine_dialog(c_dialog):

    def __init__(self, parent, aid, eid = None):

        super().__init__("examine", parent, examine_ui_class, aid, eid)

    def on_yes_field_toggled(self, is_set):

        self.ui.year_field.setEnabled(not is_set)
        self.ui.rating_field.setEnabled(not is_set)

class c_statement_dialog(c_dialog):

    def __init__(self, parent, aid, eid = None):

        super().__init__("statement", parent, statement_ui_class, aid, eid)

        self.ui.decree_field.setEnabled(config.param["level"] <= 3)

############################################################################ 

class c_education_document_model(model.list_edit.c_model):

    def __init__(self, parent, ui):

        super().__init__(
            parent = parent, ui = ui,
            table_name = "education_document",
            delete_question_title = "Удаление документа об образовании",
            delete_question = "Удалить документа об образовании?",
            edit_dialog = c_education_document_dialog,
            fields = \
            [
                model.c_field("educationXworkXdocumentXtype_value", "Тип", True, False, False),
                model.c_field("series_number", "Серия / номер", True, False, False),
                model.c_field("date", "Дата выдачи", True, False, False),
                model.c_field("who", "Кем выдан", True, False, False),
                model.c_field("main_marker", "Основание для поступления", True, False, False),
                model.c_field("copy_marker", "Копия", True, False, False)
            ])

class c_relation_model(model.list_edit.c_model):

    def __init__(self, parent, ui):

        super().__init__(
            parent = parent, ui = ui,
            table_name = "relation",
            delete_question_title = "Удаление родственника",
            delete_question = "Удалить родственника?",
            edit_dialog = c_relation_dialog,
            fields = \
            [
                model.c_field("relation_type_value", "Родство", True, False, False),
                model.c_field("fio", "ФИО", True, False, False)
            ])

class c_examine_model(model.list_edit.c_model):

    def __init__(self, parent, ui):

        super().__init__(
            parent = parent, ui = ui,
            table_name = "examine",
            delete_question_title = "Удаление экзамена",
            delete_question = "Удалить экзамен?",
            edit_dialog = c_examine_dialog,
            fields = \
            [
                model.c_field("type_name", "Тип", True, False, False),
                model.c_field("name", "Название", True, False, False),
                model.c_field("year", "Дата", True, False, False),
                model.c_field("yes", "Требуется сдать", True, False, False),
                model.c_field("rating", "Оценка", True, False, False)
            ])

class c_statement_model(model.list_edit.c_model):

    def __init__(self, parent, ui):

        super().__init__(
            parent = parent, ui = ui,
            table_name = "statement",
            delete_question_title = "Удаление цели поступления",
            delete_question = "Удалить цель поступления?",
            edit_dialog = c_statement_dialog,
            fields = \
            [
		model.c_field("target_value", "Цель", True, False, False),
		model.c_field("form_value", "Форма", True, False, False),
		model.c_field("pay_value", "Оплата", True, False, False),
		model.c_field("priority", "Приоритет", True, False, False),
		model.c_field("faculty_value", "Факультет", True, False, False),
		model.c_field("spec_code_name", "Спец.", True, False, False)
            ])

