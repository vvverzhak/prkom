
from PyQt5 import Qt, QtSql, QtWidgets
import lib, ui.wait
from lib.exception import e_base
import config, ui.widgets, ui.edit.lists
from ui.edit.base import c_dialog as c_base_dialog
from build.ui_edit import Ui_edit_dialog as ui_class

class c_dialog(c_base_dialog):

    def __init__(self, parent, aid = None):

        super().__init__("abiturient", parent, ui_class)

        self.__views = []

        for view in self.findChildren(QtWidgets.QTableView, Qt.QRegExp("_view")):

            obj_name = view.objectName()
            view_name = obj_name[ : obj_name.find("_view")]
            c_model = ui.edit.lists.__dict__[ "c_%s_model" % view_name ]

            self.__views.append(ui.widgets.c_view(self, view, c_model))

        self.ui.print_button.clicked.connect(self.print)

        self.refresh(aid)

    get_id = lambda self: self.id
    is_edit = lambda self: (self.id != None)

    def refresh(self, aid):

        super().refresh(aid)

        self.ui.print_button.setEnabled(self.is_edit())
        self.refresh_views(self.get_id())

    def refresh_views(self, aid):

        for v in self.__views:

            v.refresh(aid)

    ############################################################################
    # Кнопки

    def get_passport(self):
        
        return ( self.findChild(QtWidgets.QLineEdit, "passport_series_field").text(), self.findChild(QtWidgets.QLineEdit, "passport_number_field").text() )

    def add(self):

        try:

            if self.question("Добавление поступающего", "Действительно добавить поступающего?"):
    
                self.mapper.submit()
                self.set_data("date", Qt.QDate.currentDate())
                self.throw_if_db(not self.model.submitAll(), "Ошибка при добавлении поступающего", model = self.model)
                
                query = QtSql.QSqlQuery()
                passport = self.get_passport()
                query.exec("select id from %s where passport_series = '%s' and passport_number = '%s'" % (self.table_name, passport[0], passport[1]))
                query.next()

                self.refresh(query.value(0))

        except e_base: pass

    def __print(self, content):

        func = lambda: lib.protocol.send_recv(config.param["enroll_port"], content, True)

        dia = ui.wait.c_dialog(self, func)
        dia.run()

        return dia.result

    def print(self):

        try: 

            if self.question("Печать документов", "Распечатать документы (изменения будут сохранены)?"):

                self.base_save()
                content = { "aid" : self.get_passport() }
                result = self.__print(content)

                if "file" in result:
                
                    self.open_file(lib.protocol.unpack_file(result["file"]))

                for m in result["message"]: self.critical("Документы", m)

        except e_base: pass
                    
