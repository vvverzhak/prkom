
from PyQt5 import Qt, QtCore, QtGui
import random, socket
from lib.protocol import recv, send_to
import config, ui
from build.ui_chat import Ui_chat_dialog as ui_class

class c_server_thread(QtCore.QThread):

    recv_message = QtCore.pyqtSignal(str, str, int, bool)

    def __init__(self):

        super().__init__()

    def run(self):

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:

            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.settimeout(1)
            sock.bind(("", config.param["chat_port"]))

            self.__is_run = True

            while self.__is_run:

                try:

                    msg = recv(sock)
                    self.recv_message.emit(msg["nick"], msg["message"], msg["color_index"], msg["is_connect"])

                except socket.timeout:

                    pass

    def stop(self):

        self.__is_run = False

class c_dialog(ui.c_dialog):

    stop_server = QtCore.pyqtSignal()

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__server = c_server_thread()
        self.__server.start()
        self.throw_if(not self.__server.isRunning(), title = "Ошибка при открытии чата", msg = "Не удалось запустить сервер")

        self.ui.message_edit.returnPressed.connect(self.send_message)
        self.__server.recv_message.connect(self.recv_message)
        self.stop_server.connect(self.__server.stop)

        self.__connect()
    
    colors = [ "red", "yellow", "blue", "green", "magenta", "orange", None ]
    adjective = [ "Милая", "Прекрасная", "Смелая", "Гляяямурная", "Оригинальная", "Отважная", "сТРеМИТЕЛЬНАЯ", "Застенчивая", "Умная" ]
    noun = [ "обезьянка", "лягушка", "дифференциалка", "рокерша", "операторша", "хрюшка", "звездочка ПК", "тормозная жидкость", "твоя самая большая ошибка, чмоки, чмоки !!!111адин адин" ]

    def closeEvent(self, event):

        self.__send_message("Всем поки!!!!!!!1111АдинАдинАдин")

        self.stop_server.emit()
        self.__server.wait()

    def __prepare_message(self, msg):

        style_ind = self.ui.style_box.currentIndex()
        ret_msg = ""

        if style_ind == 0:

            # Деффчачий

            for ind in range(len(msg)):

                ret_msg += msg[ind].lower() if ind % 2 else msg[ind].upper()

        elif style_ind in (1, 2):

            # Котики

            for ch in msg:

                if ch in [" ", "\t"]:

                    ret_msg += ch

                else:

                    ret_msg += [ "Мяу", "Гав" ][style_ind == 2]

        elif style_ind == 3:

            # СанСаныч стайл

            ret_msg = "***$^@%!*#($(#уй"

        return ret_msg

    def recv_message(self, nick, msg, color_ind, is_connect):

        if msg == "": real_msg = " послала всем чмоки в этом чате :* :* :*"
        else: real_msg = ": %s" % msg

        real_msg = "%s%s" % (nick, real_msg)
        color = self.colors[color_ind]

        if color:

            self.ui.chat_edit.insertHtml("<p><span style=\"color:%s\">%s</span><br/></p>" % (color, real_msg))

        else:

            self.ui.chat_edit.insertHtml("<p>")

            for ch in real_msg:
            
                if ch == " ": self.ui.chat_edit.insertHtml("&nbsp;")
                else: self.ui.chat_edit.insertHtml("<span style=\"color:%s\">%s</span>" % (random.choice(self.colors), ch))

            self.ui.chat_edit.insertHtml("<br/></p>")

        hbar = self.ui.chat_edit.verticalScrollBar()
        hbar.setValue(hbar.maximum())

        if is_connect and (nick != self.__nick):

            self.__send_message("Приветик %s :* :* :*!!!!!!" % nick)

    def __connect(self):

        self.__nick = "%s %s" % (random.choice(self.adjective), random.choice(self.noun))
        self.__send_message("ДЕФФОЧКИ!!!!!!! Я С ВААААМИ, ВСЕХ ЛЮБЛЮ, ВСЕХ ЦЕЛЮЛЮ", is_connect = True)

    def send_message(self):

        msg = self.ui.message_edit.text()
        self.ui.message_edit.setText("")

        self.__send_message(msg)

    def __send_message(self, msg, is_connect = False):

        content = \
        {
            "nick" : self.__nick,
            "message" : self.__prepare_message(msg),
            "color_index" : random.randint(0, len(self.colors) - 1),
            "is_connect" : is_connect
        }

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:

            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            send_to(sock, content, True, "255.255.255.255", config.param["chat_port"])

