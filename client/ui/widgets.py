
from PyQt5 import Qt, QtCore, QtWidgets, QtSql
import config
from lib.exception import e_base

class c_event_filter(QtCore.QObject):
    
    def eventFilter(self, obj, event):
        
        if (event.type() == Qt.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Escape): return True
        return super().eventFilter(obj, event)

class c_widget(QtCore.QObject):

    def __init__(self, dia, widget, model, aid = None):

        super().__init__()

        self.__widget			=	widget
        self.__dia			=	dia
        self.__model			=	model
        self.__aid                      =       aid
        self.__obj_name			=	widget.objectName()
        self.__field_name		=	self.__obj_name[ : self.__obj_name.find("_field")]
        self.__field_index		=	model.fieldIndex(self.__field_name)
        self.__is_combo			=	type(widget) == QtWidgets.QComboBox
        self.__is_line_edit      	=	type(widget) == QtWidgets.QLineEdit

        # Скорее всего (я надеюсь на это) в качестве self при вызове installEventFilter используется self.__widget
        self.install_event_filter	=	self.__widget.installEventFilter

        if config.param["db_login"] == "op" and self.__field_name in [ "status" ]:
            
            self.__widget.setEnabled(False)

        if self.__is_combo:
                
            table_name = self.__field_name[ self.__field_name.rfind("_") + 1 : ]
            model.setRelation(self.__field_index, QtSql.QSqlRelation(table_name, "id", "value"))

    def real_init(self):

        if self.__is_combo:

            rel_model = self.__model.relationModel(self.__field_index)

            if self.__field_name == "relationXfull":

                rel_model.setFilter("aid = %d" % self.__aid)

            self.__widget.setModel(rel_model);
            self.__widget.setModelColumn(rel_model.fieldIndex("value"))

    def debug(self):

        if type(self.__widget) == QtWidgets.QLineEdit:		self.__widget.setText("TODO")
        elif type(self.__widget) == QtWidgets.QSpinBox:		self.__widget.setValue(3 if self.__obj_name == "course_field" else 42)

    def mapper_init(self, mapper):
        
        mapper.addMapping(self.__widget, self.__field_index)

    def autoset(self, index = 0):

        if self.__is_combo:				self.__widget.setCurrentIndex(index)
        elif self.__field_name.find("city") >= 0:	self.__widget.setText("Рязань")
        elif self.__field_name.find("year") >= 0:	self.__widget.setDate(Qt.QDate.currentDate())

        if self.__field_name == "education_address":	self.__widget.setText("Россия, Рязанская область, г. Рязань")

    def set_validator(self):

        if self.__field_name in [ "passport_series", "passport_number" ]:
            
            validator = Qt.QRegExpValidator(Qt.QRegExp("[a-zA-Zа-яА-Я0-9]*"))
            self.__widget.setValidator(validator);

# TODO выравнивание по вертикали полей описания экзаменов
class c_view(QtCore.QObject):

    def __init__(self, dia, view, c_model):

        super().__init__()

        self.__view = view
        self.__dia = dia
        
        self.__view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.__view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.__model = c_model(self.__dia, self.__view)

    def refresh(self, aid):

        self.__model.my_select(aid)

