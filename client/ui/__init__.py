
import os
from PyQt5 import Qt, QtCore, QtWidgets
import config
from lib.exception import e_base, e_db

class c_base:

    def __init__(self, ui_class):

        self.ui = ui_class()
        self.ui.setupUi(self)
        self.ui.retranslateUi(self)

    def throw(self, title, msg): raise e_base(self, title, msg)
    def throw_db(self, title, msg, model, query): raise e_db(self, title, msg, model, query)

    def throw_if(self, condition, title, msg):

        if condition: self.throw(title, msg)

    def throw_if_db(self, condition, title, msg = None, model = None, query = None):

        if condition: self.throw_db(title, msg, model, query)

    info = lambda self, title, msg: QtWidgets.QMessageBox.information(self, title, msg)
    critical = lambda self, title, msg: QtWidgets.QMessageBox.critical(self, title, msg)
    warning = lambda self, title, msg: QtWidgets.QMessageBox.warning(self, title, msg)
    question = lambda self, title, msg: \
        QtWidgets.QMessageBox.question(self, title, msg, QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No) == QtWidgets.QMessageBox.Yes

    open_file = lambda self, fname: Qt.QDesktopServices.openUrl(QtCore.QUrl.fromLocalFile(fname))

class c_window(c_base, QtWidgets.QMainWindow):

    def __init__(self, ui_class, parent = None):
            
        QtWidgets.QMainWindow.__init__(self, parent)
        c_base.__init__(self, ui_class)

class c_dialog(c_base, QtWidgets.QDialog):

    def __init__(self, ui_class, parent = None):
            
        QtWidgets.QDialog.__init__(self, parent)
        c_base.__init__(self, ui_class)

        self.parent = parent

