
from PyQt5 import QtWidgets
from lib.protocol import send_recv
import config, ui, model
from build.ui_hate import Ui_hate_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent, additional_fields = []):

        super().__init__(ui_class, parent)

        self.__model = model.c_model(self.ui.hate_view, "hate",
        [
            model.c_field("message", "Сообщение", True, False, False)
        ] + additional_fields)

        self.ui.refresh_button.clicked.connect(self.refresh)
        self.ui.message_edit.returnPressed.connect(self.send_message)

        self.refresh()

    def init_for_main_secretary(self):

        self.ui.hate_view.setShowGrid(True)
        self.ui.hate_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.ui.hate_view.horizontalHeader().setVisible(True)
        self.ui.hate_view.verticalHeader().setVisible(True)

    def refresh(self):

        self.__model.select()
        self.ui.hate_view.resizeColumnsToContents()
        self.ui.hate_view.scrollToBottom()

    def send_message(self):

        content = \
        {
            "type" : "hate",
            "message" : self.ui.message_edit.text()
        }

        self.ui.message_edit.setText("")
        send_recv(config.param["remote_control_port"], content, True)
        self.refresh()

