
import lib
from ui.report.param import c_base_dialog
from build.ui_param_fis import Ui_param_fis_dialog as ui_class

class c_dialog(c_base_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)
        
        self.ui.full_time_field.toggled.connect(self.full_time)

    def full_time(self, is_checked):

        self.ui.date_field.setEnabled(not is_checked)

    def param(self):

        param = super().param()

        param["full_time"] = self.ui.full_time_field.isChecked()
        param["date"] = lib.QDate_to_postgres_date(self.ui.date_field.date())

        return param

