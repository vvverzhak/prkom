
import lib
from ui.report.param import c_base_dialog
from build.ui_param_examine import Ui_param_examine_dialog as ui_class

class c_dialog(c_base_dialog):

    enames = [ "math", "phys", "rus", "society", "hist", "liter", "creation" ]

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        for ename in self.enames:

            field_name = "%s_examine_field" % ename

            self.ui.__dict__[field_name].addItem("Не учитывается")
            self.ui.__dict__[field_name].addItem("Не сдан")
            self.ui.__dict__[field_name].addItem("Сдан")

    def param(self):

        param = \
        {
            "examine" : self.ui.examine_field.currentIndex() + 1,
            "begin_date" : lib.QDate_to_postgres_date(self.ui.begin_date_field.date()),
            "end_date" : lib.QDate_to_postgres_date(self.ui.end_date_field.date()),
            "early_examines" : []
        }

        for eind in range(len(self.enames)):

            ind = self.ui.__dict__["%s_examine_field" % self.enames[eind]].currentIndex()

            if ind:

                param["early_examines"].append((eind + 1, ind == 1))

        return param

