
from PyQt5 import Qt, QtCore, QtWidgets, QtGui
import config, lib, ui, ui.wait
from lib.exception import e_base
from build.ui_report import Ui_report_dialog as ui_class
import ui.report.param
import ui.report.foreigner
import ui.report.today_table
import ui.report.fis
import ui.report.war
import ui.report.stat
import ui.report.form_2_6
import ui.report.hostel
import ui.report.extract
import ui.report.enrolment
import ui.report.contest
import ui.report.target_indulgence
import ui.report.examine
import ui.report.deanery_card

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__ui_classes = \
        {
            "None" : ui.report.param.c_dialog,
            "foreigner" : ui.report.foreigner.c_dialog,
            "today_table" : ui.report.today_table.c_dialog,
            "fis" : ui.report.fis.c_dialog,
            "war" : ui.report.war.c_dialog,
            "stat" : ui.report.stat.c_dialog,
            "form_2_6" : ui.report.form_2_6.c_dialog,
            "hostel" : ui.report.hostel.c_dialog,
            "extract" : ui.report.extract.c_dialog,
            "enrolment" : ui.report.enrolment.c_dialog,
            "contest" : ui.report.contest.c_dialog,
            "examine" : ui.report.examine.c_dialog,
            "deanery_card" : ui.report.deanery_card.c_dialog,
            "target_indulgence" : ui.report.target_indulgence.c_dialog
        }

        self.ui.report.doubleClicked.connect(self.generate)
        self.refresh_list()

    def refresh_list(self):

        content = { "type" : "tree" }
        content = lib.protocol.send_recv(config.param["report_port"], content, True)

        self.model = QtGui.QStandardItemModel()
        self.report_id_role = QtCore.Qt.UserRole + 1
        self.ui_class_role = QtCore.Qt.UserRole + 2

        def add_elem(main_elem, name, content):

            if name == []: current_elem = main_elem # Корень
            else:
                    
                current_elem = QtGui.QStandardItem(name[-1])
                main_elem.appendRow(current_elem)

            if type(content) == dict:

                current_elem.setSelectable(False)
                
                keys = list(content)
                keys.sort()

                for elem in keys: add_elem(current_elem, name + [ elem ], content[elem])

            else:
                    
                current_elem.setSelectable(True)
                current_elem.setData(name, self.report_id_role)
                current_elem.setData(content, self.ui_class_role)

        add_elem(self.model.invisibleRootItem(), [], content["tree"])
        self.ui.report.setModel(self.model)

    def __generate(self, content):

        func = lambda: lib.protocol.send_recv(config.param["report_port"], content, True)

        dia = ui.wait.c_dialog(self, func)
        dia.run()

        return dia.result

    def generate(self, index):

        try:

            lsl = self.ui.report.selectionModel().selectedIndexes()

            if len(lsl) > 0:

                ui_class_name = lsl[0].data(self.ui_class_role)
                dia = self.__ui_classes[ui_class_name](self)
                return_code = dia.exec()

                if return_code == QtWidgets.QDialog.Accepted:

                    content = { "type" : "report", "index" : lsl[0].data(self.report_id_role), "param" : dia.param() }
                    result = self.__generate(content)

                    self.throw_if(not result["status"], "Ошибка при получении отчета", "Не удалось получить отчет")
                    self.open_file(lib.protocol.unpack_file(result["file"]))

        except e_base:

            pass

