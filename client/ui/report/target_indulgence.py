
from PyQt5 import QtSql
from ui.report.param import c_base_dialog
from build.ui_param_target_indulgence import Ui_param_target_indulgence_dialog as ui_class

class c_dialog(c_base_dialog):

    tables = [ "target", "form" ]
    get_table_widget = lambda self, tname: self.ui.__dict__["%s_field" % tname]

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.__model = {}

        for tname in self.tables:

            self.__model[tname] = QtSql.QSqlQueryModel()
            self.__model[tname].setQuery("select value, id from %s" % tname)
            self.get_table_widget(tname).setModel(self.__model[tname]) 

    def param(self):

        param = \
        {
            "report_type" : self.ui.report_type_field.currentIndex()
        }

        for tname in self.tables:

            param[tname] = self.__model[tname].record(self.get_table_widget(tname).currentIndex()).value("id")

        return param

