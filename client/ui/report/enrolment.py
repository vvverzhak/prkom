
from ui.report.param import c_base_dialog
from build.ui_param_enrolment import Ui_param_enrolment_dialog as ui_class

class c_dialog(c_base_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

    def param(self):

        param = super().param()

        param["reason"] = self.ui.reason_field.currentIndex()

        return param

