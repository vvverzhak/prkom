
from ui.report.param import c_base_dialog
from build.ui_param_contest import Ui_param_contest_dialog as ui_class

class c_dialog(c_base_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

    def param(self):

        param = super().param()
        param["is_evolution"] = self.ui.is_evolution_field.isChecked()

        return param

