
from ui.report.param import c_base_dialog
from build.ui_param_foreigner import Ui_param_foreigner_dialog as ui_class

class c_dialog(c_base_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

    def param(self):

        param = \
        {
            "is_ebola" : self.ui.is_ebola_field.isChecked()
        }

        return param

