
from PyQt5 import Qt, QtCore, QtSql, QtWidgets
import ui
from build.ui_param import Ui_param_dialog as ui_class

class c_base_dialog(ui.c_dialog):

    lists = [ "target", "form", "pay", "faculty", "decree" ]

    def __init__(self, ui_class, parent):

        super().__init__(ui_class, parent)

        self.ui.generate_button.clicked.connect(self.accept)
        self.ui.cancel_button.clicked.connect(self.reject)

        for date_edit in self.findChildren(QtWidgets.QDateEdit):

            date_edit.setDate(QtCore.QDate.currentDate())

        for list_name in self.lists:

            self.create_list_model(list_name)

    def create_list_model(self, list_name):

        field_name = "%s_field" % list_name
        model_name = "%s_model" % list_name

        if field_name in self.ui.__dict__:

            self.__dict__[model_name] = QtSql.QSqlQueryModel()
            model = self.__dict__[model_name]

            model.setQuery("select value, id from " + list_name)
            self.ui.__dict__[field_name].setModel(model)

    def param(self):

        param = {}

        for list_name in self.lists:

            __id = "%s_id" % list_name
            field_name = "%s_field" % list_name
            model_name = "%s_model" % list_name

            if field_name in self.ui.__dict__:

                ind = self.ui.__dict__[field_name].currentIndex()
                record = self.__dict__[model_name].record(ind)
                param[__id] = record.value("id")

        for with_check_name in [ "original", "telephone" ]:

            field_name = "with_%s_field" % with_check_name

            if field_name in self.ui.__dict__:

                param["with_%s" % with_check_name] = self.ui.__dict__[field_name].isChecked()

        return param

class c_dialog(c_base_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

