
from PyQt5 import Qt, QtCore, QtGui, QtSql, QtWidgets
import itertools as it
from lib.exception import e_base
import ui, ui.report, ui.about, ui.edit
from build.ui_main import Ui_main_dialog as ui_class

class c_menu_element:

    def __init__(self, name, sub_elements = None, icon = None, fun = None):

        self.name = name
        self.sub_elements = sub_elements
        self.icon = icon
        self.fun = fun

    def is_menu(self):

        return self.sub_elements != None

    def __iter__(self):

        for elem in self.sub_elements:

            yield elem

class c_dialog(ui.c_window):

    def __init__(self, ui_class = ui_class, title_postfix = " - консоль оператора"):

        super().__init__(ui_class)

        self.setWindowTitle(self.windowTitle() + title_postfix)
        self.__model = c_model(self, self.ui)

        self.sub_menu = {}

        self.add_sub_menu(self.ui.menu,
            c_menu_element
            (
                name = "Отчеты",
                sub_elements =
                [
                    c_menu_element(name = "Отчеты", icon = ":icons/books", fun = self.report)
                ]
            ))

        self.ui.add_item.triggered.connect(self.add)
        self.ui.about_item.triggered.connect(self.about)
        self.ui.abiturient_view.doubleClicked.connect(self.edit)
        self.ui.find_button.clicked.connect(self.find)
        self.ui.abiturient_view.horizontalHeader().sectionClicked.connect(self.__model.sort_by_column)

        self.find()

    def add_sub_menu(self, parent_menu, elem):

        if elem.is_menu():

            menu = QtWidgets.QMenu(elem.name)
            self.sub_menu[elem.name] = menu
            parent_menu.addMenu(menu)

            for sub_elem in elem:

                self.add_sub_menu(menu, sub_elem)

        else:

            parent_menu.addAction(QtGui.QIcon(elem.icon), elem.name, elem.fun)

    report = lambda self: ui.report.c_dialog(self).exec()
    about = lambda self: ui.about.c_dialog(self).exec()

    def add(self):
       
        ui.edit.c_dialog(self).exec()
        self.find()

    def edit(self, index):

        ui.edit.c_dialog(self, self.__model.record(index.row()).field(0).value()).exec()
        self.find()

    def find(self):

        try:

            self.__model.select()
            self.ui.abiturient_view.setModel(self.__model)
            self.ui.abiturient_view.resizeColumnsToContents() 
            self.ui.abiturient_view.setSortingEnabled(True)

            for tid in range(self.__model.record().count()):

                self.ui.abiturient_view.setColumnHidden(tid, self.__model.is_hidden(tid))

            self.ui.comment_label.setText("Всего: %d человек(а)" % self.__model.num)

        except e_base: pass

############################################################################ 

# TODO Унаследовать от model.base_model
class c_model(QtSql.QSqlTableModel):

    def __init__(self, dia, ui):
            
        super().__init__()
        
        self.__dia = dia
        self.ui = ui
        self.num = 0
        self.__sort_column_id = 0
        self.__sort_sec = True

    def sort_by_column(self, column_id):

        if column_id == self.__sort_column_id:
            
            self.__sort_sec = not self.__sort_sec

        else:
            
            self.__sort_column_id = column_id
            self.__sort_sec = True

        self.select()

    def select(self):

        status_index = self.ui.status_box.currentIndex()
        name = self.ui.name_edit.text()
        surname = self.ui.surname_edit.text()
        patrname = self.ui.patrname_edit.text()
        reg_code = self.ui.reg_code_edit.text()
        passport_series = self.ui.passport_series_edit.text()
        passport_number = self.ui.passport_number_edit.text()

        def field_sub_query(name, value):
            
            if type(value) == str:      fmt = "%s ~ '%s'"
            elif type(value) == int:    fmt = "%s = %d"

            return " and " + fmt % (name, value)
        
        def field_status(index):

            if index == 3: value = "true"
            else: value = "status = %d" % (index + 1)

            return " and " + value

        self.setTable("abiturient_for_main_view")
        self.setFilter("true" + "".join
            (
                it.starmap(field_sub_query,
                [
                    ("name", name), ("surname", surname), ("patrname", patrname), ("reg_code", reg_code),
                    ("passport_series", passport_series), ("passport_number", passport_number)
                ])
            )
            + field_status(status_index)
        )
        self.setSort(self.__sort_column_id, QtCore.Qt.AscendingOrder if self.__sort_sec else QtCore.Qt.DescendingOrder)

        self.__disp_field = {}

        for rus_name, en_name in [ ("Рег. код", "reg_code"), ("ФИО", "fio"), ("Паспорт", "passport") ]:

            tid = self.record().indexOf(en_name)
            self.__disp_field[tid] = { "rus" : rus_name, "en" : en_name }
            self.setHeaderData(self.record().indexOf(self.__disp_field[tid]["en"]), QtCore.Qt.Horizontal, self.__disp_field[tid]["rus"])

        self.__dia.throw_if_db(not super().select(), "Ошибка при выводе списка абитуриентов", msg = "Введены некорректные критерии поиска")
        self.num = self.rowCount()

        return True

    is_hidden = lambda self, tid: tid not in self.__disp_field
    flags = lambda self, index: QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled

