
from PyQt5 import Qt
import config, ui, ui.chat, ui.hate
from build.ui_about import Ui_about_dialog as ui_class

class c_dialog(ui.c_dialog):

    def __init__(self, parent):

        super().__init__(ui_class, parent)

        self.ui.version_edit.display(config.param["version"])
        self.ui.code_name_label.setText(config.param["code_name"])
        self.ui.date_edit.setText(config.param["date"])

        self.ui.chat_button.clicked.connect(self.chat)
        self.ui.hate_button.clicked.connect(self.hate)

    chat = lambda self: ui.chat.c_dialog(self).exec()
    hate = lambda self: ui.hate.c_dialog(self).exec()

