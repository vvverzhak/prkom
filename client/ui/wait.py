
from PyQt5 import Qt, QtCore, QtWidgets
import ui
from build.ui_wait import Ui_wait_dialog as ui_class

class c_thread(QtCore.QThread):

    def __init__(self, func):

        super().__init__()

        self.__func = func
        self.result = {}

    def run(self):

        self.result = self.__func()

class c_dialog(ui.c_dialog):

    def __init__(self, parent, func):

        super().__init__(ui_class, parent)

        gif = Qt.QMovie(":/image/image/wait.gif")
        
        self.ui.view.setBackgroundRole(Qt.QPalette.Base)
        self.ui.view.setSizePolicy(Qt.QSizePolicy.Ignored, Qt.QSizePolicy.Ignored)
        self.ui.view.setScaledContents(True)
        self.ui.view.setMovie(gif)

        self.ui.cancel_button.clicked.connect(self.reject)
        
        gif.start()

        self.__thr = c_thread(func)
        self.__thr.finished.connect(self.accept)

    def run(self):

        self.__thr.start()
        self.exec()

    def accept(self):

        self.result = self.__thr.result

        super().accept()

    def reject(self):

        self.result = { "status" : False }

        super().reject()

