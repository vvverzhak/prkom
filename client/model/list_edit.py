
from PyQt5 import Qt, QtCore, QtGui, QtWidgets, QtSql
from model import c_model

class c_model(c_model):

    def __init__(self, parent, ui, table_name, fields, edit_dialog, delete_question_title, delete_question):

        super().__init__(ui, "%s_full" % table_name, fields)

        self.__aid = None
        self.__parent = parent
        self.__table_name = table_name
        self.__question = lambda: self.__parent.question(delete_question_title, delete_question)
        self.edit_dialog = edit_dialog

        self.setEditStrategy(QtSql.QSqlTableModel.OnManualSubmit)
        self.ui.horizontalHeader().setSectionResizeMode(Qt.QHeaderView.Stretch)
        self.ui.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        self.ui.customContextMenuRequested.connect(self.menu)
        self.ui.doubleClicked.connect(self.edit)

    def my_select(self, aid = None):
        
        if aid != None:

            self.__aid = aid
            self.setFilter("aid = %d" % self.__aid)

        self.select()

    def select(self):

        if self.__aid != None:

            return super().select()

        return False

    def __element_id(self, index = None):

        # TODO Жуткий костыль - в statement_full вернуть поле id
        if self.__table_name == "statement": id_field = "__id"
        else: id_field = "id"

        if index:
        
            element_id = self.record(index.row()).field(id_field).value()

        else:

            indexes = self.ui.selectedIndexes()

            if len(indexes) > 0:
                
                element_id = self.record(indexes[0].row()).field(id_field).value()

            else:

                element_id = -1

        return element_id

    ############################################################################ 
    # Редактирование

    def add(self):
        
        self.edit_dialog(self.__parent, self.__aid).exec()
        self.__parent.refresh_views(self.__parent.id)

    def delete(self):
        
        element_id = self.__element_id()
        
        if element_id >= 0 and self.__question():
            
            query = QtSql.QSqlQuery()
            self.__parent.throw_if_db(not query.exec("delete from %s where id = %d" % (self.__table_name, element_id)), "Ошибка при удалении элемента", msg = "Ошибка при удалении элемента")
            self.__parent.refresh_views(self.__parent.id)

    def menu(self):

        menu = QtWidgets.QMenu()

        if self.__aid:

            menu.addAction("Добавить", self.add)
            if self.rowCount() > 0: menu.addAction("Удалить", self.delete)

        else:

            menu.addAction("Добавьте абитуриента перед тем, как заполнять данное поле")

        menu.exec(QtGui.QCursor.pos())

    def edit(self, index):

        self.edit_dialog(self.__parent, self.__aid, self.__element_id(index)).exec()
        self.my_select()

