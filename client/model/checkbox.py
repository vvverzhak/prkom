
from PyQt5 import Qt, QtCore, QtGui, QtSql
from model import c_model

class c_model(c_model):

    def __init__(self, ui, table_name, fields):

        super().__init__(ui, table_name, fields)

        self.setEditStrategy(QtSql.QSqlTableModel.OnManualSubmit)

        self.ui.resizeColumnsToContents()
        self.ui.horizontalHeader().setSectionResizeMode(Qt.QHeaderView.Interactive)

