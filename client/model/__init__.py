
from PyQt5 import Qt, QtCore, QtSql
import lib

class c_field:

    def __init__(
        self,
        en_name, rus_name,
        is_disp, is_edit, is_relation,
        relation_field_name = "value"
    ):

        self.en_name = en_name
        self.rus_name = rus_name
        self.is_disp = is_disp
        self.is_edit = is_edit
        self.is_relation = is_relation

        if is_relation:

            self.relation_table_name = en_name.split("_")[-1]
            self.relation_field_name = relation_field_name

class c_model(QtSql.QSqlRelationalTableModel):

    def __init__(self, ui, table_name, fields):

        super().__init__()

        self.ui = ui
        self.fields = fields

        self.setTable(table_name)

        for f in fields:

            f.id = self.record().indexOf(f.en_name)
            self.setHeaderData(f.id, QtCore.Qt.Horizontal, f.rus_name)
            
            if f.is_relation:
                
                self.setRelation(f.id, QtSql.QSqlRelation(f.relation_table_name, "id", f.relation_field_name))
                self.ui.setItemDelegateForColumn(f.id, QtSql.QSqlRelationalDelegate(self.ui))

        self.ui.setSortingEnabled(True)
        self.__sort_column_id = 0
        self.__sort_sec = QtCore.Qt.AscendingOrder

        self.ui.horizontalHeader().sectionClicked.connect(self.sort_by_column)
        
        self.ui.setModel(self)

        for tid in range(self.record().count()):
            
            self.ui.setColumnHidden(tid, True)

        for f in self.fields:
            
            self.ui.setColumnHidden(f.id, not f.is_disp)

    def select(self):

        self.setSort(self.__sort_column_id, self.__sort_sec)

        return super().select()

    def sort_by_column(self, column_id):

        self.__sort_column_id = column_id

        if self.__sort_sec == QtCore.Qt.DescendingOrder or column_id != self.__sort_column_id:
                
            self.__sort_sec = QtCore.Qt.AscendingOrder

        else:

            self.__sort_sec = QtCore.Qt.DescendingOrder

        self.select()

    @lib.get_index()
    def data(self, index, role = QtCore.Qt.EditRole, ind = None):

        if self.is_disp(ind):
                
            value = super().data(index)
            
            if role == QtCore.Qt.TextAlignmentRole:	return QtCore.Qt.AlignCenter

            if type(value) == bool:

                if role == QtCore.Qt.CheckStateRole:	return QtCore.Qt.Checked if value else QtCore.Qt.Unchecked
                if role == QtCore.Qt.DisplayRole:	return "Да" if value else "Нет"
                
        return super().data(index, role)

    @lib.get_index()
    def setData(self, index, value, role = QtCore.Qt.EditRole, ind = None):

        if not self.is_edit(ind):

            return False

        if self.is_disp(ind) and role == QtCore.Qt.CheckStateRole:
        
            return super().setData(index, value == QtCore.Qt.Checked)

        return super().setData(index, value, role)

    @lib.get_index()
    def flags(self, index, ind = None):

        if self.is_disp(ind):
            
            flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable

            if self.is_edit(ind):

                flags |= QtCore.Qt.ItemIsEditable

            return flags

        return super().flags(index)

    def __get_field_name_by_type(self, value):

        if type(value) == str: return "en_name"
        elif type(value) == int: return "id"
        else: return None

    def __getitem__(self, value):

        fname = self.__get_field_name_by_type(value)
        if not fname: return None

        for f in self.fields:

            if f.__dict__[fname] == value:
                
                return f

        return None

    def is_disp(self, value):

        f = self[value]

        if f: return f.is_disp

        return False

    def disp_fields(self):

        for f in self.fields:

            if f.is_disp:

                yield f

    def is_edit(self, value):

        f = self[value]

        if f: return f.is_edit

        return False

    def edit_fields(self):

        for f in self.fields:

            if f.is_edit:

                yield f

    def relation_fields(self):

        for f in self.fields:

            if f.is_relation:

                yield f

