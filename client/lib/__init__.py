
import time
import itertools as it

level = 0
target_form_first_course = list(it.product([1, 3], [1, 2, 3]))
target_form = list(it.product([1, 2, 3, 4], [1, 2, 3]))
form_pay = list(it.product([1, 2, 3], [1, 2]))
target_form_pay = list(it.product([1, 2, 3, 4], [1, 2, 3], [1, 2]))
target_form_pay_priority = list(it.product([1, 2, 3, 4], [1, 2, 3], [1, 2], range(1, 4)))

def function_description(desc):

    def handler(func):

        def fn(*args, **kwargs):

            global level

            print("\n%s[ %s ...\n" % ("\t" * level, desc))
            level += 1

            func(*args, **kwargs)

            level -= 1
            print("\n%s... %s ]\n" % ("\t" * level, desc))

        return fn

    return handler

def id_column(order, field_name = "id"):
    
    return "row_number() over(order by %s) as %s" % (order, field_name)

# Текущая дата и время
# Возвращает: ( дата, время, дата время, дата в формате postgresql, скорректированное время )
class c_current_date_time:

    def __init__(self):

        self.__tm = time.localtime()

        self.year = self.__tm.tm_year
        self.month = self.__tm.tm_mon
        self.day = self.__tm.tm_mday
        self.hour = self.__tm.tm_hour
        self.minute = self.__tm.tm_min

        self.date = "%.2d.%.2d.%d" % (self.day, self.month, self.year)
        self.date_postgres = "%d-%.2d-%.2d" % (self.year, self.month, self.day)
        self.time = "%.2d:%.2d" % (self.hour, self.minute)

    def corrected_date(self, year_add = None, month_add = None, day_add = None):

        if not year_add: year_add = 0
        if not month_add: month_add = 0
        if not day_add: day_add = 0

        return "%.2d.%.2d.%d" % (self.day + day_add, self.month + month_add, self.year + year_add)

    def month_name(self):

        return { 1 : "январь", 2 : "февраль", 3 : "март", 4 : "апрель", 5 : "май", 6 : "июнь", 7 : "июль",
                 8 : "август", 9 : "сентябрь", 10 : "октябрь", 11 : "ноябрь", 12 : "декабрь" }[self.month]

def QDate_to_postgres_date(date):

    return "%d-%.2d-%.2d" % (date.year(), date.month(), date.day())

get_index = lambda: lambda func: lambda *args, **kwargs: func(ind = args[1].column(), *args, **kwargs)

