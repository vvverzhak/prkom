
import json, base64, socketserver, os, socket, tempfile
import config

begin_message = "<message>"
end_message = "</message>"
b_begin_message = bytes(begin_message, "UTF-8")
b_end_message = bytes(end_message, "UTF-8")
is_message = lambda data: (data.find(b_begin_message) >= 0) and (data.find(b_end_message) >= 0)

def pack(content, status = False):
    
    if content == None: content = {}
    content["status"] = status
    
    return ("%s%s%s" % (begin_message, json.dumps(content), end_message)).encode("UTF-8")

def unpack(data):
    
    try:
        
        return json.loads(data[data.find(b_begin_message) + len(b_begin_message) : data.find(b_end_message)].decode("UTF-8"))

    except:
        
        print("Получены некорректные данные")

    return None

def pack_file(fname, suffix):
    
    with open(fname, "rb") as fl:

        data = base64.b64encode(fl.read())
    
    return { "data" : data.decode("ASCII"), "suffix" : suffix }

def unpack_file(content_file, fname = None):
    
    if not fname:
        
        fname = tempfile.mktemp(suffix = content_file["suffix"])

    with open(fname, "wb") as fl:
    
        fl.write(base64.b64decode(content_file["data"]))
    
    return fname

def recv(sock):
    
    data = sock.recv(1024)
    while not is_message(data): data += sock.recv(1024)
    
    return unpack(data)

def send(sock, content, status):
    
    if content == None: status = False
    
    data = pack(content, status)

    try:
        
        sock.sendall(data)
    
    except:
        
        return False
	
    return True

def send_to(sock, content, status, ip, port):
    
    if content == None: status = False
    
    data = pack(content, status)

    try:
        
        sock.sendto(data, (ip, port))
    
    except:
        
        return False
	
    return True

def send_recv(port, content, status):

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        
        sock.connect((config.server_ip, port))
        send(sock, content, status)
        content = recv(sock)

    return content

