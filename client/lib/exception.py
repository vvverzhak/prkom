
from PyQt5 import QtSql

class e_base(Exception):

    def __init__(self, dia, title, msg, real_msg = None):

        super().__init__()

        dia.critical(title, msg)

class e_db(e_base):
	
    def __init__(self, dia, title, msg, model, query):

        if model: error = model.lastError()
        elif query: error = query.lastError()
        else: error = QtSql.QSqlDatabase.database().lastError()

        code = error.number()
        real_msg = error.text()

        if msg == None:

            msg = "Неизвестная ошибка при работе с базой данных\nКод ошибки: %d\nСообщение: %s" % (code, real_msg)

            if code in (0, -1):

                frm = real_msg.find("[[[")
                to = real_msg.find("]]]")

                if not (frm == -1 or to == -1): msg = real_msg[ frm + 3 : to ]
                elif real_msg.find("duplicate key value violates unique constraint") >= 0:
                    msg = "Абитуриент с такой комбинацией (серия паспорта, номер паспорта) уже существует"

        super().__init__(dia, title, msg, real_msg)

