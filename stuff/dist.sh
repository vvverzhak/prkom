#!/usr/bin/bash

if [ "$1" == "" ]
then

	echo "./dist.sh trash_dname"
	exit

fi

super_parent_dname=`pwd`
cd $1

parent_dname=`pwd`
dname=$parent_dname/abiturient_`date +%d.%m.%y`

mkdir -p $dname/operator $dname/main_secretary
mkdir -p $dname/raw

cd ../client
make
cd -

cp ../client $dname/raw -R

rm -Rf $dname/raw/resource

find $dname/raw -name "__pycache__" -type d -exec rm -Rf '{}' ';'
find $dname/raw -name "*.swp" -exec rm -f '{}' ';'

mv $dname/raw/client/my_operator.py $dname/operator
mv $dname/raw/client/main_secretary.py $dname/main_secretary
cp $dname/raw/client/config.py $dname/operator
mv $dname/raw/client/config.py $dname/main_secretary

cd $dname/raw/client

zip -r main_secretary.zip *
mv main_secretary.zip $dname/main_secretary
rm -Rf ui/main_secretary

zip -r operator.zip *
mv operator.zip $dname/operator/

cd $super_parent_dname
pwd

cp main_secretary.bat $dname/main_secretary
cp operator.bat $dname/operator

rm -Rf $dname/raw

cd $super_parent_dname

