
version		=	"21"
code_name	=	"Ultima ratio regum" # "Веерный Воронеж" # "Бешеный Бангкок" # "Апрельский Антананариву" # "Ultima ratio regum" "Феерический Фейерабенд" "Умопомрачительный Учкудук" "Тотальный Трабант" "Суперматичная саламандра" "Роковая Роксана" "Перманентный перепел" "Опрятная ондатра" "Неприкаянная нутрия" "Максимальный Максим" "Легендарная лаборатория" "Кропотливый кузнечик" "Йеменский Йоулупукки" "Индифферентная игуана" "Занудливый Зинэтула" "Жалостливый жиган" "Ерундовая ежевика" "Гугливый Галагура" "Досадный Дамблдор" "Визгливый Визилтер" "Бескомпромиссный базилик" "Аддитивный антрацит"
date		=	"30.09.2015"
chat_port       =       8236

is_old		=	True	# Для старого ПО сервера
base_dir        =       "/home/amv/project/abiturient/main/server/"
renderer	=	base_dir + "/lib/renderer.py"

class db:
    
    admin	=	"postgres"
    name	=	"abiturient"
    port	=	5432

class fill:
    
    num = 30
    education_document_num = 5
    relation_num = 5

class server:
    
    class report:
        
        port		=	2345
        pid_file	=	"/tmp/report_server_pid"

    class enroll:
        
        port            =	7891
        pid_file	=	"/tmp/enroll_server_pid"

    class remote_control:
        
        port		=	1987
        pid_file	=	"/tmp/remote_control_server_pid"

    class info:
        
        port		=	4242
        pid_file	=	"/tmp/info_server_pid"

class catalog:

    template		=	base_dir + "/template/"
    template_enroll	=	template + "/enroll"
    template_report	=	template + "/report"

class main_secretary:

    fio             =   "Степанов Д.С."
    fio_reversed    =   "Д.С. Степанов"
    full_name       =   "Степанов Денис Сергеевич"

