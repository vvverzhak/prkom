#!/usr/bin/python

import sys, unittest, optparse
import init, lib.server, db, db.fill, info, enroll, remote_control, report

servers = \
{
    "info" : info,
    "enroll" : enroll,
    "remote_control" : remote_control,
    "report" : report
}

def utest():

    runner = unittest.TextTestRunner(verbosity = 2)
    suite = unittest.TestSuite()

    for srv in servers.values():

        suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(srv.c_server.c_test))

    runner.run(suite)

param_parser = optparse.OptionParser()

for d in [ "start", "stop", "status", "test", "utest" ]:
    
    param_parser.add_option("--" + d, action = "callback",
        callback = lambda option, opt_str, value, parser: lib.server.run(servers, option, opt_str, value, parser),
        type = "choice", choices = list(servers.keys()))

param_parser.add_option("--init", action = "callback", callback = lambda *args, **kwargs: init.main())
param_parser.add_option("--fill", action = "callback", callback = lambda *args, **kwargs: db.fill.main())
param_parser.add_option("--all-utest", action = "callback", callback = lambda *args, **kwargs: utest())

param, args = param_parser.parse_args()

