
import unittest, tempfile
import db, config, lib, lib.server, lib.protocol
from report.tree import tree

def get_user_info(ip):

    with db.c_db() as cn:
        
        row = cn.select_one("select user_name, faculty, level from users where ip = '%s'" % ip)

    if row == None:

        return (None, None, 1000)

    return (row["user_name"], row["faculty"], row["level"])

def get_tree(content):

    user_name, user_faculty, user_level = get_user_info(content["reciever"][0])

    return ( True, { "tree" : tree.tree(user_name, user_faculty, user_level) })

def generate(content):

    user_name, user_faculty, user_level = get_user_info(content["reciever"][0])

    return tree.generate(content["index"], content["param"] if "param" in content else None, user_name, user_faculty, user_level)

def process(content):

    if	    content["type"] == "tree":	    return get_tree(content)
    elif    content["type"] == "report":    return generate(content)

    return (False, {})

class c_server:

    def start():    lib.server.start(config.server.report, process)
    def stop():	    lib.server.stop(config.server.report)
    def status():   lib.server.status(config.server.report)

    class c_test(unittest.TestCase):
        
        def doit(self, index, param = {}):

            status, content = generate({ "index" : index, "param" : param, "reciever" : ("127.0.0.1", 1234) })
            content = lib.protocol.pack(content, status)
            content_file = lib.protocol.unpack(content)["file"]
            lib.protocol.unpack_file(content_file, "/tmp/TODO.odt")

        test_today_table_first = lambda self: self.doit([ "Ежедневный отчет", "Таблицы" ], { "is_three" : False })
        test_today_table_all = lambda self: self.doit([ "Ежедневный отчет", "Таблицы" ], { "is_three" : True })
        test_today_fio = lambda self: self.doit([ "Ежедневный отчет", "ФИО" ])
        test_today_fsb = lambda self: self.doit([ "Комитет глубинного бурения", "Иностранцы" ], { "is_ebola" : False })
        test_today_fsb_with_Ebola = lambda self: self.doit([ "Комитет глубинного бурения", "Иностранцы" ], { "is_ebola" : True })
        test_fis_1 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 2, "full_time" : True, "date" : "2015-01-01" })
        test_fis_2 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 2, "full_time" : False, "date" : "2015-01-01" })
        test_fis_3 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 1, "full_time" : True, "date" : "2015-01-01" })
        test_fis_4 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 1, "full_time" : False, "date" : "2015-01-01" })
        test_fis_5 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 1, "full_time" : False, "date" : "2015-07-02" })
        test_fis_6 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 3, "full_time" : True, "date" : "2015-07-02" })
        test_fis_7 = lambda self: self.doit([ "ФИС ЕГЭ" ], { "target_id" : 4, "full_time" : True, "date" : "2015-07-02" })
        test_target_good_war = lambda self: self.doit([ "Целевики и льготники", "Правильные" ], { "report_type" : 0, "target" : 1, "form" : 1 })
        test_target_good_power = lambda self: self.doit([ "Целевики и льготники", "Правильные" ], { "report_type" : 1, "target" : 3, "form" : 1 })
        test_stupid_good = lambda self: self.doit([ "Целевики и льготники", "Правильные" ], { "report_type" : 2, "target" : 1, "form" : 1 })
        test_woexamine_good = lambda self: self.doit([ "Целевики и льготники", "Правильные" ], { "report_type" : 3, "target" : 1, "form" : 1 })
        test_target_bad = lambda self: self.doit([ "Целевики и льготники", "Ошибки" ])
        test_wo_priority = lambda self: self.doit([ "Без приоритетов" ])
        test_examine_1 = lambda self: self.doit([ "Экзамены" ], { "examine" : 1, "early_examines" : [], "begin_date" : "2010-01-01", "end_date" : "2015-12-01" })
        test_examine_2 = lambda self: self.doit([ "Экзамены" ], { "examine" : 2, "early_examines" : [], "begin_date" : "2010-01-01", "end_date" : "2015-12-01" })
        test_examine_3 = lambda self: self.doit([ "Экзамены" ], { "examine" : 5, "early_examines" : [ (2, True) ], "begin_date" : "2000-01-01", "end_date" : "2015-12-01" })
        test_examine_4 = lambda self: self.doit([ "Экзамены" ], { "examine" : 3, "early_examines" : [ (3, False) ], "begin_date" : "2010-01-01", "end_date" : "2015-12-01" })
        test_examine_5 = lambda self: self.doit([ "Экзамены" ], { "examine" : 1, "early_examines" : [], "begin_date" : "2013-01-01", "end_date" : "2015-12-01" })
        test_contest_1 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "is_evolution" : False })
        test_contest_2 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 1, "form_id" : 3, "pay_id" : 1, "is_evolution" : False })
        test_contest_3 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 1, "is_evolution" : False })
        test_contest_4 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "is_evolution" : True })
        test_contest_5 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 1, "form_id" : 3, "pay_id" : 1, "is_evolution" : True })
        test_contest_6 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 1, "is_evolution" : True })
        test_contest_7 = lambda self: self.doit([ "Конкурсные списки" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 2, "is_evolution" : False })
        test_enrolment_1 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 0 })
        test_enrolment_2 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 1 })
        test_enrolment_3 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 2 })
        test_enrolment_4 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 3 })
        test_enrolment_5 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 4 })
        test_enrolment_6 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1, "decree_id" : 2, "reason" : 0 })
        test_enrolment_7 = lambda self: self.doit([ "Список зачисленных" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 1, "decree_id" : 1, "reason" : 3 })
        test_extract_1 = lambda self: self.doit([ "Выписки из приказов" ], { "decree_id" : 1 })
        test_deanery_card_1 = lambda self: self.doit([ "Карточки", "Карточки для деканатов" ], { "faculty_id" : 3, "pay_id" : 1 })
        test_deanery_card_2 = lambda self: self.doit([ "Карточки", "Карточки для деканатов" ], { "faculty_id" : 3, "pay_id" : 2 })
        test_deanery_card_3 = lambda self: self.doit([ "Карточки", "Карточки для деканатов" ], { "faculty_id" : 2, "pay_id" : 2 })
        test_secret_card_1 = lambda self: self.doit([ "Карточки", "Секретные карточки" ], { "faculty_id" : 3, "pay_id" : 1 })
        test_secret_card_2 = lambda self: self.doit([ "Карточки", "Секретные карточки" ], { "faculty_id" : 3, "pay_id" : 2 })
        test_secret_card_3 = lambda self: self.doit([ "Карточки", "Секретные карточки" ], { "faculty_id" : 2, "pay_id" : 2 })
        test_war_1 = lambda self: self.doit([ "Военно-учетный стол" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1 })
        test_war_2 = lambda self: self.doit([ "Военно-учетный стол" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 2 })
        test_war_3 = lambda self: self.doit([ "Военно-учетный стол" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 1 })
        test_hostel_1 = lambda self: self.doit([ "Общежития" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 1 })
        test_hostel_2 = lambda self: self.doit([ "Общежития" ], { "target_id" : 1, "form_id" : 1, "pay_id" : 2 })
        test_hostel_3 = lambda self: self.doit([ "Общежития" ], { "target_id" : 3, "form_id" : 1, "pay_id" : 1 })
        test_region = lambda self: self.doit([ "Зачисленные по регионам" ])
        test_stat_1 = lambda self: self.doit([ "Статистика по бакалавриату" ], { "form_id" : 1 })
        test_stat_2 = lambda self: self.doit([ "Статистика по бакалавриату" ], { "form_id" : 2 })
        test_stat_3 = lambda self: self.doit([ "Статистика по бакалавриату" ], { "form_id" : 3 })
        test_2_6 = lambda self: self.doit([ "2.6" ], { "form_id" : 1 })

    def test():

        test = c_server.c_test()

        # test.test_today_table_first()
        # test.test_today_table_all()
        # test.test_today_fio()
        # test.test_today_fsb()
        # test.test_today_fsb_with_Ebola()
        # test.test_fis_1()
        # test.test_fis_2()
        # test.test_fis_3()
        # test.test_fis_4()
        # test.test_fis_5()
        # test.test_fis_6()
        # test.test_fis_7()
        # test.test_target_good_war()
        # test.test_target_good_power()
        # test.test_stupid_good()
        # test.test_woexamine_good()
        # test.test_target_bad()
        # test.test_examine_1()
        # test.test_examine_2()
        # test.test_examine_3()
        # test.test_examine_4()
        # test.test_examine_5()
        # test.test_contest_1()
        # test.test_contest_2()
        # test.test_contest_3()
        # test.test_contest_4()
        # test.test_contest_5()
        # test.test_contest_6()
        # test.test_contest_7()
        # test.test_enrolment_1()
        # test.test_enrolment_2()
        # test.test_enrolment_3()
        # test.test_enrolment_4()
        # test.test_enrolment_5()
        # test.test_enrolment_6()
        # test.test_enrolment_7()
        # test.test_extract_1()
        # test.test_deanery_card_1()
        # test.test_deanery_card_2()
        # test.test_deanery_card_3()
        # test.test_secret_card_1()
        # test.test_secret_card_2()
        # test.test_secret_card_3()
        # test.test_war_1()
        # test.test_war_2()
        # test.test_war_3()
        # test.test_hostel_1()
        # test.test_hostel_2()
        # test.test_hostel_3()
        # test.test_region()
        # test.test_stat_1()
        # test.test_stat_2()
        # test.test_stat_3()
        test.test_2_6()

    def utest():

        runner = unittest.TextTestRunner(verbosity = 2)
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(c_server.c_test)
        runner.run(suite)

