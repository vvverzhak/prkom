
def form_2_6(cn, param):

    form = param["form_id"]

    this_year = "date_part('year', date) = date_part('year', now())"

    names = \
    [
        "Всего",
        "    в том числе имеют образование: среднее общее",
        "        из них получили указанное образование в текущем году",
        "            из них выпускники общеобразовательных организаций",
        "                в том числе выпускники специальных (коррекционных) образовательных организаций и классов для обучающихся, воспитанников с ограниченными возможностями здоровья",
        "    среднее профессиональное образование по программам подготовки квалифицированных рабочих (служащих)",
        "        из них получили указанное образование в текущем году",
        "    среднее профессиональное образование по программам подготовки специалистов среднего звена",
        "        из них получили указанное образование в текущем году",
        "    высшее, подтвержденное дипломом бакалавра",
        "        из них получили указанное образование в текущем году",
        "    высшее, подтвержденное дипломом специалиста",
        "        из них получили указанное образование в текущем году",
        "    высшее, подтвержденное дипломом магистра",
        "        из них получили указанное образование в текущем году"
    ]

    where_vert = \
    [
        "True",
        "id in (select aid from education_document_full where is_main and is_cert)",
        "id in (select aid from education_document_full where is_main and is_cert and %s)" % this_year,
        None, # TODO - выпускники общеобразовательных организаций
        None, # TODO - в том числе - выпускники специальных (коррекционных) образовательных организаций
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 2)",
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 2 and %s)" % this_year,
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 3)",
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 3 and %s)" % this_year,
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 11)",
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 11 and %s)" % this_year,
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 4)",
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 4 and %s)" % this_year,
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 10)",
        "id in (select aid from education_document_full where is_main and educationXdocumentXtype = 10 and %s)" % this_year
    ]

    where_horz = \
    [
        # Бакалавриат
        "id in (select aid from statement_full where target = 1 and not is_spec and form = %d)" % form,
        "id in (select aid from statement_full where target = 1 and not is_spec and form = %d and decree > 1)" % form,
        "id in (select aid from statement_full where target = 1 and not is_spec and form = %d and decree > 1 and pay = 1)" % form,
        "id in (select aid from statement_full where target = 1 and not is_spec and form = %d and decree > 1 and pay = 2)" % form,

        # Специалитет
        "id in (select aid from statement_full where target = 1 and is_spec and form = %d)" % form,
        "id in (select aid from statement_full where target = 1 and is_spec and form = %d and decree > 1)" % form,
        "id in (select aid from statement_full where target = 1 and is_spec and form = %d and decree > 1 and pay = 1)" % form,
        "id in (select aid from statement_full where target = 1 and is_spec and form = %d and decree > 1 and pay = 2)" % form,

        # Магистратура
        "id in (select aid from statement_full where target = 3 and form = %d)" % form,
        "id in (select aid from statement_full where target = 3 and form = %d and decree > 1)" % form,
        "id in (select aid from statement_full where target = 3 and form = %d and decree > 1 and pay = 1)" % form,
        "id in (select aid from statement_full where target = 3 and form = %d and decree > 1 and pay = 2)" % form
    ]

    data = []
    get = lambda cn, v, h: cn.select_one("select count(*) as count from abiturient_conf_statement where %s and %s" % (v, h))["count"]
    count = len(names)

    for ind in range(count):

        data.append([ names[ind], "%0.2d" % (ind + 1) ])
        t_data = data[-1]
        v = where_vert[ind]

        for h in where_horz:

            t_data.append(get(cn, where_vert[ind], h) if (v != None and h != None) else "")

    return ("2_6", { "form" : cn.get_value("form", form), "data" : data })

