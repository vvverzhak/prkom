
def fis(cn, param):

    target = param["target_id"]
    full_time = param["full_time"]
    date = param["date"]

    name = [ "первый курс", "второй и последующий курсы", "магистратура", "второй и последующий курсы магистратуры" ] [ target - 1 ]

    real_date = [ "на %s" % date, "за все время" ] [ full_time ]
    date_where = [ "( date = '%s' )" % date, "True" ] [ full_time ]

    content = []
    aids = cn.select_all("select aid from statement_full where target = %d and aid in (select id from abiturient_conf where %s) group by aid"
            % (target, date_where))

    for __aid in aids:

        content.append({})
        t_content = content[-1]

        ############################################################################ 

        aid = __aid["aid"]

        info = cn.select_all("select * from abiturient_conf where id = %d" % aid)[0]

        t_content["reg_code"] = info["reg_code"]
        t_content["fio"] = info["fio"]
        t_content["date"] = str(info["date"])
        t_content["passport_country_value"] = info["passport_country_value"]
        t_content["passport_series"] = info["passport_series"]
        t_content["passport_number"] = info["passport_number"]
        t_content["passport_when"] = str(info["passport_when"])
        t_content["passport_who"] = str(info["passport_who"])
        t_content["born_date"] = str(info["born_date"])
        t_content["born_address"] = info["born_address"]
        t_content["category"] = info["category_value"]
        t_content["is_cert_best"] = [ "", "X" ][ info["is_cert_best"] ]

        ############################################################################ 

        exams = cn.select_all("""
        
        select

            concat(name, case when is_rsreu then ' (РГРТУ)' else '' end) as name,
            rating
            
        from examine_full
        where
        
            aid = %d
            and
            not yes
            
        """ % aid)

        t_content["examine"] = \
        [
            { "name" : ex["name"], "rating" : ex["rating"] }
            for ex in exams
        ]

        ############################################################################ 

        t_content["achievement"] = []
        t_ach = t_content["achievement"]

        if info["is_publ"]: t_ach.append("Имеются публикации")
        if info["is_cert_best"]: t_ach.append("Аттестат с отличием")
        if info["is_diplom_best"]: t_ach.append("Диплом с отличием")
        if info["achievementxcatx1"] > 1: t_ach.append(info["achievementxcatx1_value"])
        if info["achievementxcatx2"] > 1: t_ach.append(info["achievementxcatx2_value"])
        if info["achievementxolymp"] > 1: t_ach.append(info["achievementxolymp_value"])

        ############################################################################ 

        t_content["statement"] = \
        [
            { "value" : st["spec_value"] }
            for st in
            cn.select_all("select spec_value from statement_full where aid = %d and target = %d order by target, form, pay, priority"
                % (aid, target))
        ]

        ############################################################################ 

        doc = cn.select_one("select * from education_document_full where aid = %d and is_main" % aid)

        if doc:

            t_content["educationxdocumentxtype_value"] = doc["educationxdocumentxtype_value"]
            t_content["education_document_series"] = doc["series"]
            t_content["education_document_number"] = doc["number"]
            t_content["education_document_date"] = str(doc["date"])
            t_content["education_document_reg_number"] = doc["reg_number"]
            t_content["education_who"] = doc["who"]
            t_content["education_document_copy"] = "X" if doc["is_copy"] else ""

        else:
                
            t_content["educationxdocumentxtype_value"] = ""
            t_content["education_document_series"] = ""
            t_content["education_document_number"] = ""
            t_content["education_document_date"] = ""
            t_content["education_document_reg_number"] = ""
            t_content["education_who"] = ""
            t_content["education_document_copy"] = ""

    return ("fis", { "name" : name, "content" : content, "real_date" : real_date })

