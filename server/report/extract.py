
def extract(cn, param):

    decree = param["decree_id"]
    query = """
    
    select
    
        fio,
        spec_code_name, course, decree_value,
        lpad(date_part('day', decree_date) :: text, 2, '0') as decree_day,
        lpad(date_part('month', decree_date) :: text, 2, '0') as decree_month
        
    from abiturient_conf_statement
    where
    
        decree = %d
        
    order by fio

    """ % decree

    content = \
    {
        "abits" : cn.select_all(query)
    }

    return ("extract", content)

