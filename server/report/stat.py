
from copy import deepcopy

def stat_bachelor(cn, param):

    form = param["form_id"]

    data = []
    specs = cn.select_all("select * from contestXfull where target = 1 and form = %d order by code_name" % form)

    for spec in specs:

        stat = c_stat(cn, spec["code_name"], spec["spec_group"])
        abits = cn.select_all("select * from abiturient_conf_statement where decree > 1 and target = 1 and form = %d and contest = %d" % (form, spec["id"]))

        for abit in abits:

            stat(abit["aid"], abit["rating"], abit["is_target"], abit["privxdoc"])

        is_yes, row = stat.get()
        if is_yes: data.append(row)

    return ( "stat", { "name" : cn.get_value("form", form).lower(), "data" : data } )

class c_stat:

    def __init__(self, cn, code_name, group):

        self.ege_priv = []
        self.ege_target = []
        self.ege = []

        self.rsreu_priv = []
        self.rsreu_orphan = []
        self.rsreu_stupid = []
        self.rsreu_target = []
        self.rsreu = []

        self.__cn = cn
        self.__code_name = code_name
        self.__ex_name = list(map(lambda ind: cn.select_one("select get_examine_name_by_group(%d, %d) as name" % (group, ind))["name"], range(1, 4)))

    def __call__(self, aid, rating, is_target, privxdoc):

        def check_rsreu(name):

            row = self.__cn.select_one("select is_rsreu from examine_full where aid = %d and examineXname = %d" % (aid, name))

            return (bool(row) and row["is_rsreu"])

        is_rsreu = any(map(check_rsreu, self.__ex_name))
        is_priv = (privxdoc > 1)
        is_orphan = (privxdoc == 3) # Сирота
        is_stupid = (privxdoc in (4, 5, 6, 7)) # Инвалид

        if is_rsreu:

            if is_priv: self.rsreu_priv.append(rating)

            if is_orphan: self.rsreu_orphan.append(rating)
            elif is_stupid: self.rsreu_stupid.append(rating)
            elif is_target: self.rsreu_target.append(rating)
            else: self.rsreu.append(rating)

        else:

            if is_priv: self.ege_priv.append(rating)
            elif is_target: self.ege_target.append(rating)
            else: self.ege.append(rating)

    def get(self):

        def avg(lst):

            sm = 0
            count = 0

            for x in lst:
                
                if x:

                    sm += x
                    count += 1

            return (sm / (3. * count)) if count else 0

        ege_priv_avg = avg(self.ege_priv)
        ege_target_avg = avg(self.ege_target)
        ege_avg = avg(self.ege)

        rsreu_priv_avg = avg(self.rsreu_priv)
        rsreu_orphan_avg = avg(self.rsreu_orphan)
        rsreu_stupid_avg = avg(self.rsreu_stupid)
        rsreu_target_avg = avg(self.rsreu_target)
        rsreu_avg = avg(self.rsreu)

        ege_priv_count = len(self.ege_priv)
        ege_target_count = len(self.ege_target)
        ege_count = len(self.ege)

        rsreu_priv_count = len(self.rsreu_priv)
        rsreu_target_count = len(self.rsreu_target)
        rsreu_count = len(self.rsreu)

        count = sum((ege_priv_count, ege_target_count, ege_count, rsreu_priv_count, rsreu_target_count, rsreu_count))

        row = \
        [
            self.__code_name,
            [ ege_priv_avg, ege_target_avg, ege_avg ],
            [ rsreu_priv_avg, rsreu_stupid_avg, rsreu_orphan_avg, rsreu_target_avg ],
            [ count, ege_priv_count, rsreu_priv_count, ege_target_count, rsreu_target_count, ege_count, rsreu_count ]
        ]

        return (count > 0, deepcopy(row))

