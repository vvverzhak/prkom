
# TODO Костыль на костыле и костылем погоняет

from copy import deepcopy
import lib

main_group = None

def get_abits(cn, target, form, pay):

    abits = {}
    raw_abits = cn.select_all("select * from abiturient_conf_statement where target = %d and form = %d and pay = %d and not is_woexamine" % (target, form, pay))

    for raw_abit in raw_abits:

        aid = raw_abit["id"]

        if aid in abits:

            abit = abits[aid]

        else:

            abits[aid] = { "spec" : {}, "exam" : {}, "group" : {}, "spec_1" : "", "spec_2" : "", "spec_3" : "", "priority" : 4, "comment" : None }
            abit = abits[aid]

            slow_abit = cn.select_one("select is_education_document_main_copy_marker, achievement_bachelor, achievement_master, exam_1, exam_2, exam_3, exam_4, exam_5, exam_6, exam_7, exam_8, group_1, group_2, group_3, group_4, group_5 from abiturient_full_slow where id = %d" % aid)

            abit["fio"] = raw_abit["fio"]
            abit["is_copy"] = slow_abit["is_education_document_main_copy_marker"]
            abit["additional"] = slow_abit["achievement_%s" % ("bachelor" if target in (1, 2) else "master")]
            abit["is_stupid"] = raw_abit["is_stupid"]
            abit["is_target"] = raw_abit["is_target"]

            for eid in range(8): abit["exam"][eid + 1] = slow_abit["exam_%d" % (eid + 1)]
            for gid in range(5): abit["group"][gid + 1] = slow_abit["group_%d" % (gid + 1)]

        if raw_abit["decree"] > 1: abit["priority"] = raw_abit["priority"]
        abit["spec"][raw_abit["priority"]] = raw_abit["spec_code_name"]
        abit["spec_%d" % raw_abit["priority"]] = raw_abit["spec_code"]

    return abits

def get_spec(cn, target, specs, code_name):
    
    global main_group

    info = cn.select_one("select spec_group, code_name from contestXfull where code_name = '%s'" % code_name)
    group = main_group if main_group else info["spec_group"] 

    if code_name in specs:
        
        spec = specs[code_name]

    else:
        
        specs[code_name] = { "name" : code_name, "stupid" : [], "target" : [], "other" : [] }
        spec = specs[code_name]

        for ind in (1, 2, 3):

            name = cn.select_one("select get_examine_name_by_group(%d, %d) as name" % (group, ind))["name"]
            spec["exam_%d" % ind] = cn.select_one("select value from examineXname where id = %d" % name)["value"] if name else ""

    if group == 1: names = [ 1, 2, 3 ]
    elif group == 2: names = [ 1, 4, 3 ]
    elif group == 3: names = [ 4, 5, 3 ]
    elif group == 4: names = [ 6, 7, 3 ]
    elif group == 5: names = [ 8, None, None ]

    return (spec, names, group)

def get_specs(cn, target, abits, is_evolution):

    specs = {}
    is_master = (target > 2)

    for aid in abits:

        abit = abits[aid]

        for priority in abit["spec"]:

            spec, names, group = get_spec(cn, target, specs, abit["spec"][priority])

            if abit["is_stupid"] and priority == 1: category = "stupid"
            elif abit["is_target"] and priority == 1: category = "target"
            else: category = "other"
            
            if is_evolution and abit["priority"] <= priority:

                abit["spec_%d" % priority] = ""
                abit["comment"] = "В конкурсе не участвует"
                continue

    for aid in abits:

        abit = abits[aid]

        for priority in abit["spec"]:

            spec, names, group = get_spec(cn, target, specs, abit["spec"][priority])

            if abit["is_stupid"] and priority == 1: category = "stupid"
            elif abit["is_target"] and priority == 1: category = "target"
            else: category = "other"
            
            if is_evolution and abit["priority"] <= priority:

                continue

            spec[category].append(deepcopy(abit))
            new_abit = spec[category][-1]

            for eid in range(3):

                new_abit["ex_%d" % (eid + 1)] = abit["exam"][names[eid]] if names[eid] else ""

            new_abit["rating"] = abit["group"][group]

    return specs

def sorting_abits(specs):

    for code_name in specs:

        spec = specs[code_name]

        def key(abit):
            
            rating = abit["rating"]
            ex_1 = abit["ex_1"]
            ex_2 = abit["ex_2"]
            ex_3 = abit["ex_3"]

            cond = lambda value: value if value else 0

            return ( cond(rating), cond(ex_1), cond(ex_2), cond(ex_3) )

        spec["stupid"].sort(key = key, reverse = True)
        spec["target"].sort(key = key, reverse = True)
        spec["other"].sort(key = key, reverse = True)

        def num(category):

            count = 1

            for abit in spec[category]:
            
                abit["id"] = count
                count += 1

        num("stupid")
        num("target")
        num("other")

def sorting_specs(specs, is_evolution):

    specs.sort(key = lambda spec: spec["name"])
    real_specs = []

    for spec in specs:

        if is_evolution and len(spec["other"]) == 0: continue

        real_specs.append(spec)

    return real_specs

def contest(cn, param):

    target = param["target_id"]
    form = param["form_id"]
    pay = param["pay_id"]
    is_evolution = param["is_evolution"]

    target_name = cn.get_value("target", target)
    form_name = cn.get_value("form", form)
    pay_name = cn.get_value("pay", pay)

    global main_group
    main_group = 5 if target in (3, 4) else None

    content = \
    {
        "name" : "%s / %s / %s" % (target_name, form_name, pay_name),
        "data" : []
    }

    abits = get_abits(cn, target, form, pay)
    specs = get_specs(cn, target, abits, is_evolution)
    sorting_abits(specs)

    for code_name in specs: content["data"].append(specs[code_name])

    content["data"] = sorting_specs(content["data"], is_evolution)
    
    if is_evolution: tname = "contest_evolution"
    else: tname = "contest"

    return (tname, content)

