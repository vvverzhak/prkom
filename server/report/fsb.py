
import lib

def foreigner(cn, param):

    is_ebola = param["is_ebola"]
    passport_country_where = "passport_country %s" % [ "> 1", "in (146, 202, 115, 57, 58, 165, 236)" ][ is_ebola ]
    
    order = "fio"

    rows = cn.select_all("""
    select
        
        %s, fio, passport_country_value, passport

    from abiturient_enrolment

    where

        (%s)

    order by %s
    """ % (lib.id_column(order), passport_country_where, order))

    content = \
    {
        "name" : [ "", "(Нигерия, Сьерра-Леоне, Либерия и абсолютно все Гвинеи)" ][ is_ebola ],
        "data" : rows
    }

    return ( "foreigner", content )

