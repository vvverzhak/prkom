
import lib

def examine(cn, param):
 
    eind = param["examine"]
    begin_date = param["begin_date"]
    end_date = param["end_date"]
    early_examines = param["early_examines"]

    content = \
    {
        "name" : cn.select_one("select value from examineXname where id = %d" % eind)["value"],
        "data" : \
        [
            { "name" : "Очная", "abits" : [] },
            { "name" : "Очно-заочная / Бюджет", "abits" : [] },
            { "name" : "Очно-заочная / Внебюджет", "abits" : [] },
            { "name" : "Заочная / Бюджет", "abits" : [] },
            { "name" : "Заочная / Внебюджет", "abits" : [] },
            { "name" : "Остальные", "abits" : [] }
        ]
    }
    count = [ 1, 1, 1, 1, 1, 1 ]

    early_where = "True"

    for early in early_examines:

        if early[1]: early_where += " and id in (select aid from examine where examineXname = %d and not yes)" % early[0]
        else: early_where += " and id in (select aid from examine where examineXname = %d and yes)" % early[0]

    rows = cn.select_all("""

    select

        id, reg_code, fio, passport

    from abiturient_conf
    where

        date >= '%s' and date <= '%s'
        and
        id in (select aid from examine where examineXname = %d and yes)
        and
        %s

    order by fio

    """ % ( begin_date, end_date, eind, early_where ))

    for abit in rows:

        aid = abit["id"]

        if cn.select_one("select count(*) as cnt from statement_full where aid = %d and form = 1" % aid)["cnt"] > 0: ind = 0
        elif cn.select_one("select count(*) as cnt from statement_full where aid = %d and form = 2 and pay = 1" % aid)["cnt"] > 0: ind = 1
        elif cn.select_one("select count(*) as cnt from statement_full where aid = %d and form = 2 and pay = 2" % aid)["cnt"] > 0: ind = 2
        elif cn.select_one("select count(*) as cnt from statement_full where aid = %d and form = 3 and pay = 1" % aid)["cnt"] > 0: ind = 3
        elif cn.select_one("select count(*) as cnt from statement_full where aid = %d and form = 3 and pay = 2" % aid)["cnt"] > 0: ind = 4
        else: ind = 5

        content["data"][ind]["abits"].append([ count[ind], abit["reg_code"], abit["fio"], abit["passport"] ])
        count[ind] += 1

    return ("examine", content)

