
import lib

def report(cn, param):

    is_three = param["is_three"]

    content = \
    {
        "target_form" : [ table(cn, target, form, is_three) for target, form in lib.target_form_first_course ]
    }

    return ("today_table", content)

def table(cn, target, form, is_three):

    content = \
    {
        "name" : "Количество поданных заявлений",
        "target" : [ "бакалавриат", "бакалавриат (второй и последующий курсы)", "магистратура", "магистратура (второй и последующий курсы)" ] [ target - 1 ],
        "form" : [ "очная", "очно - заочная", "заочная" ] [ form - 1 ],
        "priority_num" : [ "первый приоритет", "три приоритета"][is_three],
        "faculty" : [],
        "all" : get_all(cn, target, form, is_three)
    }

    ############################################################################ 
    # Группы направлений

    content["spec_group"] = \
    [
        [ "технических направлений" ],
        [ "экономических направлений" ],
        [ "гуманитарных направлений" ],
        [ "творческих направлений" ]
    ]
    t_content = content["spec_group"]

    for ind in range(len(t_content)):
        
        t_content[ind] += get_group(cn, target, form, ind + 1, is_three)

    ############################################################################ 
    # Факультеты, направления и специальности

    faculties = cn.select_all("""
    
    select
        
        faculty as id, faculty_value_full as value
        
    from contestXfull
    where
    
        target = %d and form = %d and faculty > 1
        
    group by faculty, faculty_value_full
    order by faculty
    
    """ % (target, form))

    for faculty in faculties:

        content["faculty"].append([])
        t_content_faculty = content["faculty"][-1]
        t_content_faculty += [ faculty["value"] ] + get_faculty(cn, target, form, faculty["id"], is_three) + [ [] ]

        specs = cn.select_all("""

        select

            spec as id, code_name as value

        from contestXfull
        where

            target = %d and form = %d and faculty = %d

        group by spec, code_name, code
        order by code

        """ % (target, form, faculty["id"]))

        for spec in specs:

            t_content_faculty[7].append([ spec["value"] ])
            t_content = t_content_faculty[7][-1]

            t_content += get_spec(cn, target, form, spec["id"], is_three)

    return content

############################################################################ 
# Удобные замыкания для выборки

get_all = lambda cn, target, form, is_three: get(cn, target, form, is_three, "True")
get_group = lambda cn, target, form, group, is_three: get(cn, target, form, is_three, "spec_group = %d" % group)
get_faculty = lambda cn, target, form, faculty, is_three: get(cn, target, form, is_three, "faculty = %d" % faculty)
get_spec = lambda cn, target, form, spec, is_three: get(cn, target, form, is_three, "spec = %d" % spec)

############################################################################ 
# Выборка

def get_base(cn, target, form, is_three, where, table_name = "abiturient_conf_statement"):

    priority_where = [ "priority = 1", "True" ][ is_three ]

    count = cn.select_one("""
    
    select
    
        count(*) as cnt
    
    from %s
    where

        target = %d and form = %d and %s and %s

    """ % (table_name, target, form, priority_where, where))[ "cnt" ]

    return count

def get(cn, target, form, is_three, where):

    __get = lambda where_1, is_three: get_base(cn, target, form, is_three, "(%s) and (%s)" % (where, where_1))

    content = \
    [
        __get("pay = 1", is_three),
        __get("pay = 1 and is_stupid", False),
        __get("pay = 1 and is_target_war", False),
        __get("pay = 1 and is_target_power", False),
        __get("pay = 2", is_three),
        get_base(cn, target, form, is_three, "(%s) and (%s)" % (where, "pay = 1 and not is_education_document_main_copy"), "abiturient_conf_statement_slow")
    ]

    return content

