
import lib

def report(cn, param):

    def fn(where):
            
        rows = cn.select_all("select %s, fio from abiturient_conf_slow where %s" % (lib.id_column("fio"), where))
        if not rows: rows = []

        return rows

    return ("today_fio",
    {
        "woexamine" : fn("is_woexamine"),
        "stupid" : fn("is_stupid"),
        "target" : fn("is_target"),
        "other" : fn("not (is_woexamine or is_stupid or is_target)")
    })

