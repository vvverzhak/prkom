
def additional_where(with_original = None):

    where = "True"

    if with_original:
        
        where += " and (not is_education_document_main_copy)"

    return where

def additional_column(with_telephone = None):

    column = \
    """
        %s as telephone_full
    """ % (
        ["''", "telephone_full"][with_telephone]
    )

    return column

def list_value(cn, list_name, id, column_name = "value"):

    value = cn.select_one("select %s as value from %s where id = %d" % (column_name, list_name, id))["value"]

    return value

