
import lib

def wo_priority(cn, param):

    content = \
    {
        "name" : "Без приоритетов",
        "title" : [ "№", "Рег. код", "ФИО", "Дата подачи заявления", "" ],
        "data" : []
    }

    abits = cn.select_all("select %s, reg_code, fio, my_printable_date_dmy(date) as date from abiturient_conf where id not in (select aid from statement) order by fio" % lib.id_column("fio"))

    for abit in abits:

        content["data"].append([ abit["id"], abit["reg_code"], abit["fio"], abit["date"], "" ])

    return ( "list", content )

