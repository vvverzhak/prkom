
import itertools as it

def region(cn, param):

    content = \
    {
        "name" : "Количество зачисленных по регионам",
        "title" : [ "Регион", "Цель поступления", "Форма обучения", "Форма оплаты", "Количество" ],
        "data" : []
    }

    targets = cn.select_all("select * from target order by id")
    forms = cn.select_all("select * from form order by id")
    pays = cn.select_all("select * from pay order by id")

    data = content["data"]

    ############################################################################ 
    # Рязань

    for target, form, pay in it.product(targets, forms, pays):

        data.append([ "Рязань", target["value"], form["value"], pay["value"], get(cn, target["id"], form["id"], pay["id"], ryazan_where) ])

    ############################################################################ 
    # Рязанская область

    for target, form, pay in it.product(targets, forms, pays):

        data.append([ "Рязанская область", target["value"], form["value"], pay["value"], get(cn, target["id"], form["id"], pay["id"], ryazan_region_where) ])

    ############################################################################ 
    # Прочие регионы

    regions = cn.select_all("select id, value from region where id > 1 order by id")

    for region in regions:

        __where = lambda: where(region["id"])
        data.append([ region["value"], target["value"], form["value"], pay["value"], get(cn, target["id"], form["id"], pay["id"], __where) ])

    return ("list", content)

ryazan_where = lambda: "address_city like '%Рязань%'"
ryazan_region_where = lambda: "address_base_region = 1 and address_city not like '%Рязань%'"
other_region_where = lambda: "address_base_region > 1"
where = lambda rid: "address_base_region = %d" % rid

get = lambda cn, target, form, pay, where_func: cn.select_one("""
    
        select
        
            count(*) as cnt
            
        from abiturient_conf_statement
        where

            target = %d
            and
            form = %d
            and
            pay = %d
            and
            decree > 1
            and
            %s

        """ % (target, form, pay, where_func()))["cnt"]

