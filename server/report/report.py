
import config, db, lib.private

class c_report:

    def __init__(self, name, elem, user_name = [], user_faculty = [], min_user_level = 3, ui_class = None, is_first = False):

        self.name = name
        self.__elem = elem
        self.user_name = user_name
        self.user_faculty = user_faculty
        self.min_user_level = min_user_level
        self.__ui_class = ui_class
        self.__is_first = is_first

    def tree(self, user_name, user_faculty, user_level):

        if type(self.__elem) == list:

            tree = {}

            for elem in self.__elem:

                if \
                (
                    (
                        (len(elem.user_name) == 0 or user_name in elem.user_name)
                        and
                        (len(elem.user_faculty) == 0 or user_faculty in elem.user_faculty)
                        and
                        (user_level <= elem.min_user_level)
                    )
                    or
                    user_level <= 3
                ):
                    
                    tree[elem.name] = elem.tree(user_name, user_faculty, user_level)

            return tree

        else:

            return "%s" % self.__ui_class

    def generate(self, index, param, user_name, user_faculty, user_level):

        if \
        (
            (
                (len(self.user_name) == 0 or user_name in self.user_name)
                and
                (len(self.user_faculty) == 0 or user_faculty in self.user_faculty)
                and
                (user_level <= self.min_user_level)
            )
            or
            user_level <= 3
        ):

            if type(self.__elem) == list:

                for elem in self.__elem:

                    if elem.name == index[0]:

                        return elem.generate(index[1 : ], param, user_name, user_faculty, user_level)

            else:
            
                return self.__base_gen(param)

        return (False, { "file" : None })

    def __base_gen(self, param):

        with db.c_db() as cn:

            template_fname, content = self.__elem(cn, param)

            if template_fname == None:

                return (False, {})

            tm = lib.c_current_date_time()
            content["dt"] = \
            {
                "date" : tm.date,
                "time" : tm.time
            }

            content["main_secretary"] = \
            {
                "fio" : config.main_secretary.fio,
                "fio_reversed" : config.main_secretary.fio_reversed,
                "full_name" : config.main_secretary.full_name
            }

        return (True, { "file" : lib.private.render(config.catalog.template_report, template_fname, content) })

