
def good(cn, param):

    target = param["target"]
    form = param["form"]
    report_type = param["report_type"]

    content = \
    {
        "target" : cn.select_one("select value from target where id = %d" % target)["value"],
        "form" : cn.select_one("select value from form where id = %d" % form)["value"],
        "data" : []
    }

    if report_type == 0:
    
        content["name"] = "Целевики - ОПК"
        where = "purpose = 2" 
        
    elif report_type == 1:
        
        content["name"] = "Целевики - ОВ"
        where = "purpose = 3" 

    elif report_type == 2:

        content["name"] = "Особое право приема"
        where = "is_stupid"

    elif report_type == 3:

        content["name"] = "Прием без вступительных испытаний"
        where = "is_woexamine"

    rows = cn.select_all("""

    select
    
        id as aid, my_printable_date_dmy(date) as date, reg_code, fio, concat(category_value, ' (', category_comment, ') ') as target

    from abiturient_conf where %s

    order by reg_code

    """ % where)

    id = 1

    for row in rows:

        aid = row["aid"]

        spec = cn.select_one("""
        
        select
        
            spec_code_name,
            spec_group,
            rating
            
        from statement_full
        where
        
            aid = %d
            and
            target = %d and form = %d
            and
            pay = 1 and priority = 1
            
        """ % (aid, target, form))

        if spec:
            
            content["data"].append({})
            t_content = content["data"][-1]

            t_content["id"] = id
            t_content["reg_code"] = row["reg_code"]
            t_content["date"] = row["date"]
            t_content["fio"] = row["fio"]
            t_content["target"] = row["target"]
            t_content["spec"] = spec["spec_code_name"]
            t_content["rating"] = spec["rating"]

            id += 1

    return ( "target_indulgence", content )

def bad(cn, param):

    content = \
    {
        "name" : "Ошибки (целевой набор)",
        "title" : [ "№", "Рег. код", "ФИО", "Специальность", "Приоритет" ],
        "data" : []
    }

    count = 1
    abits = cn.select_all("select * from abiturient_conf_statement where (is_teach_war_center or spec_code = '11.03.01') and priority > 1 and target = 1 and form = 1 and pay = 1")

    for abit in abits:

        content["data"].append([ count, abit["reg_code"], abit["fio"], abit["spec_value"], abit["priority"] ])
        count += 1

    return ( "list", content )

