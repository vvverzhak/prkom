
# TODO Костыль на костыле
# TODO Отладить шаблоны - нормальные фамилии

def enrolment(cn, param):

    target = param["target_id"]
    form = param["form_id"]
    pay = param["pay_id"]
    decree = param["decree_id"]
    reason = param["reason"]

    ############################################################################ 
    # Причины

    template = "enrolment%s" % ( "_target" if reason in (1, 2) else "" )

    reason_name = \
    [
        "приема лиц по общему конкурсу",
        "целевого приема (ОПК)",
        "целевого приема (ОВ)",
        "целевого приема (УВЦ)",
        "тотального приема"
    ][reason]

    reason_where = \
    [
        "(not is_woexamine and not is_target) or (priority > 1 or pay = 2)",
        "is_target_war and priority = 1 and pay = 1",
        "is_target_power and priority = 1 and pay = 1",
        "is_teach_war_center and priority = 1 and pay = 1",
        "True"
    ][reason]

    commentary = \
    [
        "fio",
        "category_value",
        "category_comment",
        "fio",
        "fio"
    ][reason]

    ############################################################################ 

    fclt = {}
    abits = cn.select_all("select * from abiturient_conf_statement where (%s) and target = %d and form = %d and pay = %d and decree = %d" % (reason_where, target, form, pay, decree))

    for abit in abits:

        faculty = abit["faculty"]
        spec = abit["contest"]

        if faculty not in fclt: fclt[faculty] = { "faculty" : abit["faculty_value"], "data" : {} }
        t_fclt = fclt[faculty]

        if spec not in t_fclt["data"]: t_fclt["data"][spec] = { "spec" : abit["spec_code_name"], "data" : [] }
        t_spec = t_fclt["data"][spec]

        t_spec["data"].append({ "fio" : abit["fio"], "rating" : abit["rating"], "commentary" : abit[commentary] })

    ############################################################################ 

    data = []
    num = 0

    for f in (3, 4, 5, 6, 7, 2, 8):

        if f not in fclt: continue

        t_fclt = fclt[f]
        data.append({ "faculty" : t_fclt["faculty"], "data" : [], "num" : 0 })
        t_fclt_data = data[-1]["data"]

        for s in t_fclt["data"]:

            t_spec = t_fclt["data"][s]
            t_fclt_data.append({ "spec" : t_spec["spec"], "data" : [] })
            t_spec["data"].sort(key = lambda abit: abit["fio"])
            t_fclt_data[-1]["data"] = t_spec["data"]
            data[-1]["num"] += len(t_spec["data"])

            count = 1

            for a in t_fclt_data[-1]["data"]:

                a["id"] = count
                count += 1

        num += data[-1]["num"]

    ############################################################################ 

    return ( template, { "reason" : reason_name, "data" : data, "num" : num } )

