
deanery_card = lambda cn, param: ( "deanery_card", generate(cn, param) )
secret_card = lambda cn, param: ( "secret_card", generate(cn, param) )

def generate(cn, param):

    faculty = param["faculty_id"]
    pay = param["pay_id"]

    content = \
    {
        "faculty" : cn.get_value("faculty", faculty),
        "data" : []
    }

    abits = cn.select_all("""

    select

        *

    from abiturient_conf_statement

    where

        faculty = %d
        and
        decree > 1
        and pay = %d

    order by fio

    """ % (faculty, pay))

    for abit in abits:

        aid = abit["id"]

        content["data"].append({})
        t_abit = content["data"][-1]

        t_abit["fio"] = abit["fio"]
        t_abit["spec"] = abit["spec_code_name"]
        t_abit["address"] = abit["address"]
        t_abit["telephone"] = abit["telephone"]
        t_abit["mobile"] = abit["mobile"]
        t_abit["born_date"] = str(abit["born_date"])
        t_abit["born_address"] = abit["born_address"]
        t_abit["passport_country_value"] = abit["passport_country_value"]
        t_abit["lang_value"] = abit["lang_value"]
        t_abit["is_married_marker_large"] = abit["is_married_marker_large"]
        t_abit["common_work"] = abit["common_work"]
        t_abit["sports_category"] = abit["sports_category"]
        t_abit["is_course_marker_word"] = abit["is_course_marker_word"]

        ############################################################################ 
        # Документ об образовании

        doc = cn.select_one("""

        select

            description

        from education_document_full

        where

            aid = %d
            and
            is_main

        """ % aid)

        t_abit["education_description"] = doc["description"]

        ############################################################################ 
        # Экзамены

        for eid in range(1, 4):

            name = cn.select_one("select get_examine_name_by_group(%d, %d) as name" % (abit["spec_group"], eid))["name"]
            rus_name = cn.get_value("examineXname", name)

            t_abit["examine_%d" % eid] = rus_name
            t_abit["rating_%d" % eid] = abit["ex_%d" % eid]

        ############################################################################ 
        # Родители

        t_abit["parent"] = []

        parents = cn.select_all("""
        
        select
        
            *
            
        from relation_full
        
        where
        
            aid = %d
            
        order by relationXtype
        
        """ % aid)

        for parent in parents:

            t_abit["parent"].append({})
            t_parent = t_abit["parent"][-1]

            t_parent["relation_type_value"] = parent["relation_type_value"]
            t_parent["fio"] = parent["fio"]
            t_parent["work_description"] = parent["work_description"]
            t_parent["address"] = parent["address"]
            t_parent["telephone"] = parent["telephone"]
            t_parent["mobile"] = parent["mobile"]

    return content

