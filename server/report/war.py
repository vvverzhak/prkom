
def war(cn, param):

    target = param["target_id"]
    form = param["form_id"]
    pay = param["pay_id"]

    content = \
    {
        "target" : cn.get_value("target", target),
        "form" : cn.get_value("form", form),
        "pay" : cn.get_value("pay", pay),
        "faculties" : {}
    }
    fclts = content["faculties"]

    abits = cn.select_all("""

    select
    
        *

    from

        abiturient_conf_statement
        
    where
    
        decree > 1
        and
        target = %d
        and
        form = %d
        and
        pay = %d
        and
        sex = 1

    order by faculty, spec_code, fio

    """ % (target, form, pay))
    
    for abit in abits:

        fname = abit["faculty_value"]
        if fname not in fclts: fclts[fname] = {}
        fclt = fclts[fname]
        
        sname = abit["spec_code_name"]
        if sname not in fclt: fclt[sname] = []
        spec = fclt[sname]

        spec.append(
        {
            "id" : len(spec) + 1,
            "fio" : abit["fio"],
            "born_date" : str(abit["born_date"]),
            "passport" : abit["passport"],
            "passport_who" : abit["passport_who"],
            "passport_when" : str(abit["passport_when"]),
            "address" : abit["address"],
            "telephone" : abit["telephone"],
            "mobile" : abit["mobile"]
        })

    return ( "war", content )

