
def hostel(cn, param):

    target = param["target_id"]
    form = param["form_id"]
    pay = param["pay_id"]

    data = {}
    abits = cn.select_all("""

    select

        fio, telephone_full, faculty_value_full

    from

        abiturient_conf_statement

    where

        is_hostel
        and
        target = %d
        and
        form = %d
        and
        pay = %d
        and
        decree > 1

    order by fio

    """ % (target, form, pay))

    for abit in abits:

        faculty_name = abit["faculty_value_full"]

        if faculty_name not in data:

            data[faculty_name] = \
            {
                "name" : faculty_name,
                "count" : 1,
                "data" : []
            }

        faculty = data[faculty_name]

        faculty["data"].append(
        {
            "id" : faculty["count"],
            "fio" : abit["fio"],
            "telephone_full" : abit["telephone_full"]
        })

        faculty["count"] += 1

    content = \
    {
        "target" : cn.get_value("target", target),
        "form" : cn.get_value("form", form),
        "pay" : cn.get_value("pay", pay),
        "data" : list(data.values())
    }

    return ("hostel", content)

