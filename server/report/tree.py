
from report.report import c_report

from report.today.table import report as today_table
from report.today.fio import report as today_fio
import report.fsb as fsb
import report.fis as fis
import report.target_indulgence as target_indulgence
import report.wo_priority as wo_priority
import report.examine as examine
import report.contest as contest
import report.enrolment as enrolment
import report.extract as extract
import report.hostel as hostel
import report.deanery_card as deanery_card
import report.war as war
import report.region as region
import report.stat as stat
import report.forms as forms

############################################################################ 

#
# Функция генерации отчета:
#
#   Имеет параметры: ( соединение (cn), список_параметров (param) )
#
#   Возвращает: ( имя_файла_шаблона_без_расширения, содержимое_(content)_в_виде_словаря )
#

tree = c_report(name = None, min_user_level = 5,
    elem = \
    [
        c_report(name = "Ежедневный отчет",
        elem = \
        [
            c_report(name = "Таблицы", elem = today_table, ui_class = "today_table"),
            c_report(name = "ФИО", elem = today_fio)
        ]),
        c_report(name = "Комитет глубинного бурения",
        elem = \
        [
            c_report(name = "Иностранцы", elem = fsb.foreigner, ui_class = "foreigner")
        ]),
        c_report(name = "Целевики и льготники",
        elem = \
        [
            c_report(name = "Правильные", elem = target_indulgence.good, ui_class = "target_indulgence"),
            c_report(name = "Ошибки", elem = target_indulgence.bad)
        ]),
        c_report(name = "Карточки",
        elem = \
        [
            c_report(name = "Карточки для деканатов", elem = deanery_card.deanery_card, ui_class = "deanery_card"),
            c_report(name = "Секретные карточки", elem = deanery_card.secret_card, ui_class = "deanery_card")
        ]),
        c_report(name = "ФИС ЕГЭ", elem = fis.fis, ui_class = "fis"),
        c_report(name = "Без приоритетов", elem = wo_priority.wo_priority),
        c_report(name = "Экзамены", elem = examine.examine, ui_class = "examine"),
        c_report(name = "Конкурсные списки", elem = contest.contest, ui_class = "contest"),
        c_report(name = "Список зачисленных", elem = enrolment.enrolment, ui_class = "enrolment"),
        c_report(name = "Выписки из приказов", elem = extract.extract, ui_class = "extract"),
        c_report(name = "Общежития", elem = hostel.hostel, ui_class = "hostel"),
        c_report(name = "Военно-учетный стол", elem = war.war, ui_class = "war"),
        c_report(name = "Зачисленные по регионам", elem = region.region),
        c_report(name = "Статистика по бакалавриату", elem = stat.stat_bachelor, ui_class = "stat"),
        c_report(name = "2.6", elem = forms.form_2_6, ui_class = "form_2_6"),
    ])

############################################################################# 

