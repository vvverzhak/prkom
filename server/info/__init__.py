
import unittest
import config, lib.private, lib.server, db

def process(content):
    
    with db.c_db() as cn:

        return ( True,
            {
                "version"			:	config.version,
                "code_name"			:	config.code_name,
                "date"				:	config.date,
                "chat_port"                     :       config.chat_port,
                "report_port"			:	config.server.report.port,
                "enroll_port"			:	config.server.enroll.port,
                "remote_control_port"	        :	config.server.remote_control.port,
                "db_port"			:	config.db.port,
                "db_name"			:	config.db.name,
                "db_login"			:	lib.private.ip_to_login(cn, content["reciever"][0]),
                "level"                         :       lib.private.level(cn, content["reciever"][0])
	    })

class c_server:
    
    def start():	lib.server.start(config.server.info, process)
    def stop():		lib.server.stop(config.server.info)
    def status():	lib.server.status(config.server.info)
    
    class c_test(unittest.TestCase):
        
        def test_info(self):

            for x in range(256):
            
                content = { "reciever" : ( "192.168.33.%d" % x, "1111" ) }
                print(content["reciever"], process(content)[1]["db_login"])

    def test():

        test = c_server.c_test()

        test.test_info()

    def utest():

        runner = unittest.TextTestRunner(verbosity = 2)
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(c_server.c_test)
        runner.run(suite)

