
import time, random as rnd
import config, lib

def rnd_enum(count):
    
    return rnd.randint(1, count - 1)

rnd_date = lambda: "%d-%d-%d" % (rnd.randint(1990, time.localtime().tm_year), rnd.randint(1, 12), rnd.randint(1, 28))
rnd_mobile = lambda: str(rnd.randint(100000000, 999999999))
rnd_bool = lambda: bool(rnd.randint(False, True))

def insert(cn, table_name, param):
    
    insert_query_field_names = ""
    insert_query_values = ""
    
    for p_name in param:
        
        if type(param[p_name]) == str:
            
            p_value = "'%s'" % (param[p_name])

        elif type(param[p_name]) == bool:
            
            p_value = [ "FALSE", "TRUE" ][ param[p_name] ]
        
        else:
            
            p_value = str(param[p_name])
            
        insert_query_field_names += ", %s" % (p_name)
        insert_query_values += ", %s" % (p_value)
        
    insert_query = "insert into %s (%s) values (%s);" % (table_name, insert_query_field_names[2 : ], insert_query_values[2 : ])
    cn.execute(insert_query)

def education_document(cn, aid, count, is_main):

    param = \
    {
        "aid" : aid,
        "educationXdocumentXtype" : rnd_enum(count["educationXdocumentXtype"]),
        "series" : str(rnd.randint(1000, 9999)),
        "number" : str(rnd.randint(100000, 999999)),
	"reg_number" : str(rnd.randint(100000, 999999)),
        "date" : rnd_date(),
        "who" : "Рязанский государственный радиотехнический университет",
        "educationXdocumentXoriginalXcopy" : rnd_enum(count["educationXdocumentXoriginalXcopy"]),
        "is_medal" : rnd_bool(),
        "is_main" : is_main,
        "spec" : "Мастер по выращиванию огурцов",
        "qualification" : "Кандидат сурдопереводческих наук"
    }

    insert(cn, "education_document", param)

def statement(cn, aid, count):

    for target, form, pay in lib.target_form_pay:

        contests = cn.select_all("select id from contest where target = %d and form = %d and pay = %d" % (target, form, pay))

        if len(contests):

            max_priority = 3 if target in (1, 3) else 1

            for priority in range(1, max_priority + 1):

                cnt = rnd.choice(contests)

                param = \
                {
                    "aid" : aid,
                    "contestXfull" : cnt["id"],
                    "decree" : 1,
                    "priority" : priority,
                    "course" : rnd.randint(1, 6)
                }

                insert(cn, "statement", param)

def examine(cn, aid, count):

    for ex in range(1, count["examineXname"]):
        
        param = \
        {
            "aid" : aid,
            "examineXtype" : rnd_enum(count["examineXtype"]),
            "examineXname" : ex,
            "year" : rnd_date(),
            "yes" : rnd_bool(),
            "rating" : rnd.randint(1, 100)
        }

        insert(cn, "examine", param)

def relation(cn, aid, count):

    param = \
    {
        "aid" : aid,
        "relationXtype" : rnd_enum(count["relationXtype"]),
        "relationXplaceXtype" : rnd_enum(count["relationXplaceXtype"]),
	"surname" : "Иванова",
	"name" : "Степанида",
	"patrname" : "Петровна",
	"address_country" : rnd_enum(count["country"]),
        "address_base_region" : rnd_enum(count["region"]),
        "address_region_additional" : "Карачумская область",
	"address_city" : "Рязань",
	"address_cityXtype" : rnd_enum(count["cityXtype"]),
	"address_district" : "Московский",
	"address_street" : "ул. Гагарина",
	"address_home" : str(rnd.randint(1, 20)),
	"address_building" : str(rnd.randint(1, 7)),
	"address_flat" : str(rnd.randint(1, 100)),
	"address_index" : str(rnd.randint(100000, 999999)),
	"telephone_code" : str(rnd.randint(100, 10000)),
	"telephone_number" : str(rnd.randint(100000, 9999999)),
	"mobile" : rnd_mobile(),
	"work_place" : "Хлебзавод",
	"work_post" : "Секретарь",
	"work_telephone_code" : str(rnd.randint(100, 10000)),
	"work_telephone_number" : str(rnd.randint(100000, 9999999))
    }

    insert(cn, "relation", param)

def person(cn, count, index, passport_series = None, passport_number = None):

    if not passport_series: passport_series = str(rnd.randint(1000, 9999))
    if not passport_number: passport_number = str(rnd.randint(100000, 999999))

    param = \
    {
        "reg_code_letter" : rnd_enum(count["letter"]),
        "reg_code_index" : index,
        "date" : rnd_date(),
        "status" : rnd_enum(count["status"]),
        "surname" : "Иванов",
        "surname_genitive" : "Иванова",
        "name" : "Иван",
        "name_genitive" : "Ивана",
        "patrname" : "Иванович",
        "patrname_genitive" : "Ивановича",
        "sex" : rnd_enum(count["sex"]),
        "email" : "aaa@bbb.com",
        "born_country" : rnd_enum(count["country"]),
        "born_base_region" : rnd_enum(count["region"]),
        "born_region_additional" : "Карачумская область",
        "born_city" : "Рязань",
        "born_cityXtype" : rnd_enum(count["cityXtype"]),
        "born_district" : "Московский",
        "born_date" : rnd_date(),
        "passport_country" : rnd_enum(count["country"]),
        "passport_series" : passport_series,
        "passport_number" : passport_number,
        "passport_who" : "Областной УВД",
        "passport_when" : rnd_date(),
        "address_country" : rnd_enum(count["country"]),
        "address_base_region" : rnd_enum(count["region"]),
        "address_region_additional" : "Карачумская область",
        "address_city" : "Рязань",
        "address_cityXtype" : rnd_enum(count["cityXtype"]),
        "address_district" : "Московский",
        "address_street" : "ул. Гагарина",
        "address_home" : str(rnd.randint(1, 20)),
        "address_building" : str(rnd.randint(1, 7)),
        "address_flat" : str(rnd.randint(1, 100)),
        "address_index" : str(rnd.randint(100000, 999999)),
        "telephone_code" : str(rnd.randint(100, 10000)),
        "telephone_number" : str(rnd.randint(100000, 9999999)),
        "mobile" : rnd_mobile(),
        "previous_education_term" : rnd_enum(count["term"]),
        "previous_education_course" : rnd.randint(1, 6),
        "previous_education_contest" : "Менеджер по запуску ракет",
        "previous_address_country" : rnd_enum(count["country"]),
        "previous_address_base_region" : rnd_enum(count["region"]),
        "previous_address_region_additional" : "Карачумская область",
        "previous_address_city" : "Рязань",
        "previous_address_cityXtype" : rnd_enum(count["cityXtype"]),
        "previous_address_district" : "Московский",
        "previous_address_street" : "ул. Гагарина",
        "previous_address_home" : str(rnd.randint(1, 20)),
        "previous_address_building" : str(rnd.randint(1, 7)),
        "previous_address_flat" : str(rnd.randint(1, 100)),
        "previous_address_index" : str(rnd.randint(100000, 999999)),
        "is_advantage" : rnd_bool(),
        "advantage_base" : "Абитуриент - красава",
        "category" : rnd_enum(count["category"]),
        "category_comment" : "Лапы ломит, хвост отваливается",
        "privXdoc" : rnd_enum(count["privXdoc"]),
        "achievementXolymp" : rnd_enum(count["achievementXolymp"]),
        "achievementXcatX1" : rnd_enum(count["achievementXcatX1"]),
        "achievementXcatX2" : rnd_enum(count["achievementXcatX2"]),
        "is_cert_best" : rnd_bool(),
        "is_publ" : rnd_bool(),
        "is_diplom_best" : rnd_bool(),
        "is_first" : rnd_bool(),
        "is_hostel" : rnd_bool(),
        "is_married" : rnd_bool(),
	"is_course" : rnd_bool(),
	"is_profile" : rnd_bool(),
        "lang" : rnd_enum(count["lang"]),
        "documentXreturn" : rnd_enum(count["documentXreturn"]),
	"common_work" : "Ничего",
	"sports_category" : "Отсутствует",
        "special_condition" : "Рюмка водки на столе",
        "additional_information" : "Характер стойкий, нордический"
    }

    insert(cn, "abiturient", param)
    
    rows = cn.select_one("select id from abiturient where passport_series = '%s' and passport_number = '%s'" % (param["passport_series"], param["passport_number"]))

    if len(rows) > 0:

        aid = rows["id"]

        statement(cn, aid, count)
        examine(cn, aid, count)

        for u in range(0, config.fill.relation_num):

            relation(cn, aid, count)

        for u in range(0, config.fill.education_document_num):

            education_document(cn, aid, count, u == 0)

@lib.function_description("Создание абитуриентов")
def fill(cn):

    count = dict(map(lambda name: (name, cn.select_one("select count(*) + 1 as cnt from %s;" % name)["cnt"]), 
            [ "status", "country", "region", "cityXtype", "sex", "lang", "documentXreturn", "term", "spec", "letter", "faculty", "specXgroup", "target", "form",
              "pay", "privXdoc", "purpose", "educationXdocumentXbaseXtype", "educationXdocumentXtype", "educationXdocumentXoriginalXcopy",
              "examineXtype", "examineXname", "relationXtype", "relationXplaceXtype", "category", "decree", "contest", "achievementXolymp", "achievementXcatX1",
              "achievementXcatX2" ]))
            
    person(cn, count, 1, "8888", "777777")

    for u in range(1, config.fill.num):
            
        person(cn, count, u + 1)
        if u % 5 == 0: print("Абитуриент", u, "из", config.fill.num)

