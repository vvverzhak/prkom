
import lib, db
import db.fill.lists
import db.fill.person

@lib.function_description("Заполнение базы данных случайными значениями")
def main():

    with db.c_db() as cn:

        db.fill.lists.fill(cn)
        db.fill.person.fill(cn)

