
schema = """

create table abiturient
(

    id serial,

    -- Регистрационный код
    reg_code_letter serial,            -- Факультет, где лежит дело
    reg_code_index int,	        	-- Номер дела

    -- Заполнение
    date date,				-- Дата
    status serial,			-- Статус абитуриента

    -- Имя
    surname text,			-- Фамилия (именительный падеж)
    surname_genitive text,		-- Фамилия (родительный падеж)
    name text,				-- Имя (именительный падеж)
    name_genitive text,			-- Имя (родительный падеж)
    patrname text,			-- Отчество (именительный падеж)
    patrname_genitive text,		-- Отчество (родительный падеж)

    -- Пол
    sex serial,

    -- E-mail
    email text,

    -- Рождение
    born_country serial,		-- Страна
    born_base_region serial,		-- Регион
    born_region_additional text,	-- Регион - дополнительно
    born_city text,			-- Населенный пункт
    born_cityXtype serial,		-- Тип населенного пункта
    born_district text,			-- Район
    born_date date,			-- Дата

    -- Паспорт
    passport_country serial,	        -- Гражданство
    passport_series text,		-- Серия
    passport_number text,		-- Номер
    passport_who text,			-- Кем выдан
    passport_when date,			-- Когда выдан

    -- Адрес
    address_country serial,		-- Страна
    address_base_region serial,		-- Регион
    address_region_additional text,	-- Регион - дополнительно
    address_city text,			-- Населенный пункт
    address_cityXtype serial,	        -- Тип населенного пункта
    address_district text,		-- Район
    address_street text,		-- Улица
    address_home text,			-- Дом
    address_building text,		-- Корпус
    address_flat text,			-- Квартира
    address_index text,			-- Индекс

    -- Домашний телефон
    telephone_code text,	        -- Код
    telephone_number text,	        -- Номер

    -- Мобильный телефон
    mobile text,

    -------------------------------------------------------------------------------------
    -- Предыдущее место обучения

    previous_education_term serial,     -- Семестр
    previous_education_course int,      -- Курс
    previous_education_contest text,    -- Направление подготовки / специальность

    -- Адрес
    previous_address_country serial,		-- Страна
    previous_address_base_region serial,	-- Регион
    previous_address_region_additional text,    -- Регион - дополнительно
    previous_address_city text,			-- Населенный пункт
    previous_address_cityXtype serial,	        -- Тип населенного пункта
    previous_address_district text,		-- Район
    previous_address_street text,		-- Улица
    previous_address_home text,			-- Дом
    previous_address_building text,		-- Корпус
    previous_address_flat text,			-- Квартира
    previous_address_index text,		-- Индекс

    -------------------------------------------------------------------------------------
    -- Поступление, преимущественное право и индивидуальные достижения

    category serial,                        -- Категория зачисления
    category_comment text default null,
    privXdoc serial,                        -- Документы, дающие право на льготы
    is_advantage bool,                      -- Преимущественное право на зачисление
    advantage_base text,                    -- Основание на преимущественное право на зачисление
    achievementXolymp serial,               -- Участие в олимпиаде
    achievementXcatX1 serial,               -- Категория 1
    achievementXcatX2 serial,               -- Категория 2
    is_cert_best bool,                      -- Аттестат о среднем общем образовании с отличием
    is_publ bool,                           -- Публикации
    is_diplom_best bool,                    -- Диплом о высшем образовании с отличием

    -------------------------------------------------------------------------------------

    is_first bool,	            -- Образование получает впервые?
    is_hostel bool,	            -- Нуждается в общежитии?
    is_married bool,	            -- Женат / замужем?
    is_course bool,	            -- Проходил подготовительные курсы?
    is_profile bool,	            -- Профильный класс?
    lang serial,	            -- Иностранный язык
    documentXreturn serial,         -- Способ возврата документов
    common_work text,	            -- Общественная работа, выполняемая ранее
    sports_category text,           -- Спортивный разряд
    special_condition text,         -- Специальные условия при проведении экзаменов
    additional_information text,    -- Дополнительные сведения, которые абитуриент хочет сообщить о себе

    -------------------------------------------------------------------------------------

    primary key (id),

    unique (passport_series, passport_number),

    foreign key (reg_code_letter) references letter (id) on delete cascade on update cascade,
    foreign key (status) references status (id) on delete cascade on update cascade,
    foreign key (sex) references sex (id) on delete cascade on update cascade,
    foreign key (born_country) references country (id) on delete cascade on update cascade,
    foreign key (born_base_region) references region (id) on delete cascade on update cascade,
    foreign key (born_cityXtype) references cityXtype (id) on delete cascade on update cascade,
    foreign key (passport_country) references country (id) on delete cascade on update cascade,
    foreign key (address_country) references country (id) on delete cascade on update cascade,
    foreign key (address_base_region) references region (id) on delete cascade on update cascade,
    foreign key (address_cityXtype) references cityXtype (id) on delete cascade on update cascade,
    foreign key (previous_education_term) references term (id) on delete cascade on update cascade,
    foreign key (previous_address_country) references country (id) on delete cascade on update cascade,
    foreign key (previous_address_base_region) references region (id) on delete cascade on update cascade,
    foreign key (previous_address_cityXtype) references cityXtype (id) on delete cascade on update cascade,
    foreign key (category) references category (id) on delete cascade on update cascade,
    foreign key (privXdoc) references privXdoc (id) on delete cascade on update cascade,
    foreign key (achievementXolymp) references achievementXolymp (id) on delete cascade on update cascade,
    foreign key (achievementXcatX1) references achievementXcatX1 (id) on delete cascade on update cascade,
    foreign key (achievementXcatX2) references achievementXcatX2 (id) on delete cascade on update cascade,
    foreign key (lang) references lang (id) on delete cascade on update cascade,
    foreign key (documentXreturn) references documentXreturn (id) on delete cascade on update cascade

);

"""

############################################################################ 

schema_view = """

create view abiturient_full as
select

    abit.*,

    reg_code_letter_table.value as reg_code_letter_value,
    status_table.value as status_value,
    sex_table.value as sex_value,
    born_country_table.value as born_country_value,
    born_base_region_table.value as born_base_region_value,
    born_cityXtype_table.value as born_cityXtype_value,
    passport_country_table.value as passport_country_value,
    address_country_table.value as address_country_value,
    address_base_region_table.value as address_base_region_value,
    address_cityXtype_table.value as address_cityXtype_value,
    term_table.value as previous_education_term_value,
    previous_address_country_table.value as previous_address_country_value,
    previous_address_base_region_table.value as previous_address_base_region_value,
    previous_address_cityXtype_table.value as previous_address_cityXtype_value,
    category_table.value as category_value,
    category_table.purpose as purpose,
    purpose_table.value as purpose_value,
    privXdoc_table.value as privXdoc_value,
    achievementXolymp_table.value as achievementXolymp_value,
    achievementXcatX1_table.value as achievementXcatX1_value,
    achievementXcatX2_table.value as achievementXcatX2_value,
    lang_table.value as lang_value,
    documentXreturn_table.value as documentXreturn_value,

    -- generic-поля

    case when reg_code_letter = 1 then
        ''
    else
        concat(reg_code_letter_table.value, '-', reg_code_index)
    end as reg_code,

    concat(surname, ' ', name, ' ', patrname) as fio,
    
    concat(passport_series, ' ', passport_number) as passport,

    case when born_base_region = 2 then born_region_additional else born_base_region_table.value end as born_region,
    case when address_base_region = 2 then address_region_additional else address_base_region_table.value end as address_region,
    case when previous_address_base_region = 2 then previous_address_region_additional else previous_address_base_region_table.value end as previous_address_region,

    concat(
        born_country_table.value, ', ',
        case when born_base_region = 2 then born_region_additional else born_base_region_table.value end, ', ',
        born_district, case when born_district <> '' then ' район, ' else '' end,
        case when born_cityXtype = 6 then '' else my_lower(concat(born_cityXtype_table.value, ' ')) end,
        born_city
    ) as born_address,
		
    concat(
        address_country_table.value,
        case when address_index = '' then '' else concat(', ', address_index) end,
        concat(', ', case when address_base_region = 2 then address_region_additional else address_base_region_table.value end),
        case when address_district = '' then '' else concat(', ', address_district, ' район') end,
        case when address_cityXtype = 6 then ',' else concat(', ', my_lower(address_cityXtype_table.value)) end,
        case when address_city = '' then '' else concat(' ', address_city) end,
        case when address_street = '' then '' else concat(', ', address_street) end,
        case when address_home = '' then '' else concat(', дом ', address_home) end,
        case when address_building = '' then '' else concat(', корпус ', address_building) end,
        case when address_flat = '' then '' else concat(', квартира ', address_flat) end
    ) as address,
    
    concat(
        concat(', ', case when address_base_region = 2 then address_region_additional else address_base_region_table.value end),
        case when address_district = '' then '' else concat(', ', address_district, ' район') end
    ) as address_1,
    
    concat(
        case when address_cityXtype = 6 then '' else my_lower(concat(address_cityXtype_table.value, ' ')) end, address_city
    ) as address_2,

    concat(
        '(', telephone_code, ') ', telephone_number
    ) as telephone,

    concat(
        'Моб.: ', mobile, ' / Дом.: (', telephone_code, ') ', telephone_number
    ) as telephone_full,

    passport_country = 1 as is_russia,
    category_table.purpose > 1 as is_target,
    category_table.purpose = 2 as is_target_war,
    category_table.purpose = 3 as is_target_power,
    category = 2 as is_stupid,
    category = 3 as is_woexamine,
    documentXreturn_table.id = 1 as document_return_personally,
    documentXreturn_table.id = 2 as document_return_post,
    case when documentXreturn_table.id = 1 then 'X' else '' end as document_return_personally_marker,
    case when documentXreturn_table.id = 2 then 'X' else '' end as document_return_post_marker,
    case when is_course then 'X' else '' end as is_course_marker,

    concat(
        previous_address_country_table.value,
        case when previous_address_index = '' then '' else concat(', ', previous_address_index) end,
        case when previous_address_base_region = 2 then previous_address_region_additional else concat(', ', previous_address_base_region_table.value) end,
        case when previous_address_district = '' then '' else concat(', ', previous_address_district, ' район') end,
        case when previous_address_cityXtype = 6 then ',' else concat(', ', my_lower(previous_address_cityXtype_table.value)) end,
        case when previous_address_city = '' then '' else concat(' ', previous_address_city) end,
        case when previous_address_street = '' then '' else concat(', ', previous_address_street) end,
        case when previous_address_home = '' then '' else concat(', дом ', previous_address_home) end,
        case when previous_address_building = '' then '' else concat(', корпус ', previous_address_building) end,
        case when previous_address_flat = '' then '' else concat(', квартира ', previous_address_flat) end
    ) as previous_education_address,

    case when is_course then
        case when sex = 1 then
            'Посещал'
        else
            'Посещала'
        end
    else
        case when sex = 1 then
            'Не посещал'
        else
            'Не посещала'
        end
    end as is_course_marker_word,

    case when is_married then 'X' else '' end as is_married_marker,
    case when is_married then
        case when sex = 1 then
            'Женат'
        else
            'Замужем'
        end
    else '' end as is_married_marker_large
    
from abiturient as abit
	
inner join letter as reg_code_letter_table on reg_code_letter_table.id = abit.reg_code_letter
inner join status as status_table on status_table.id = abit.status
inner join sex as sex_table on sex_table.id = abit.sex
inner join country as born_country_table on born_country_table.id = abit.born_country
inner join region as born_base_region_table on born_base_region_table.id = abit.born_base_region
inner join cityXtype as born_cityXtype_table on born_cityXtype_table.id = abit.born_cityXtype
inner join country as passport_country_table on passport_country_table.id = abit.passport_country
inner join country as address_country_table on address_country_table.id = abit.address_country
inner join region as address_base_region_table on address_base_region_table.id = abit.address_base_region
inner join cityXtype as address_cityXtype_table on address_cityXtype_table.id = abit.address_cityXtype
inner join term as term_table on term_table.id = abit.previous_education_term
inner join country as previous_address_country_table on previous_address_country_table.id = abit.previous_address_country
inner join region as previous_address_base_region_table on previous_address_base_region_table.id = abit.previous_address_base_region
inner join cityXtype as previous_address_cityXtype_table on previous_address_cityXtype_table.id = abit.previous_address_cityXtype
inner join category as category_table on category_table.id = abit.category
inner join purpose as purpose_table on purpose_table.id = category_table.purpose
inner join privXdoc as privXdoc_table on privXdoc_table.id = abit.privXdoc
inner join achievementXolymp as achievementXolymp_table on achievementXolymp_table.id = abit.achievementXolymp
inner join achievementXcatX1 as achievementXcatX1_table on achievementXcatX1_table.id = abit.achievementXcatX1
inner join achievementXcatX2 as achievementXcatX2_table on achievementXcatX2_table.id = abit.achievementXcatX2
inner join lang as lang_table on lang_table.id = abit.lang
inner join documentXreturn as documentXreturn_table on documentXreturn_table.id = abit.documentXreturn
inner join users on users.user_name = session_user;

create view abiturient_full_slow as
select

    abit.*,

    (select count(*) from education_document where educationXdocumentXtype in (4, 11)) > 0 as first_master,

    -- Родственники
    (select count(*) from relation where aid = abit.id) > 0
        as is_relation,

    case when
        (select count(*) from relation where aid = abit.id) > 0
    then '' else 'родственники не имеются' end as relation_marker,

    -- Дипломы об образовании
    
    (select count(*) from education_document_full where aid = abit.id) > 0
        as is_education_document,
    
    (select count(*) from education_document_full where aid = abit.id and is_main) > 0
        as is_education_document_main,
    
    (select count(*) from education_document_full where aid = abit.id and is_main and is_copy) > 0
        as is_education_document_main_copy,
    
    case when
        (select count(*) from education_document_full where aid = abit.id and is_main and is_copy) > 0
    then 'К' else '' end as is_education_document_main_copy_marker,
    
    case when
        (select count(*) from education_document_full where aid = abit.id) > 0
    then 'есть документы об образовании' else 'документов об образовании нет' end as education_document_marker,

    case when
        (select count(*) from education_document_full where aid = abit.id and is_degree) > 0
    then '' else 'Ученой степени не имею' end as degree_marker,

    -- Экзамены

    get_examine_rating(id, 1) as math,
    get_examine_rating(id, 2) as phys,
    get_examine_rating(id, 3) as rus,
    get_examine_rating(id, 4) as society,
    get_examine_rating(id, 5) as hist,
    get_examine_rating(id, 6) as liter,
    get_examine_rating(id, 7) as crea,
    get_examine_rating(id, 8) as interview,

    get_rating_by_group(id, 1) as group_tech,
    get_rating_by_group(id, 2) as group_eco,
    get_rating_by_group(id, 3) as group_hum,
    get_rating_by_group(id, 4) as group_crea,
    get_rating_by_group(id, 5) as group_master,

    get_examine_rating(id, 1) as exam_1,
    get_examine_rating(id, 2) as exam_2,
    get_examine_rating(id, 3) as exam_3,
    get_examine_rating(id, 4) as exam_4,
    get_examine_rating(id, 5) as exam_5,
    get_examine_rating(id, 6) as exam_6,
    get_examine_rating(id, 7) as exam_7,
    get_examine_rating(id, 8) as exam_8,

    get_rating_by_group(id, 1) as group_1,
    get_rating_by_group(id, 2) as group_2,
    get_rating_by_group(id, 3) as group_3,
    get_rating_by_group(id, 4) as group_4,
    get_rating_by_group(id, 5) as group_5,

    get_achievement(id, False) as achievement_bachelor,
    get_achievement(id, True) as achievement_master

from abiturient_full as abit;

create view abiturient_conf as select * from abiturient_full as abit where abit.status = 2;
create view abiturient_conf_slow as select * from abiturient_full_slow as abit where abit.status = 2;

create view abiturient_conf_statement
as
select

    abit.*,
    st.*
    
from abiturient_conf as abit
inner join statement_full as st on st.aid = abit.id;

create view abiturient_conf_statement_slow
as
select

    abit.*,
    st.*
    
from abiturient_conf_slow as abit
inner join statement_full as st on st.aid = abit.id;

create view abiturient_enrolment as select * from abiturient_full abit, enrolment enr where abit.id = enr.aid;
create view abiturient_enrolment_slow as select * from abiturient_full_slow abit, enrolment enr where abit.id = enr.aid;
create view abiturient_not_enrolment as select * from abiturient_full abit, statement_full enr where abit.id = enr.aid and abit.id not in (select aid from enrolment);
create view abiturient_not_enrolment_slow as select * from abiturient_full_slow abit, statement_full enr where abit.id = enr.aid and abit.id not in (select aid from enrolment);

create view abiturient_for_main_view as
select

    *

from abiturient_full as abit;

"""

############################################################################ 

schema_fill = None

############################################################################ 

schema_trigger = """

create or replace function abiturient_before_update() returns trigger as '

    def error(msg): plpy.error("[[[%s]]]" % msg)

    ############################################################################
    # Только секретари могут исправлять заявления подтвержденных абитуриентов

    level = plpy.execute("select level from users where user_name = session_user")[0]["level"]

    if TD["old"]:
		
        if level > 5 and TD["old"]["status"] == 2: error("Недостаточно прав на обновление подтвержденного заявления")

    ############################################################################ 
    # Только секретари могут подтверждать или удалять заявления

    if level > 5 and TD["new"]["status"] > 1: error("Недостаточно прав на подтверждение или удаление")

    ############################################################################ 
    # Проверка уникальности номера дела

    is_letter_set = TD["new"]["reg_code_letter"] > 1
    is_reg_code_exists = plpy.execute("select count(*) > 0 as is_exist from abiturient where id <> %d and reg_code_letter = %d and reg_code_index = %d" % \
            (TD["new"]["id"], TD["new"]["reg_code_letter"], TD["new"]["reg_code_index"]))[0]["is_exist"]

    if is_letter_set and is_reg_code_exists: error("Абитуриент с данным регистрационным кодом уже существует")

    ############################################################################
    # Проверка заполнения экзаменов, требуемых для выбранных направлений подготовки

    groups = plpy.execute("select spec_group from statement_full where aid = %d group by spec_group" % TD["new"]["id"])

    is_exists = {}
    
    for name in range(1, 9):

        is_exist = plpy.execute("select count(*) > 0 as is_exist from examine_full where aid = %d and examineXname = %d" % (TD["new"]["id"], name))[0]["is_exist"]
        is_exists[name] = is_exist

    if not is_exists[8]:

        def check_examine(names):

            for name in names:

                if not is_exists[name]:

                    rus_name = plpy.execute("select value from examineXname where id = %d" % name)[0]["value"]
                    error("Отсутствует экзамен: %s" % rus_name)

        for __group in groups:

            group = __group["spec_group"]

            if group == 1: check_examine([1, 2, 3]) # Техническая
            elif group == 2: check_examine([1, 4, 3]) # Экономическая
            elif group == 3: check_examine([4, 5, 3]) # Гуманитарная
            elif group == 4: check_examine([6, 7, 3]) # Графическая

    ############################################################################ 
    # Заполнение ключевых полей

    field_fail = ""

    for f in \
    [
        ( "surname_genitive", "Фамилия (родительный падеж)" ), ( "surname", "Фамилия" ), ( "name_genitive", "Имя (родительный падеж)" ), ( "name", "Имя" ), ( "address_city", "Город проживания" ),
        ( "passport_series", "Серия паспорта" ), ( "passport_number", "Номер паспорта" ), ( "passport_who", "Кем выдан паспорт" ), ( "born_city", "Город рождения" )
    ]:
    
        if TD["new"][f[0]] == None or (type(TD["new"][f[0]]) == str and TD["new"][f[0]] == ""):
        
            field_fail += "%s, " % f[1]

    if field_fail != "": error("Требуется заполнить следующие поля: %s" % field_fail[ : -2])

' language plpythonu;

create trigger abu before insert or update on abiturient for each row execute procedure abiturient_before_update();

"""

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): cn.execute(schema_trigger)

