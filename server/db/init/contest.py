
schema = """

create table specXgroup     (id serial, value text, primary key (id));	    -- Группа специальностей
create table target	    (id serial, value text, primary key (id));	    -- Цель поступления
create table form	    (id serial, value text, primary key (id));	    -- Форма обучения (очная, заочная, очно - заочная)
create table pay	    (id serial, value text, primary key (id));	    -- Форма оплаты

create table decree      -- Приказы о зачислении
(

    id serial,
    value text,
    date date,

    primary key (id)

);

create table spec   -- Специальность
(

    id serial,
    group_specXgroup serial,			-- Группа специальностей
    code text,					-- Код
    name text,					-- Название
    is_spec bool default false,			-- Специальность (true) или направление подготовки (false)
    is_fast bool default false,                 -- Ускоренники
	
    primary key (id),
    foreign key (group_specXgroup) references specXgroup (id) on delete cascade on update cascade

);

create table contest
(

    id serial unique,

    target serial,
    form serial,
    pay serial,
    faculty serial,
    spec serial,

    is_teach_war_center bool default false,     -- Учебно-военный центр
    
    primary key (id),
    foreign key (target) references target (id) on delete cascade on update cascade,
    foreign key (form) references form (id) on delete cascade on update cascade,
    foreign key (pay) references pay (id) on delete cascade on update cascade,
    foreign key (faculty) references faculty (id) on delete cascade on update cascade,
    foreign key (spec) references spec (id) on delete cascade on update cascade,
    unique (target, form, pay, faculty, spec)

);

create table statement  -- Заявления
(

    id serial,
    aid serial,

    contestXfull serial,
    decree serial,                          -- Приказ о зачислении
    priority int,
    course int,

    primary key (id),
    foreign key (aid) references abiturient (id) on delete cascade on update cascade,
    foreign key (contestXfull) references contest (id) on delete cascade on update cascade,
    foreign key (decree) references decree (id) on delete cascade on update cascade,

    check (course >= 1 and course <= 6)

);

"""

############################################################################ 

schema_view = """

create view spec_full
as
select

    sp.id as id,
    sp.code as code,
    sp.name as name,
    sp.group_specXgroup as spec_group,
    sp.is_spec as is_spec,
    sp.is_fast as is_fast,

    group_specXgroup_table as spec_group_value

from spec as sp

inner join specXgroup as group_specXgroup_table on sp.group_specXgroup = group_specXgroup_table.id;

----------------------------------------------------------------

create view contestXfull
as
select

    cn.id as id,
    cn.target as target,
    cn.form as form,
    cn.pay as pay,
    cn.faculty as faculty,
    cn.spec as spec,
    cn.is_teach_war_center as is_teach_war_center,

    target_table.value as target_value,
    form_table.value as form_value,
    pay_table.value as pay_value,

    faculty_table.value as faculty_value,
    faculty_table.value_full as faculty_value_full,
    faculty_table.dean as faculty_dean,

    sp.code as code,
    sp.name as name,
    sp.spec_group as spec_group,
    sp.is_spec as is_spec,
    sp.is_fast as is_fast,
    sp.spec_group_value as spec_group_value,

    -- generic-поля

    cn.target > 2 as is_master,
    concat(sp.code, ' - ', sp.name) as code_name,
    concat(target_table.value, ' / ', form_table.value, ' / ', pay_table.value, ' / ',
        faculty_table.value, ' - ', sp.code, ' - ', sp.name, case when cn.is_teach_war_center then ' (УВЦ)' else '' end) as value

from contest as cn

inner join target as target_table on cn.target = target_table.id
inner join form as form_table on cn.form = form_table.id
inner join pay as pay_table on cn.pay = pay_table.id
inner join faculty as faculty_table on cn.faculty = faculty_table.id
inner join spec_full as sp on cn.spec = sp.id

order by target, form, pay, faculty, code;

----------------------------------------------------------------

create view statement_full
as
select

    st.id as __id,
    st.aid as aid,
    st.contestXfull as contest,
    st.decree as decree,
    st.priority as priority,
    st.course as course,

    cn.is_teach_war_center as is_teach_war_center,

    cn.target as target,
    cn.form as form,
    cn.pay as pay,
    cn.target_value as target_value,
    cn.form_value as form_value,
    cn.pay_value as pay_value,

    cn.faculty as faculty,
    cn.faculty_value as faculty_value,
    cn.faculty_value_full as faculty_value_full,
    cn.faculty_dean as faculty_dean,

    cn.spec as spec,
    cn.code as spec_code,
    cn.name as spec_name,
    cn.spec_group as spec_group,
    cn.is_spec as is_spec,
    cn.is_fast as is_fast,
    cn.spec_group_value as spec_group_value,

    cn.is_master as is_master,
    cn.value as spec_value,
    cn.code_name as spec_code_name,

    dcr.value as decree_value,
    dcr.date as decree_date,

    -- generic-поля

    cn.target in (1, 3) and cn.form = 1 and cn.pay = 1 and st.priority = 1 as is_maybe_target,

    get_examine_rating_by_group(aid, spec_group, 1) as ex_1,
    get_examine_rating_by_group(aid, spec_group, 2) as ex_2,
    get_examine_rating_by_group(aid, spec_group, 3) as ex_3,
    get_achievement(aid, is_master) as achievement,
    get_rating_by_group(aid, spec_group) as rating

from statement as st

inner join contestXfull as cn on cn.id = st.contestXfull
inner join decree as dcr on dcr.id = st.decree

order by target, form, pay, faculty, spec_code;

create view enrolment as select * from statement_full where decree > 1;

"""

############################################################################

schema_fill = """

insert into faculty (value, value_full, dean) values
    ('', 'Отсутствует', ''),
    ('ВФ', 'Вечерний факультет', 'Козлов В.Н.'),
    ('ФРТ', 'Факультет радиотехники телекоммуникаций', 'Филимонов Б.И.'),
    ('ФЭ', 'Факультет электроники', 'Верещагин Н.М.'),
    ('ФАИТУ', 'Факультет автоматики и информационных технологий в управлении', 'Холопов С.И.'),
    ('ФВТ', 'Факультет вычислительной техники', 'Пылькин А.Н.'),
    ('ИЭФ', 'Инженерно-экономический факультет', 'Евдокимова Е.Н.'),
    ('М', 'Магистратура', 'Корячко А.В.');

insert into letter (value, faculty) values
    ('', 1), ('Ч', 2), ('З', 2), ('П', 2), ('Р', 3), ('Э', 4), ('А', 5), ('В', 6), ('И', 7), ('М', 8);

insert into specXgroup (value) values ('Техническая'), ('Экономическая'), ('Гуманитарная'), ('Графическая');
insert into target (value) values ('Бакалавриат / специалитет'), ('Бакалавриат / специалитет (второй и последующий курсы)'), ('Магистратура'), ('Магистратура (второй и последующий курсы)');
insert into form (value) values ('Очная'), ('Очно - заочная'), ('Заочная');
insert into pay (value) values ('Бюджет'), ('Вне бюджет');
insert into decree (value) values ('Не зачислен'), ('Зачислен наполовину');

"""

############################################################################ 

schema_trigger = """

create or replace function statement_before_update() returns trigger as '

    def error(msg): plpy.error("[[[%s]]]" % msg)

    __id = TD["new"]["id"]
    aid = TD["new"]["aid"]
    priority = TD["new"]["priority"]
    contest = TD["new"]["contestxfull"]

    ############################################################################
    # Проверка на зачисление - если человек уже зачислен, то зачислить его еще раз нельзя

    decree = TD["new"]["decree"]
    is_enrolment = (len(plpy.execute("select * from enrolment where __id <> %d and aid = %d" % (__id, aid))) > 0)

    if is_enrolment and decree > 1:
    
        error("Данный абитуриент уже зачислен и редактируется не его зачисление")

    ############################################################################
    # Проверка на существование приоритета и на количество приоритетов

    row = plpy.execute("select target, form, pay from contest where id = %d" % contest)
    target = row[0]["target"]
    form = row[0]["form"]
    pay = row[0]["pay"]

    if \
        (target in (1, 3) and priority not in (1, 2, 3)) \
        or \
        \
        (target not in (1, 3) and priority != 1):
        error("Некорректный приоритет заявления")

    is_exist = (len(plpy.execute("select * from statement_full where target = %d and form = %d and pay = %d and priority = %d and __id <> %d and aid = %d" % (target, form, pay, priority, __id, aid))) > 0)

    if is_exist:
    
        error("Цель поступления и приоритет уже существуют")

    #############################################################################
    # Право на зачисление

    level = plpy.execute("select level from users where user_name = session_user")[0]["level"]

    if level > 3 and TD["new"]["decree"] > 1: error("Только пользователи с уровнем доступа 3 и меньше (главные секретари) имеют право зачислять абитуриентов")

' language plpythonu;

create trigger stbu before insert or update on statement for each row execute procedure statement_before_update();

"""

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): cn.execute(schema_trigger)

