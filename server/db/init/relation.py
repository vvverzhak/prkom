
schema = """

create table relationXtype  (id serial, value text, primary key (id));              -- Тип родственника (отец, мать, отчим, мачеха, опекун)
create table relationXplaceXtype  (id serial, value text, primary key (id));        -- Местонахождение родственника

create table relation
(

    id serial unique,
    aid serial,

    relationXtype serial,           -- Тип родственника
    relationXplaceXtype serial,     -- Местонахождение родственника

    -- Имя
    surname text,	            -- Фамилия
    name text,	                    -- Имя
    patrname text,	            -- Отчество

    -- Адрес
    address_country serial,	    -- Страна
    address_base_region serial,	    -- Регион
    address_region_additional text, -- Регион - дополнительно
    address_city text,		    -- Населенный пункт
    address_cityXtype serial,	    -- Тип населенного пункта
    address_district text,	    -- Район
    address_street text,	    -- Улица
    address_home text,		    -- Дом
    address_building text,	    -- Корпус
    address_flat text,		    -- Квартира
    address_index text,		    -- Индекс

    -- Домашний телефон
    telephone_code text,	    -- Код
    telephone_number text,	    -- Номер

    -- Мобильный телефон
    mobile text,

    -- Работа
    work_place text,		    -- Место работы
    work_post text,		    -- Должность
    work_telephone_code text,	    -- Код телефона
    work_telephone_number text,	    -- Номер телефона

    check (surname <> '' and name <> ''),

    primary key (id), 
    foreign key (aid) references abiturient (id) on delete cascade on update cascade,
    foreign key (relationXtype) references relationXtype (id) on delete cascade on update cascade,
    foreign key (relationXplaceXtype) references relationXplaceXtype (id) on delete cascade on update cascade,
    foreign key (address_country) references country (id) on delete cascade on update cascade,
    foreign key (address_cityXtype) references cityXtype (id) on delete cascade on update cascade

);

"""

############################################################################ 

schema_view = """

create view relation_full as
select

    relation.*,

    relation_type_table.value as relation_type_value,
    relation_place_type_table.value as relation_place_type_value,
    address_cityXtype_table.value as address_cityXtype_value,

    -- generic-поля

    concat(surname, ' ', name, ' ', patrname) as fio,
		
    case when address_base_region = 2 then address_region_additional else address_base_region_table.value end as address_region,

    concat(
	address_country_table.value,
        case when address_index = '' then '' else concat(', ', address_index) end,
        concat(', ', case when address_base_region = 2 then address_region_additional else address_base_region_table.value end),
	case when address_district = '' then '' else concat(', ', address_district, ' район') end,
	case when address_cityXtype = 6 then ',' else concat(', ', my_lower(address_cityXtype_table.value)) end,
	case when address_city = '' then '' else concat(' ', address_city) end,
	case when address_street = '' then '' else concat(', ', address_street) end,
	case when address_home = '' then '' else concat(', дом ', address_home) end,
	case when address_building = '' then '' else concat(', корпус ', address_building) end,
	case when address_flat = '' then '' else concat(', квартира ', address_flat) end
    ) as address,

    concat(
        '(', telephone_code, ') ', telephone_number
    ) as telephone,

    concat(
        'Моб.: ', mobile, ' / Дом.: (', telephone_code, ') ', telephone_number
    ) as telephone_full,

    concat(work_place, ', ', work_post) as work_description,
    
    relationXplaceXtype = 2 as is_dead,
    relationXplaceXtype in (2, 3, 5, 6, 7) as not_found

    from relation as relation

    inner join relationXtype as relation_type_table on relation_type_table.id = relation.relationXtype
    inner join relationXplaceXtype as relation_place_type_table on relation_place_type_table.id = relation.relationXplaceXtype
    inner join country as address_country_table on address_country_table.id = relation.address_country
    inner join region as address_base_region_table on address_base_region_table.id = relation.address_base_region
    inner join cityXtype as address_cityXtype_table on address_cityXtype_table.id = relation.address_cityXtype;

create view relationXfull as
select

    relation_full.*,
    relation_full.fio as value

from relation_full;

"""

############################################################################ 

schema_fill = \
"""
    insert into relationXtype (value) values ('Отец'), ('Мать'), ('Отчим'), ('Мачеха'), ('Опекун');
    insert into relationXplaceXtype (value) values
        (''),
        ('Умер'), ('Отец/мать - с семьей не проживает'), ('Отец/мать - одиночка'), ('Отец/мать - записан(а) со слов матери/отца'), ('Отец/мать - прочерк в свидетельстве о рождении'),
        ('Местонахождение не установлено');
"""

############################################################################ 

schema_trigger = """

create or replace function relation_before_update() returns trigger as '

    def error(msg): plpy.error("[[[%s]]]" % msg)

' language plpythonu;

create trigger rbu before insert or update on relation for each row execute procedure relation_before_update();

"""

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): cn.execute(schema_trigger)

