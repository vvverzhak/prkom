
schema = """

create table educationXdocumentXbaseXtype	(id serial, value text, primary key (id));	-- Тип документа об образовании
create table educationXdocumentXoriginalXcopy	(id serial, value text, primary key (id));	-- Оригинал, копия, нотариально заверенная копия

-- Конкретный тип документа об образовании
create table educationXdocumentXtype
(

    id serial,
    bid serial,
    value text,
    
    primary key (id),
    foreign key (bid) references educationXdocumentXbaseXtype (id) on delete cascade on update cascade

);

create table education_document
(
    
    id serial,
    aid serial,

    educationXdocumentXtype serial,	        -- Тип документа об образовании
    series text,	                	-- Серия
    number text,		                -- Номер
    reg_number text,		                -- Регистрационный номер
    date date,		                        -- Дата выдачи
    who text,                                   -- Кем выдан
    educationXdocumentXoriginalXcopy serial,	-- Тип документа
    is_medal bool,		                -- Медаль / диплом с отличием?
    is_main bool,                               -- Является основанием для поступления
    spec text,                                  -- Специальность
    qualification text,                         -- Квалификация

    primary key (id),
    foreign key (aid) references abiturient (id) on delete cascade on update cascade,
    foreign key (educationXdocumentXtype) references educationXdocumentXtype (id) on delete cascade on update cascade,
    foreign key (educationXdocumentXoriginalXcopy) references educationXdocumentXoriginalXcopy (id) on delete cascade on update cascade

);

"""

############################################################################ 

schema_view = """

create view education_document_full as
select

    education_document.*,
    educationXdocumentXbaseXtype_table.value as educationXdocumentXbaseXtype_value,
    educationXdocumentXtype_table.value as educationXdocumentXtype_value,
    educationXdocumentXoriginalXcopy_table.value as educationXdocumentXoriginalXcopy_value,

    -- generic поля

    concat(series, ' ', number) as series_number,
    educationXdocumentXoriginalXcopy <> 1 as is_copy,
    educationXdocumentXoriginalXcopy = 1 as is_original,
    case when is_main then 'Да' else '' end as main_marker,
    case when educationXdocumentXoriginalXcopy <> 1 then 'К' else '' end as copy_marker,
    case when educationXdocumentXoriginalXcopy <> 1 then '(К)' else '' end as copy_marker_with_braces,
    concat(qualification, ', ', date, ', ', series, ' ', number) as degree_description,
    (educationXdocumentXbaseXtype_table.id = 3) as is_degree,
    (educationXdocumentXbaseXtype_table.id = 1) as is_cert,
    (educationXdocumentXbaseXtype_table.id = 6) as is_reference,
    (educationXdocumentXbaseXtype_table.id = 2 or educationXdocumentXbaseXtype_table.id = 3) as is_diplom,
    (educationXdocumentXbaseXtype_table.id = 4) as is_incomplete_reference,
    (educationXdocumentXbaseXtype_table.id = 5) as is_incomplete_diplom,

    case when (educationXdocumentXbaseXtype_table.id = 1) then
        concat('выдан(а) ', who, ', ', date, ', ', spec, ', ', qualification)
    else
        concat('выдан(а) ', who, ', ', date, ', ', series, ' ', number, ', ', spec, ', ', qualification)
    end as description

from education_document

    inner join educationXdocumentXtype as educationXdocumentXtype_table on educationXdocumentXtype_table.id = education_document.educationXdocumentXtype
    inner join educationXdocumentXbaseXtype as educationXdocumentXbaseXtype_table on educationXdocumentXbaseXtype_table.id = educationXdocumentXtype_table.bid
    inner join educationXdocumentXoriginalXcopy as educationXdocumentXoriginalXcopy_table on educationXdocumentXoriginalXcopy_table.id = education_document.educationXdocumentXoriginalXcopy;

"""

############################################################################ 

schema_fill = """

insert into educationXdocumentXbaseXtype (value) values
    ('Аттестат'), ('Диплом'), ('Диплом об ученой степени'), ('Справка'), ('Диплом о неполном ВПО'), ('Зачетная книжка');

insert into educationXdocumentXtype (value, bid) values
    ('Аттестат', 1),
    ('Диплом НПО', 2),
    ('Диплом СПО', 2),
    ('Диплом ВПО (специалист)', 2),
    ('Справка об обучении', 4),
    ('Зачетная книжка', 6),
    ('Диплом о неполном ВПО', 5),
    ('Диплом кандидата наук', 3),
    ('Диплом доктора наук', 3),
    ('Диплом ВПО (магистр)', 2),
    ('Диплом ВПО (бакалавр)', 2);

insert into educationXdocumentXoriginalXcopy (value) values ('Оригинал'), ('Копия');

"""

############################################################################ 

schema_trigger = """

create or replace function education_document_before_update() returns trigger as '

    def error(msg): plpy.error("[[[%s]]]" % msg)

    if TD["new"]["is_main"] and (plpy.execute("select count(*) from education_document where id <> %d and aid = %d and is_main" % (TD["new"]["id"], TD["new"]["aid"]))[0]["count"] > 0):

        error("У абитуриента уже есть документ - основание для поступления")

' language plpythonu;

create trigger edbu before insert or update on education_document for each row execute procedure education_document_before_update();

"""

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): cn.execute(schema_trigger)

