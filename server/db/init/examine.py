
schema = """

create table examineXtype (id serial, value text, primary key (id)); -- Тип экзамена
create table examineXname (id serial, value text, primary key (id)); -- Название экзамена

create table examine  -- Экзамены
(

    id serial unique,
    
    aid serial,
    examineXtype serial,
    examineXname serial,

    year date,	 -- Год сдачи
    yes bool,    -- Требуется допустить
    rating int,  -- Оценка

    primary key (id),
    foreign key (aid) references abiturient (id) on delete cascade on update cascade,
    foreign key (examineXtype) references examineXtype (id) on delete cascade on update cascade,
    foreign key (examineXname) references examineXname (id) on delete cascade on update cascade,

    check (rating >= 0 and rating <= 100),
    unique (aid, examineXname)

);

create or replace function get_examine_rating(aid int, name int) returns int as '

    rating = plpy.execute("select rating from examine where aid = %d and examineXname = %d" % (aid, name))
    if len(rating) == 0: return None

    return rating[0]["rating"]

' language plpythonu;

create or replace function get_examine_name_by_group(spec_group int, ind int) returns int as '

    if ind not in (1, 2, 3): return None

    if spec_group == 1: name = [ 1, 2, 3 ][ ind - 1]
    elif spec_group == 2: name = [ 1, 4, 3 ][ ind - 1]
    elif spec_group == 3: name = [ 4, 5, 3 ][ ind - 1]
    elif spec_group == 4: name = [ 6, 7, 3 ][ ind - 1]
    elif spec_group == 5 and ind == 1: name = 8
    else: return None

    return name

' language plpythonu;

create or replace function get_examine_rating_by_group(aid int, spec_group int, ind int) returns int as '

    name = plpy.execute("select get_examine_name_by_group(%d, %d) as name" % (spec_group, ind))[0]["name"]
    if not name: return None

    rating = plpy.execute("select get_examine_rating(%d, %d) as rating" % (aid, name))

    return rating[0]["rating"]

' language plpythonu;

create or replace function get_achievement(aid int, is_master bool) returns int as '

    abit = plpy.execute("select is_publ, is_diplom_best, achievementXcatX1, achievementXcatX2, achievementXolymp, is_cert_best from abiturient where id = %d" % aid)[0]

    if is_master:

        is_publ = abit["is_publ"]
        is_diplom_best = abit["is_diplom_best"]
        cat_1 = abit["achievementxcatx1"] > 1
        cat_2 = abit["achievementxcatx2"] > 1

        achievement = [ 0, 1 ][ is_publ ] + [ 0, 5 ][ is_diplom_best ] + [0, 2][ cat_1 ] + [0, 2][ cat_2 ]

    else:
        
        olymp = abit["achievementxolymp"]
        is_cert_best = abit["is_cert_best"]

        achievement = [0, 2, 2, 3, 3, 4, 4][ olymp - 1 ] + [0, 6][ is_cert_best ]

    return achievement

' language plpythonu;

create or replace function get_rating_by_group(aid int, spec_group int) returns int as '

    if spec_group == 5:

        rating = plpy.execute("select get_examine_rating_by_group(%d, %d, 1) as rating" % (aid, spec_group))[0]["rating"]
        if not rating: return None

        achievement = plpy.execute("select get_achievement(%d, True) as rating" % aid)[0]["rating"]

    else:
        
        rating = 0

        for ind in (1, 2, 3):

            ex = plpy.execute("select get_examine_rating_by_group(%d, %d, %d) as rating" % (aid, spec_group, ind))[0]["rating"]
            if not ex: return None
            rating += ex

        achievement = plpy.execute("select get_achievement(%d, False) as rating" % aid)[0]["rating"]

    return rating + achievement

' language plpythonu;

"""

############################################################################ 

schema_view = """

create view examine_full
as
select

    ex.id as id,
    ex.aid as aid,
    ex.examineXtype as examineXtype,
    ex.examineXname as examineXname,
    ex.year as year,
    ex.yes as yes,
    ex.rating as rating,

    ext.value as type_name,
    exn.value as name,

    -- generic-поля

    ex.examineXtype = 1 as is_ege,
    ex.examineXtype = 2 as is_rsreu,
    ex.examineXtype = 3 as is_olymp,
    ex.examineXtype in (1, 2, 3) as is_examine_real

from examine as ex
inner join examineXtype as ext on ext.id = examineXtype
inner join examineXname as exn on exn.id = examineXname;

"""

############################################################################

schema_fill = """

insert into examineXtype (value) values ('ЕГЭ'), ('РГРТУ'), ('Олимпиада'), ('Не нужен');
insert into examineXname (value) values
    ('Математика'),
    ('Физика'),
    ('Русский язык'),
    ('Обществознание'),
    ('История'),
    ('Литература'),
    ('Творческий экзамен'),
    ('Собеседование');

"""

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): pass

