
schema = """

-- История печати заявлений
create table history
(

    id serial,
    uid serial,
    aid serial,
    name text,
    surname text,
    patrname text,
    passport_series text,
    passport_number text,
    when_date date,
    when_time time,

    primary key (id), 
    foreign key (uid) references users (id) on delete cascade on update cascade,
    foreign key (aid) references abiturient (id) on delete cascade on update cascade

);

create table hate
(

    id serial,

    ip text,
    when_date date,
    when_time time,
    message text,

    primary key (id)

);

create or replace function my_upper(t text) returns text as '

    return t.decode("UTF-8").upper().encode("UTF-8")

' language plpythonu;
	
create or replace function my_lower(t text) returns text as '

    return t.decode("UTF-8").lower().encode("UTF-8")

' language plpythonu;

create or replace function my_printable_date_my(dt date) returns text as $$
begin
    return concat(lpad(date_part('month', dt) :: text, 2, '0'), '.', date_part('year', dt) :: text);
end
$$ language plpgsql;

create or replace function my_printable_date_dmy(dt date) returns text as $$
begin
    return concat(lpad(date_part('day', dt) :: text, 2, '0'), '.', lpad(date_part('month', dt) :: text, 2, '0'), '.', date_part('year', dt) :: text);
end
$$ language plpgsql;

create or replace function my_current_user_faculty_id() returns int as $$
begin
    return (select faculty from users where user_name = session_user);
end
$$ language plpgsql;

"""

############################################################################ 

schema_view = """

create view history_full as
select

    history.*,
    users_table.user_name as user_name,
    users_table.ip as ip,
    users_table.description as description

from history

inner join users as users_table on users_table.id = history.uid;

create view telephone
as

    select
	
	id as aid,
	fio,
	telephone_code as code,
	telephone_number as number,
	'Домашний телефон' as type,
	'' as who,
        '' as relation_type_value

    from abiturient_full

union

    select

        id as aid,
        fio,
        '' as code,
        mobile as number,
        'Мобильный' as type,
        '' as who,
        '' as relation_type_value

    from abiturient_full

union

    select

        aid,
        relation_full.fio as fio,
        relation_full.telephone_code as code,
        relation_full.telephone_number as number,
        'Домашний телефон' as type,
        abit.fio as who,
        relation_type_value

    from relation_full
    
    inner join abiturient_full as abit on abit.id = aid

union

    select

        aid,
        relation_full.fio as fio,
        '' as code,
        relation_full.mobile as number,
        'Мобильный' as type,
        abit.fio as who,
        relation_type_value

    from relation_full

    inner join abiturient_full as abit on abit.id = aid

union

    select

        aid,
        relation_full.fio as fio,
        relation_full.work_telephone_code as code,
        relation_full.work_telephone_number as number,
        'Рабочий телефон' as type,
        abit.fio as who,
        relation_type_value

    from relation_full

    inner join abiturient_full as abit on abit.id = aid;

"""

############################################################################ 

schema_fill = None

############################################################################ 

schema_trigger = None

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def fill(cn): cn.execute(schema_fill)
def trigger(cn): cn.execute(schema_trigger)

