
import config, lib, db
import db.init.role
import db.init.contest
import db.init.examine
import db.init.education
import db.init.relation
import db.init.persons
import db.init.other
import db.init.lists

############################################################################ 

@lib.function_description("Инициализация базы данных")
def main():

    with db.c_db(db_name = "postgres") as cn:
        
        cn.execute("drop database if exists %s" % config.db.name)
        cn.execute("create database %s with owner = %s encoding = 'UTF-8' template = template0" % (config.db.name, config.db.admin))

    with db.c_db() as cn:
        
        __init(cn)
        view(cn)
        fill(cn)
        trigger(cn)

############################################################################ 

def base(cn, fun_name, lst):

    for md in lst:

        md.__dict__[fun_name](cn)

@lib.function_description("Создание таблиц")
def __init(cn): base(cn, "init",
    [
        db.init.lists,
        db.init.persons,
        db.init.contest,
        db.init.examine,
        db.init.relation,
        db.init.education,
        db.init.role,
        db.init.other
    ])

@lib.function_description("Создание представлений")
def view(cn): base(cn, "view",
    [
        db.init.lists,
        db.init.contest,
        db.init.examine,
        db.init.relation,
        db.init.education,
        db.init.role,
        db.init.persons,
        db.init.other
    ])

@lib.function_description("Заполнение таблиц")
def fill(cn): base(cn, "fill",
    [
        db.init.lists,
        db.init.persons,
        db.init.contest,
        db.init.examine,
        db.init.relation,
        db.init.education,
        db.init.other,
        db.init.role
    ])

@lib.function_description("Создание триггеров")
def trigger(cn): base(cn, "trigger",
    [
        db.init.lists,
        db.init.persons,
        db.init.contest,
        db.init.examine,
        db.init.relation,
        db.init.education,
        db.init.other,
        db.init.role
    ])

