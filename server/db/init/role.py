
import config

schema = """

drop role if exists operator;
drop role if exists secretary;
drop role if exists main_secretary;

create role operator with nologin;
create role secretary with nologin in role operator;
create role main_secretary with nologin in role secretary;

create table users
(

    id serial,
    user_name text,
    faculty serial,
    level int default 10,
    ip text not null,
    description text,

    primary key (id),
    unique (user_name),
    foreign key (faculty) references faculty (id) on delete cascade on update cascade

);
	
"""

############################################################################ 

schema_view = None

############################################################################ 

schema_fill = None

############################################################################ 

schema_trigger = None

############################################################################ 
# Таблицы

list_tables = \
[
    "letter",
    "status",
    "country",
    "region",
    "cityXtype",
    "sex",
    "lang",
    "documentXreturn",
    "term",
    "achievementXolymp",
    "achievementXcatX1",
    "achievementXcatX2",
    "faculty",
    "specXgroup",
    "target",
    "form",
    "pay",
    "privXdoc",
    "purpose",
    "educationXdocumentXbaseXtype",
    "educationXdocumentXtype",
    "educationXdocumentXoriginalXcopy",
    "examineXtype",
    "examineXname",
    "relationXtype",
    "relationXplaceXtype",
    "users"
]

list_to_edit_tables = \
[
    "category",
    "decree",
    "spec",
    "contest"
]

tables = \
[
    "abiturient",
    "education_document",
    "statement",
    "examine",
    "relation",
    "hate"
]

priv_tables = \
[
    "history"
]

############################################################################ 
# Представления

list_views = \
[
]

list_to_edit_views = \
[
    "contestXfull"
]

views = \
[
    "abiturient_full",
    "abiturient_conf",
    "abiturient_full_slow",
    "abiturient_conf_slow",
    "abiturient_enrolment",
    "abiturient_enrolment_slow",
    "abiturient_not_enrolment",
    "abiturient_not_enrolment_slow",
    "abiturient_for_main_view",
    "abiturient_conf_statement",
    "abiturient_conf_statement_slow",
    "spec_full",
    "statement_full",
    "enrolment",
    "education_document_full",
    "examine_full",
    "relation_full",
    "relationXfull"
]

priv_views = \
[
    "history_full",
    "telephone"
]

############################################################################ 

def init(cn): cn.execute(schema)
def view(cn): cn.execute(schema_view)
def trigger(cn): cn.execute(schema_trigger)

def fill(cn):

    ############################################################################ 
    # Права доступа к таблицам и представлениям

    def set_rights(tables, views, table_rights, index_rights, view_rights):

        for table_name in tables:

            for who in table_rights:
                
                cn.execute("grant %s on %s to %s" % (table_rights[who], table_name, who))

            for who in index_rights:
                
                cn.execute("grant %s on %s_id_seq to %s" % (index_rights[who], table_name, who))

        for view_name in views:

            for who in view_rights:
                
                cn.execute("grant %s on %s to %s" % (view_rights[who], view_name, who))

    set_rights(list_tables, list_views,
            { "operator" : "select" },
            { "operator" : "select" },
            { "operator" : "select" })

    set_rights(list_to_edit_tables, list_to_edit_views,
            { "secretary" : "all", "operator" : "select" },
            { "secretary" : "update", "operator" : "select" },
            { "operator" : "select" })

    set_rights(tables, views,
            { "secretary" : "all", "operator" : "select, insert, update" },
            { "operator" : "update" },
            { "operator" : "select" })

    set_rights(priv_tables, priv_views,
            { "main_secretary" : "all" },
            { "operator" : "update" },
            { "main_secretary" : "select" })

    ############################################################################ 

    def add_user(login, password, level, role, ip, desc, faculty = 1):

        cn.execute("drop role if exists %s" % login)
        cn.execute("create role %s with login encrypted password '%s' in role %s" % (login, password, role))
        cn.execute("insert into users (user_name, faculty, level, ip, description) values ('%s', %d, %d, '%s', '%s')" % (login, faculty, level, ip, desc))

    cn.execute("insert into users (user_name, faculty, level, ip) values ('%s', %d, %d, '%s')" % (config.db.admin, 1, 1, ""))

    add_user("s_main",      "ert34sdfqwe22456w",    role = "main_secretary",	            level = 3,      ip = "127.0.0.1",	    desc = "Сервер")
    add_user("arina",       "ert34sdfqwe22456w",    role = "main_secretary",	            level = 3,      ip = "192.168.33.2",    desc = "Арина")
    add_user("denis",       "ert34sdfqwe22456w",    role = "main_secretary",	            level = 3,      ip = "192.168.33.3",    desc = "Денис")
    add_user("tp",          "ert34sdfqwe22456w",    role = "main_secretary",	            level = 3,      ip = "192.168.33.4",    desc = "Ирина Васильевна")
    add_user("volk",        "ert34sdfqwe22456w",    role = "main_secretary",	            level = 3,      ip = "192.168.33.5",    desc = "Волченков")

    add_user("s_fvt",	    "ssdfg34sdfw3rdas",	    role = "secretary",	    faculty = 6,    level = 5,      ip = "192.168.33.10",   desc = "Секретарь ФВТ")
    add_user("s_rtf",	    "erte63tfaqe123w1",	    role = "secretary",	    faculty = 3,    level = 5,      ip = "192.168.33.11",   desc = "Секретарь РТФ")
    add_user("s_faitu",	    "xcvdfawq3wdfe4r2",	    role = "secretary",	    faculty = 5,    level = 5,      ip = "192.168.33.12",   desc = "Секретарь ФАИТУ")
    add_user("s_fe",	    "tyuert23qsew1234",	    role = "secretary",	    faculty = 4,    level = 5,      ip = "192.168.33.13",   desc = "Секретарь ФЭ")
    add_user("s_ief",	    "sdfcety34dswddwe",	    role = "secretary",	    faculty = 7,    level = 5,      ip = "192.168.33.14",   desc = "Секретарь ИЭФ")
    add_user("s_vf_1",	    "dfgbtr23rasfsdyr",	    role = "secretary",	                    level = 5,      ip = "192.168.33.20",   desc = "Секретарь ВФ (20)")
    add_user("s_vf_2",	    "dfgbtr23rasfsdyr",	    role = "secretary",	                    level = 5,      ip = "192.168.33.21",   desc = "Секретарь ВФ (21)")
    add_user("s_vf_3",	    "dfgbtr23rasfsdyr",	    role = "secretary",	                    level = 5,      ip = "192.168.33.22",   desc = "Секретарь ВФ (22)")
    add_user("s_master",    "dfgbtr23rasfsdyr",	    role = "secretary",	    faculty = 8,    level = 5,      ip = "192.168.33.23",   desc = "Магистратура")


    for last_byte in range(200, 255):

        num = last_byte - 200 + 1
        add_user("op_%d" % last_byte, "657rfwerewqrwwew", role = "operator", level = 10, ip = "192.168.33.%d" % last_byte, desc = "Оператор (%d от двери)" % num)

