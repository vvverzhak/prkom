
import psycopg2 as pg
import psycopg2.extras as pg_extras
import config

class c_db():

    def __init__(self, login = config.db.admin, password = None, db_name = config.db.name):

        self.cn = pg.connect(database = db_name, user = login, password = password)
        self.cn.set_isolation_level(pg.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # TODO Нужен ли при этом коммит?
        self.cursor = self.cn.cursor(cursor_factory = pg_extras.RealDictCursor)

    def __execute__(self, query):

        self.cursor.execute(query.encode("UTF-8"))

    def execute(self, query):

        if query:

            self.__execute__(query)
            self.cn.commit()

    def select_one(self, query):

        if query:

            self.__execute__(query)
            return self.cursor.fetchone()

        return None

    def select_all(self, query):

        if query:

            self.__execute__(query)
            return self.cursor.fetchall()

        return None

    exist = lambda self, query: len(self.select_one(query)) > 0
    get_value = lambda self, tname, id: self.select_one("select value from %s where id = %d" % (tname, id))["value"]

    def close(self, is_commit = True):

        if is_commit: self.cn.commit()

        self.cn.close()

    def __enter__(self, db_name = config.db.name):

        return self

    def __exit__(self, ex_type, ex_value, traceback):

        if ex_type == None: self.close()

