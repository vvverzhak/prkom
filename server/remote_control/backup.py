
import tempfile, os
from functools import partial
import lib, db.init.role

def backup():

    tables_for_backup = db.init.role.list_to_edit_tables + db.init.role.tables

    tm = lib.c_current_date_time()
    fname = "/tmp/backup_%s_%s_%s" % (tm.date, tm.hour, tm.minute)
    prepared_ln = []

    for tname in tables_for_backup:

        prepared_ln.append("delete from %s;\n" % tname)
        os.system("pg_dump abiturient -a --column-inserts -U postgres --table=%s >> %s" % (tname, fname))

    fl = open(fname, "r")
    ln = fl.readlines()
    fl.close()

    bad_prefix = [ "--", "SET", "SELECT" ]
    check_bad_prefix = lambda bp, l: l[0 : len(bp)] == bp

    for l in ln:

        if not (any(map(partial(check_bad_prefix, l = l), bad_prefix)) or l == "\n" or len(l) == 0):

            prepared_ln.append(l)

    for tname in tables_for_backup:

        print(tname)
        prepared_ln.append("SELECT setval('%s_id_seq', (SELECT MAX(id) + 1 FROM %s));\n" % (tname, tname))

    fl = open(fname, "w")
    fl.writelines(prepared_ln)
    fl.close()

    os.system("tar cf %s.tar %s" % (fname, fname))

    return ( True, { "file" : lib.protocol.pack_file("%s.tar" % fname, ".tar") } )

