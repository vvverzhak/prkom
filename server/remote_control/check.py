
import tempfile, csv
import itertools as it
import db, lib

class c_writer:
	
    def __init__(self, cn, fname):

        self.__fl = open(fname, "w", newline = "")
        self.__writer = csv.writer(self.__fl, delimiter = "%")

        head = \
        [
            "Регистрационный код",
            "ФИО",
            "Факультет",
            "Номер дела",
            "Копия",
            "Категория",
            "Гражданство",
            "Серия паспорта",
            "Номер паспорта",
            "Номер городского телефона",
            "Номер сотового телефона",
            "Страна рождения",
            "Регион рождения",
            "Страна проживания",
            "Регион проживания",
            "Математика",
            "Физика",
            "Русский язык",
            "Обществознание",
            "История",
            "Литература",
            "Творческий экзамен",
            "Собеседование",
            "Индивидуальные достижения - бакалавриат",
            "Индивидуальные достижения - магистратура",
            "Технические специальности",
            "Экономические специальности",
            "Гуманитарные специальности",
            "Творческие специальности",
            "Оценки - магистратура",
            "Приказ о зачислении",
            "Куда зачислен"
        ]

        targets = cn.select_all("select value from target order by id")
        forms = cn.select_all("select value from form order by id")
        pays = cn.select_all("select value from pay order by id")

        for target, form, pay, priority in it.product(targets, forms, pays, range(1, 4)):

            head.append("%s / %s / %s / %d-й приоритет" % (target["value"], form["value"], pay["value"], priority))

        self.__writer.writerow(head)

    def __enter__(self):
            
        return self
    
    def __exit__(self, ex_type, ex_value, traceback):
            
        if ex_type == None: self.__fl.close()

    def __call__(self, abit, st, decree):
    
        row = \
        [
            abit["reg_code"],
            abit["fio"],
            abit["reg_code_letter_value"],
            abit["reg_code_index"],
            abit["is_education_document_main_copy_marker"],
            abit["category_value"],
            abit["passport_country_value"],
            abit["passport_series"],
            abit["passport_number"],
            abit["telephone"],
            abit["mobile"],
            abit["born_country_value"],
            abit["born_region"],
            abit["address_country_value"],
            abit["address_region"],
            abit["math"],
            abit["phys"],
            abit["rus"],
            abit["society"],
            abit["hist"],
            abit["liter"],
            abit["crea"],
            abit["interview"],
            abit["achievement_bachelor"],
            abit["achievement_master"],
            abit["group_tech"],
            abit["group_eco"],
            abit["group_hum"],
            abit["group_crea"],
            abit["group_master"],
            decree["decree_value"],
            decree["spec_value"]
        ] + st

        for ind in range(len(row)):
                
            if row[ind] == None:
                        
                row[ind] = ""

        self.__writer.writerow(row)

def check():
	
    fname = tempfile.mktemp(".csv")
    
    with db.c_db() as cn:
    
        with c_writer(cn, fname) as writer:
            
            abits = cn.select_all("select * from abiturient_conf_slow")

            for abit in abits:
                
                aid = abit["id"]

                ############################################################################ 
                # Специальности

                st = []

                for target, form, pay, priority in lib.target_form_pay_priority:

                    row = cn.select_one("""
                    
                    select
                    
                        spec_code_name
                        
                    from statement_full
                    where
                    
                        aid = %d
                        and
                        target = %d and form = %d and pay = %d and priority = %d

                    """ % (aid, target, form, pay, priority))

                    st.append(row["spec_code_name"] if row else "")

                ############################################################################ 
                # Приказ о зачислении

                max_decree = cn.select_one("select max(decree) as max_decree from statement_full where aid = %d" % aid)["max_decree"]

                if max_decree == 1:

                    decree = { "decree_value" : "", "spec_value" : "" }

                else:

                    decree = cn.select_one("select decree_value, spec_value from statement_full where aid = %d and decree = %d" % (aid, max_decree))

                writer(abit, st, decree)
            
    return ( True, { "file" : lib.protocol.pack_file(fname, ".csv") } )

