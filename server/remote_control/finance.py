
import tempfile, csv
import db, lib.protocol

class c_writer:

    def __init__(self, fname):

        self.__fl = open(fname, "w", newline = "")
        self.__writer = csv.writer(self.__fl, delimiter = "%")
        self.__writer.writerow(
        [
            "Фамилия",
            "Имя",
            "Отчество",
            "Дата рождения",
            "Место рождения",
            "Гражданство",
            "Индекс",
            "Регион",
            "Регион - сокращение",
            "Район",
            "Район - сокращение",
            "Город",
            "Город - сокращение",
            "Населенный пункт",
            "Населенный пункт - сокращение",
            "Улица",
            "Улица - сокращение",
            "Дом",
            "Корпус",
            "Квартира",
            "Паспорт - серия",
            "Паспорт - номер",
            "Дата выдачи паспорты",
            "Кем выдан паспорт",
            "Телефон - мобильный"
        ])

    def __enter__(self):

        return self

    def __exit__(self, ex_type, ex_value, traceback):

        if ex_type == None: self.__fl.close()

    def __call__(self, row):

        is_city = (row["address_cityxtype"] == 1)

        real_row = \
        [
            row["surname"],                                  # Фамилия
            row["name"],                                     # Имя
            row["patrname"],                                 # Отчество
            row["printable_born_date"],                      # Дата рождения
            row["born_city"],                                # Место рождения
            row["passport_country_value"],                   # Гражданство
            row["address_index"],                            # Индекс
            row["address_region"],                           # Регион
            "",                                              # Регион - сокращение
            row["address_district"],                         # Район
            "",                                              # Район - сокращение
            [ "", row["address_city"] ][is_city],            # Город
            [ "", "г" ][is_city],                            # Город - сокращение
            [ row["address_city"], "" ][is_city],            # Населенный пункт
            [ row["born_cityxtype_value"], ""][is_city],     # Населенный пункт - сокращение
            row["address_street"],                           # Улица
            "",                                              # Улица - сокращение
            row["address_home"],                             # Дом
            row["address_building"],                         # Корпус
            row["address_flat"],                             # Квартира
            row["passport_series"],                          # Паспорт - серия
            row["passport_number"],                          # Паспорт - номер
            row["printable_passport_when"],                  # Дата выдачи паспорты
            row["passport_who"],                             # Кем выдан паспорт
            row["mobile"]                                    # Телефон - мобильный
        ]

        self.__writer.writerow(real_row)

def finance():

    fname = tempfile.mktemp(".csv")

    with db.c_db() as cn:

        with c_writer(fname) as writer:

            rows = cn.select_all("""

            select
                
                *,
                my_printable_date_dmy(born_date) as printable_born_date,
                my_printable_date_dmy(passport_when) as printable_passport_when
            
            from abiturient_full
            where

                id in (select aid from statement_full where decree > 1 and target = 1 and form = 1 and pay = 1)

            """)

            for r in rows:

                writer(r)

    return ( True, { "file" : lib.protocol.pack_file(fname, ".csv") } )

