
import tempfile, csv
import db, lib

class c_writer:

    def __init__(self, fname):

        self.__fl = open(fname, "w", newline = "")
        self.__writer = csv.writer(self.__fl, delimiter = "%")
        self.__writer.writerow([ "Серия паспорта", "Номер паспорта", "Фамилия", "Имя", "Отчество", "Математика", "Физика", "Русский язык", "Обществознание", "История", "Литература" ])

    def __enter__(self):

        return self

    def __exit__(self, ex_type, ex_value, traceback):

        if ex_type == None: self.__fl.close()

    def __call__(self, passport_series, passport_number,
            real_surname = None, real_name = None, real_patrname = None,
            surname = None, name = None, patrname = None,
            math_rating = None, phys_rating = None, rus_rating = None, society_rating = None, hist_rating = None, liter_rating = None):

        row = []

        for value in [ surname, name, patrname, math_rating, phys_rating, rus_rating, society_rating, hist_rating, liter_rating ]:

            if value == None: row.append("")
            else: row.append(value)

        if any(row):

            if row[0] == "": row[0] = real_surname
            if row[1] == "": row[1] = real_name
            if row[2] == "": row[2] = real_patrname

            self.__writer.writerow([ passport_series, passport_number ] + row)

def check(fis_value, our_value):

    if fis_value != our_value:

        return "%s -> %s" % (our_value, fis_value)

    return None

def check_examine(cn, ename, aid, fis_value):

    row = cn.select_one("select * from examine_full where aid = %d and examineXname = %d and is_ege" % (aid, ename))

    if row:

        try:

            return check(int(fis_value), row["rating"])

        except ValueError:
            
            return "%s -> %s (некорректное значение)" % (row["rating"], fis_value)

    return None

############################################################################ 

def fis_ege():

    with db.c_db() as cn:
            
        query = "select surname, name, patrname, passport_series, passport_number from abiturient_conf where id in (select aid from examine_full where is_ege)"
        fname = tempfile.mktemp(".csv")

        with open(fname, "w", newline = "") as fl:

            writer = csv.writer(fl, delimiter = "%")

            for r in cn.select_all(query):
                
                writer.writerow([ r["surname"], r["name"], r["patrname"], r["passport_series"], r["passport_number"] ])

    return ( True, { "file" : lib.protocol.pack_file(fname, ".csv") } )

def check_fis_ege(content):

    fname = tempfile.mktemp(".csv")

    with db.c_db() as cn:

        with c_writer(fname) as writer:

            reader = csv.reader(content["data"], delimiter = "%")

            ind = 0

            for row in reader:

                ind += 1
                if ind == 1: continue

                surname = row[2]
                name = row[3]
                patrname = row[4]
                passport_series = row[5]
                passport_number = row[6]
                math_rating = row[12]
                phys_rating = row[14]
                rus_rating = row[10]
                society_rating = row[30]
                hist_rating = row[20]
                liter_rating = row[32]

                row = cn.select_one("select * from abiturient_full where passport_series = '%s' and passport_number = '%s'" % (passport_series, passport_number))

                if row == None:

                    writer(passport_series, passport_number, surname = "Не найден")

                else:

                    writer(passport_series, passport_number,
                        real_surname = surname, real_name = name, real_patrname = patrname,
                        surname = check(surname, row["surname"]),
                        name = check(name, row["name"]),
                        patrname = check(patrname, row["patrname"]),
                        math_rating = check_examine(cn, 1, row["id"], math_rating),
                        phys_rating = check_examine(cn, 2, row["id"], phys_rating),
                        rus_rating = check_examine(cn, 3, row["id"], rus_rating),
                        society_rating = check_examine(cn, 4, row["id"], society_rating),
                        hist_rating = check_examine(cn, 5, row["id"], hist_rating),
                        liter_rating = check_examine(cn, 6, row["id"], liter_rating))

    return ( True, { "file" : lib.protocol.pack_file(fname, ".csv") } )

