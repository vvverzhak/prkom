
import unittest, codecs
import config, lib, lib.server
from remote_control.backup import backup
from remote_control.fsb import not_russian
from remote_control.hate import hate
from remote_control.fis_ege import fis_ege, check_fis_ege
from remote_control.ivc import ivc
from remote_control.check import check
from remote_control.finance import finance

def process(content):

    if content["type"] == "backup": status, content = backup()
    elif content["type"] == "not_russian": status, content = not_russian()
    elif content["type"] == "hate": status, content = hate(content)
    elif content["type"] == "fis_ege": status, content = fis_ege()
    elif content["type"] == "check_fis_ege": status, content = check_fis_ege(content)
    elif content["type"] == "ivc": status, content = ivc(content)
    elif content["type"] == "check": status, content = check()
    elif content["type"] == "finance": status, content = finance()
    else: status, content = False, {}

    return (status, content)

class c_server:

    def start():	lib.server.start(config.server.remote_control, process)
    def stop():		lib.server.stop(config.server.remote_control)
    def status():	lib.server.status(config.server.remote_control)

    ############################################################################ 

    class c_test(unittest.TestCase):
        
        def doit(self, tp, suffix, target_id = 1, form_id = 1, pay_id = 1, decree = 1):

            content = \
            {
                "type" : tp,
                "target_id" : target_id,
                "form_id" : form_id,
                "pay_id" : pay_id,
                "decree" : decree
            }

            if tp == "check_fis_ege":

                with codecs.open("../../2014/trash/ФБС/BatchCheckResult.csv", "r", encoding = "CP1251") as fl:
            
                    content["data"] = fl.readlines()
        
            status, content = process(content)
            lib.protocol.unpack_file(content["file"], "/tmp/TODO.%s" % suffix)

        def test_backup(self): self.doit("backup", "tar")
        def test_not_russian(self): self.doit("not_russian", "zip")
        def test_fis_ege(self): self.doit("fis_ege", "csv")
        def test_check_fis_ege(self): self.doit("check_fis_ege", "csv")
        def test_ivc(self): self.doit("ivc", "csv", decree = 1)
        def test_check(self): self.doit("check", "csv")
        def test_finance(self): self.doit("finance", "csv")

        def test_hate(self):

            content = \
            {
                "type" : "hate",
                "message" : "I hate you",
                "reciever" : "127.0.0.1"
            }

            print(process(content))

    def test():

        test = c_server.c_test()

        # test.test_backup()
        # test.test_not_russian()
        # test.test_fis_ege()
        # test.test_check_fis_ege()
        # test.test_ivc()
        # test.test_check()
        test.test_finance()

    def utest():

        runner = unittest.TextTestRunner(verbosity = 2)
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(c_server.c_test)
        runner.run(suite)

