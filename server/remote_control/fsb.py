
import tempfile, zipfile
import db, config, lib, lib.private, enroll.generate

def not_russian():

    with db.c_db() as cn:

        rows = cn.select_all("select id, passport_series, passport_number from abiturient_full_slow where not is_russia")

        ind = 1
        num = len(rows)

        archive_fname = tempfile.mktemp(".zip")

        with zipfile.ZipFile(archive_fname, "w") as zp:

            for row in rows:

                fname, suffix = generate(cn, row)
                zp.write(fname, "%s_%s.odt" % (row["passport_series"], row["passport_number"]))

                print("Иностранец %d из %d (%.2f %%)" % (ind, num, (ind * 100.) / num))
                ind += 1

    return ( True, { "file" : lib.protocol.pack_file(archive_fname, ".zip") } )

def generate(cn, row):

    content = {}
    enroll.generate.generate_base(cn, (row["passport_series"], row["passport_number"]))

    return lib.private.render_base(config.catalog.template_enroll, "enroll", content)

