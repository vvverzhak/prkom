
import lib

sex_translate = lambda ind: { 1 : 1, 2 : 2 }[ind]
married_translate = lambda ind: [ 1, 2 ][ind]
lang_translate = lambda ind: { 1 : 1, 2 : 3, 3 : 2, 4 : 6, 5 : 6, 6 : 4 }[ind]
hostel_translate = lambda ind: { True : 2, False : 1 }[ind]

def citizen_translate(ind):

    dct = \
    {
        1 : 1, 219 : 2, 2 : 3, 218 : 4, 90 : 5, 67 : 6, 6 : 7, 119 : 8, 137 : 9, 114 : 10, 112 : 11, 204 : 12, 19 : 13, 215 : 14, 239 : 15, 3 : 16
    }

    if ind in dct:
        
        return dct[ind]

    return 17

def parent_translate(cn, content, row):

    def parent(rtype, rus_name):

        prnt = cn.select_one("select * from relation_full where aid = %d and relationXtype in %s" % (row["id"], rtype))

        if prnt == None:

            return "%s: отсутствует" % rus_name

        return "%s: %s; Место работы: %s; Должность: %s; Адрес: %s." % (rus_name, prnt["fio"], prnt["work_place"], prnt["work_post"], prnt["address"])

    parents = "%s\n%s" % (parent("(1)", "Отец"), parent("(2)", "Мать"))
    content["ROD"] = parents

def date_translate(cn, content, row):

    current_date = lib.c_current_date_time()

    content["DTR"] = str(row["born_date"])
    content["PASD"] = str(row["passport_when"])
    content["DPO"] = "01.09.%s" % current_date.year

def stupid_translate(cn, content, row):

    if row["privxdoc"] == 3:
        
        content["LGT"] = 3

    elif row["privxdoc"] == 4:
        
        content["LGT"] = 4

    else:
        
        content["LGT"] = 1

def mgt_translate(cn, content, row):

    if row["address_city"] == "Рязань":
        
        content["MGT"] = 1

    elif row["address_base_region"] == 1:

        content["MGT"] = 2

    else:
            
        citizen = citizen_translate(row["passport_country"])

        if citizen == 1:
            
            content["MGT"] = 3

        elif citizen == 17:

            content["MGT"] = 5

        else:

            content["MGT"] = 4

def spec_translate(cn, content, row, decree):

    spec_row = cn.select_one("select * from statement_full where aid = %d and decree = %d" % (row["id"], decree))
    spec_code = spec_row["spec_code"]
    is_target_war = row["is_target_war"] and spec_row["is_maybe_target"]
    is_target_power = row["is_target_power"] and spec_row["is_maybe_target"]

    content["FAK"] = { 2 : 8, 3 : 4, 4 : 1, 5 : 3, 6 : 2, 7 : 7, 8 : 9 }[ spec_row["faculty"] ]
    content["FOB"] = { 1 : 1, 2 : 2, 3 : 3 }[ spec_row["form"] ]
    content["KUR"] = spec_row["course"]
    content["TIP"] = { 1 : 5, 2 : 3, 3 : 8, 4 : 3 }[ spec_row["target"] ]
    content["MAG"] = [ 1, 2 ][ spec_row["target"] in (3, 4) ]
    content["VYB"] = [ 1, 29 ][ decree == 2 ]

    if spec_row["is_teach_war_center"]:

        content["NAP"] = 3

    elif spec_row["pay"] == 2:

        citizen_type = citizen_translate(row["passport_country"])

        if citizen_type == 1: content["NAP"] = 4
        elif citizen_type != 17: content["NAP"] = 6
        else: content["NAP"] = 5

    else:

        content["NAP"] = 1

    if is_target_war: content["CEL"] = 3
    elif is_target_power: content["CEL"] = 2
    else: content["CEL"] = 1

    if spec_code[-4 : -2] == "05":
        
        content["SPE"] = spec_code
        content["NAPR"] = "0"

    else:

        content["SPE"] = "0"
        content["NAPR"] = spec_code

