
import csv

class c_writer:

    def __init__(self, fname):

        self.__head = \
        [
            "FAM",
            "IMJ",
            "OTH",
            "GHRAG",
            "FAK",
            "SPE",
            "PAS",
            "POL",
            "DTR",
            "FOB",
            "KUR",
            "SEM",
            "LNG",
            "OBS",
            "TIP",
            "LGT",
            "OVS",
            "ROD",
            "MTR",
            "MGT",
            "IN1",
            "AD1",
            "IN2",
            "AD2",
            "NTL",
            "NAP",
            "CEL",
            "NAPR",
            "TELE1",
            "PASD",
            "PASR",
            "DPO",
            "MAG",
            "VYB"
        ]

        self.__fl = open(fname, "w", newline = "")
        self.__writer = csv.writer(self.__fl, delimiter = "%")
        self.__writer.writerow(self.__head)

    __enter__ = lambda self: self
    
    def __exit__(self, ex_type, ex_value, traceback):
            
        if ex_type == None: self.__fl.close()

    def __call__(self, row):
    
        real_row = []

        for f in self.__head:

            real_row.append(row[f])

        self.__writer.writerow(real_row)

