
import tempfile
import db, lib
from remote_control.ivc.writer import c_writer
from remote_control.ivc.translate import *

def ivc(param):

    decree = param["decree"]

    with db.c_db() as cn:
        
        fname = generate(cn, decree)

    return ( True, { "file" : lib.protocol.pack_file(fname, ".csv") } )

def generate(cn, decree):

    fname = tempfile.mktemp(".csv")

    with c_writer(fname) as writer:
            
        rows = cn.select_all("select * from abiturient_full where id in (select aid from statement where decree = %d) order by fio" % decree)

        for r in rows:

            content = {}

            parent_translate(cn, content, r)
            date_translate(cn, content, r)
            stupid_translate(cn, content, r)
            mgt_translate(cn, content, r)
            spec_translate(cn, content, r, decree)

            content["FAM"] = r["surname"]
            content["IMJ"] = r["name"]
            content["OTH"] = r["patrname"]
            content["GHRAG"] = citizen_translate(r["passport_country"])
            content["PAS"] = r["passport"]
            content["POL"] = sex_translate(r["sex"])
            content["SEM"] = married_translate(r["is_married"])
            content["LNG"] = lang_translate(r["lang"])
            content["OBS"] = hostel_translate(r["is_hostel"])
            content["OVS"] = 5
            content["MTR"] = r["born_address"]
            content["IN1"] = r["address_index"]
            content["AD1"] = r["address"]
            content["IN2"] = 0
            content["AD2"] = 0
            content["NTL"] = r["telephone"]
            content["TELE1"] = r["mobile"]
            content["PASR"] = r["passport_who"]

            writer(content)

    return fname

