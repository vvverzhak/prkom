
import sys, os, json, tempfile, config
from lib import protocol

def ip_to_login(cn, ip):

    row = cn.select_one("select user_name from users where ip = '%s'" % ip)

    if row == None:

        return None

    return row["user_name"]

def login_to_ip(cn, login):

    row = cn.select_one("select ip from users where user_name = '%s'" % login)

    if row == None:

        return None

    return row["ip"]

def level(cn, login):

    row = cn.select_one("select level from users where ip = '%s'" % login)

    if row == None:

        return 20

    return row["level"]

############################################################################ 
# Костыль

def render_base(template_dname, template_fname, content):
    
    suffix = ".odt"
    fname = tempfile.mktemp(suffix)
    template_fname = "%s/%s.odt" % ( template_dname, template_fname )
    
    with open(fname, "w") as fl:
        
        fl.write(json.dumps(content))
        
    os.system("/usr/bin/python2 %s %s %s" % (config.renderer, fname, template_fname))

    return (fname, suffix)

def render(template_dname, template_fname, content):
    
    fname, suffix = render_base(template_dname, template_fname, content)

    return protocol.pack_file(fname, suffix)

