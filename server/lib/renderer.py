#coding=utf-8

import sys, json, os
from appy.pod.renderer import Renderer

fname = sys.argv[1]
template_fname = sys.argv[2]

fl = open(fname, "r")
content = json.loads(fl.read())
fl.close()

os.unlink(fname)

renderer = Renderer(template_fname, content, fname)
renderer.run()

