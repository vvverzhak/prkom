
import sys, socketserver, threading, os, signal, time
import lib, lib.protocol

############################################################################ 
# Пересылка сообщений

class c_handler(socketserver.BaseRequestHandler):
    
    process = None
	
    def handle(self):
        
        sock = self.request
        
        content = lib.protocol.recv(sock)
        content["reciever"] = sock.getpeername()

        if content != None:
            
            status, content = c_handler.process(content)
        
        if not lib.protocol.send(sock, content, status):
            
            print("Отправить данные не удалось")
            
        sock.close()

############################################################################ 
# Серверный поток и потоки, обрабатывающие сообщения

server = None
server_config = None

def terminate(signum, frame):
	
    global server
	
    server.shutdown()

def main():

    global server
    global server_config

    server = socketserver.TCPServer(("0.0.0.0", server_config.port), c_handler)
    server_thread = threading.Thread(target = server.serve_forever)
    server_thread.start()

    signal.signal(signal.SIGTERM, terminate)

    with open(server_config.pid_file, "w") as fl:
        
        fl.write(str(os.getpid()))

    return 0

############################################################################
# Интерфейс

@lib.function_description("Запуск сервера")
def start(config, process):

    global server_config

    server_config = config
    c_handler.process = process

    if os.fork() == 0: sys.exit(main())

@lib.function_description("Останов сервера")
def stop(config):

    if os.path.exists(config.pid_file):

        fl = open(config.pid_file, "r")
        pid = int(fl.read())
        fl.close()

        os.kill(pid, signal.SIGTERM)
        time.sleep(1)

        os.remove(config.pid_file)
        
        if os.path.exists("/proc/" + str(pid)): os.kill(pid, signal.SIGKILL)

        print("Сервер остановлен")

    else: print("Сервер не запущен")

@lib.function_description("Получение статуса сервера")
def status(config):

    if os.path.exists(config.pid_file):

        with open(config.pid_file, "rb") as fl:

            pid = int(fl.read())

        print("Сервер запущен; pid = %d" % pid)
    
    else:
        
        print("Сервер не запущен")

def run(servers, option, opt_str, value, parser):

    option = str(option)
    servers[value].c_server.__dict__[ option[ option.rfind("-") + 1 : ] ]()

