
import lib

def is_any_alpha(strng):

    return any(map(lambda x: x.isalpha(), strng))

def set_bool(content, field_name, flag):

    content[field_name] = "X" if flag else ""

def set_yes_no(content, field_name, flag):

    set_bool(content, "%s_yes" % field_name, flag)
    set_bool(content, "%s_no" % field_name, not flag)

def set_current_date(content):

    tm = lib.c_current_date_time()

    content["current_day"] = tm.day
    content["current_month"] = tm.month_name().lower()
    content["current_year"] = tm.year

def set_translate_date(content, field_name, date):

    content[field_name] = "%.2d.%.2d.%.2d" % (date.day, date.month, date.year)

def set_lang(content, lang):

    set_bool(content, "lang_english", lang == 1)
    set_bool(content, "lang_dutch", lang == 2)
    set_bool(content, "lang_french", lang == 3)
    set_bool(content, "lang_other", lang not in [1, 2, 3])

def append_to_history(cn, row, reciever):

    tm = lib.c_current_date_time()
    ip, port = reciever

    uid = cn.select_one("select id from users where ip = '%s'" % ip)["id"]
    cn.execute("""
        insert into history (uid, aid, name, surname, patrname, passport_series, passport_number, when_date, when_time)
        values (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s')
    """ % (uid, row["id"], row["name"], row["surname"], row["patrname"], row["passport_series"], row["passport_number"], tm.date_postgres, tm.time))
    cn.select_one("select id from history where uid = %d and when_date = '%s' and when_time = '%s'" % (uid, tm.date_postgres, tm.time))

