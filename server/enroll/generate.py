
import config, db, lib.private
from enroll.various import *

def generate(aid, reciever):

    try:

        with db.c_db() as cn:
        
            content = generate_base(cn, aid, reciever)
            
        return (True, { "file" : lib.private.render(config.catalog.template_enroll, "enroll", content), "message" : [] })

    except RuntimeError as err:
        
        return (False, { "message" : [ err.args[0] ] })

def generate_base(cn, aid, reciever = None, row = None):

    content = {}

    if not row:

        row = cn.select_one("""

            select *
            from abiturient_full_slow as abit
            where ( abit.passport_series = '%s' and abit.passport_number = '%s' )

            """ % (aid[0], aid[1]))

    real_aid = row["id"]

    ############################################################################ 
    # История и служебная информация

    if reciever: append_to_history(cn, row, reciever)

    set_current_date(content)

    #############################################################################
    # Основные поля

    content["reg_code"] = row["reg_code"]
    content["born_address"] = row["born_address"]
    content["telephone_code"] = row["telephone_code"]
    content["telephone_number"] = row["telephone_number"]
    content["mobile"] = row["mobile"]
    content["email"] = row["email"]
    content["name"] = row["name"]
    content["surname"] = row["surname"]
    content["patrname"] = row["patrname"]
    content["name_genitive"] = row["name_genitive"]
    content["surname_genitive"] = row["surname_genitive"]
    content["patrname_genitive"] = row["patrname_genitive"]
    content["passport_country"] = row["passport_country_value"]
    content["passport_series"] = row["passport_series"]
    content["passport_number"] = row["passport_number"]
    content["passport_who"] = row["passport_who"]
    content["address"] = row["address"]
    content["document_return_personally_marker"] = row["document_return_personally_marker"]
    content["document_return_post_marker"] = row["document_return_post_marker"]
    content["special_condition"] = row["special_condition"]
    content["previous_education_address"] = row["previous_education_address"]
    content["previous_education_term"] = row["previous_education_term"]
    content["previous_education_course"] = row["previous_education_course"]
    content["previous_education_contest"] = row["previous_education_contest"].lower()
    content["achievement_olymp"] = row["achievementxolymp_value"]
    content["achievement_cat_1"] = row["achievementxcatx1_value"]
    content["achievement_cat_2"] = row["achievementxcatx2_value"]

    set_bool(content, "is_special_condition", is_any_alpha(row["special_condition"]))
    set_bool(content, "is_cert_best", row["is_cert_best"])
    set_bool(content, "is_publ", row["is_publ"])
    set_bool(content, "is_diplom_best", row["is_diplom_best"])

    set_translate_date(content, "passport_when", row["passport_when"])
    set_translate_date(content, "born_date", row["born_date"])

    set_yes_no(content, "stupid", row["is_stupid"])
    set_yes_no(content, "woexamine", row["is_woexamine"])
    set_yes_no(content, "hostel", row["is_hostel"])
    set_yes_no(content, "first", row["is_first"])

    content["category_comment"] = row["category_comment"] if row["is_stupid"] or row["is_woexamine"] else ""

    set_lang(content, row["lang"])

    ############################################################################ 
    # Поступление

    content["bachelor"] = []
    content["master"] = []
    content["spec_for_note"] = []

    for target in (1, 3):

        for form, pay in lib.form_pay:

            target_name = "bachelor" if target == 1 else "master"
            sts = cn.select_all("select * from statement_full where aid = %d and target = %d and form = %d and pay = %d order by priority" %
                    (real_aid, target, form, pay))

            if len(sts):

                content[target_name].append({})
                t_content = content[target_name][-1]

                is_target = (target in (1, 3) and form == 1 and pay == 1 and row["is_target"])

                set_bool(t_content, "is_see", form == 1)
                set_bool(t_content, "is_evening", form == 2)
                set_bool(t_content, "is_not_see", form == 3)
                set_bool(t_content, "is_free", pay == 1)
                set_bool(t_content, "is_pay", pay == 2)
                set_bool(t_content, "is_target", is_target)

                t_content["spec"] = []

                for st in sts:

                    t_content["spec"].append({})
                    t_spec = t_content["spec"][-1]

                    t_spec["priority"] = st["priority"]
                    t_spec["code"] = st["spec_code"]
                    t_spec["name"] = st["spec_name"]
                    t_spec["faculty"] = st["faculty_value"]

                    content["spec_for_note"].append({})
                    t_spec_for_note = content["spec_for_note"][-1]
                    
                    t_spec_for_note["spec_value"] = st["spec_value"]
                    t_spec_for_note["priority"] = st["priority"]

    sts = cn.select_all("select * from statement_full where aid = %d and target in (2, 4)" % real_aid)
    content["tol"] = []

    for st in sts:

        content["tol"].append({})
        t_content = content["tol"][-1]

        t_content["form"] = st["form_value"].lower()
        t_content["spec"] = st["spec_code_name"]
        t_content["faculty"] = st["faculty_value"]
        t_content["course"] = st["course"]

        set_bool(t_content, "is_see", st["form"] == 1)
        set_bool(t_content, "is_evening", st["form"] == 2)
        set_bool(t_content, "is_not_see", st["form"] == 3)
        set_bool(t_content, "is_free", st["pay"] == 1)
        set_bool(t_content, "is_pay", st["pay"] == 2)

        content["spec_for_note"].append({})
        t_spec_for_note = content["spec_for_note"][-1]
                    
        t_spec_for_note["spec_value"] = st["spec_value"]
        t_spec_for_note["priority"] = st["priority"]

    ############################################################################ 
    # Экзамены

    exs = \
    [
        ( "math", 1 ), ( "phys", 2 ), ( "rus", 3 ), ( "society", 4 ),
        ( "hist", 5 ), ( "liter", 6 ), ( "creation", 7 ), ( "interview", 8 )
    ]

    for en_name, eid in exs:

        ex = cn.select_one("select * from examine_full where examineXname = %d and aid = %d and is_examine_real" % (eid, real_aid))

        if ex:
            
            is_ege = (ex["examinextype"] == 1)
            is_in = (ex["examinextype"] == 2)
            is_olymp = (ex["examinextype"] == 3)
            is_need = ex["yes"]

            if is_need:

                rating = ""
                year = ""

            else:

                rating = ex["rating"]
                year = ex["year"].year

        else:

            rating = ""
            year = ""

            is_ege = False
            is_in = False
            is_olymp = False
            is_need = False

        content[en_name + "_rating"] = rating
        content[en_name + "_year"] = year

        set_bool(content, "is_" + en_name + "_need", is_need)
        set_bool(content, "is_" + en_name + "_ege", is_ege and not is_need)
        set_bool(content, "is_" + en_name + "_olymp", is_olymp and not is_need)
        set_bool(content, "is_" + en_name + "_ege_need", is_ege and is_need)
        set_bool(content, "is_" + en_name + "_in_need", is_in and is_need)

    ############################################################################ 
    # Диплом об образовании

    ed_row = cn.select_one("select * from education_document_full where aid = %d and is_main" % real_aid)

    if not ed_row:

        raise RuntimeError("Не указан документ об образовании, по которому абитуриент поступает в ВУЗ")
    
    content["education_document_date"] = ed_row["date"]
    content["education_document_date_year"] = ed_row["date"].year
    content["education_document_who"] = ed_row["who"]
    content["education_document_series"] = ed_row["series"]
    content["education_document_number"] = ed_row["number"]
    content["education_document_date"] = "%s.%.2d.%s" % (ed_row["date"].day, ed_row["date"].month, ed_row["date"].year)
    content["education_document_type"] = ed_row["educationxdocumentxtype_value"]

    set_yes_no(content, "is_copy", ed_row["is_copy"])
    set_yes_no(content, "first_master", row["first_master"])

    set_bool(content, "is_cert", ed_row["is_cert"])
    set_bool(content, "is_diplom", ed_row["is_diplom"])
    set_bool(content, "is_reference", ed_row["is_reference"])
    set_bool(content, "is_incomplete_reference", ed_row["is_incomplete_reference"])

    ############################################################################ 

    return content

