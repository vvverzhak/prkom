
import unittest
import config, lib, lib.server, enroll.generate

def process(content):

    aid = content["aid"]
    reciever = content["reciever"]

    is_success, content = enroll.generate.generate(aid, reciever)

    return (is_success, content)

class c_server:

    def start():	lib.server.start(config.server.enroll, process)
    def stop():		lib.server.stop(config.server.enroll)
    def status():	lib.server.status(config.server.enroll)

    class c_test(unittest.TestCase):
        
        def test_enroll(self):

            is_success, content = enroll.generate.generate([ "8888", "777777" ], [ "192.168.33.231", 1988 ])

            self.assertTrue(is_success, msg = str(content))

            content = lib.protocol.pack(content, True)
            content_file = lib.protocol.unpack(content)["file"]
            lib.protocol.unpack_file(content_file, "/tmp/TODO.odt")

    def test():

        test = c_server.c_test()

        test.test_enroll()

    def utest():

        runner = unittest.TextTestRunner(verbosity = 2)
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(c_server.c_test)
        runner.run(suite)

